package org.xtext.example.mydsl.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.example.mydsl.services.MyOcl2GrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMyOcl2Parser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'package'", "'end'", "'context'", "'::'", "'('", "')'", "':'", "'Double'", "'Integer'", "'Boolean'", "'STRING'", "'>'", "'<'", "'='", "'+'", "'.'", "'@pre'", "'.end'", "'and'", "'-'", "'@'", "'post'", "'pre'", "'inv'", "'deff'", "'import'", "'http://'", "'/'"
    };
    public static final int RULE_ID=4;
    public static final int T__29=29;
    public static final int T__28=28;
    public static final int T__27=27;
    public static final int T__26=26;
    public static final int T__25=25;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int RULE_SL_COMMENT=8;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__30=30;
    public static final int T__19=19;
    public static final int T__31=31;
    public static final int RULE_STRING=6;
    public static final int T__32=32;
    public static final int T__33=33;
    public static final int T__16=16;
    public static final int T__34=34;
    public static final int T__15=15;
    public static final int T__35=35;
    public static final int T__18=18;
    public static final int T__36=36;
    public static final int T__17=17;
    public static final int T__37=37;
    public static final int T__12=12;
    public static final int T__38=38;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=5;
    public static final int RULE_WS=9;

    // delegates
    // delegators


        public InternalMyOcl2Parser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMyOcl2Parser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMyOcl2Parser.tokenNames; }
    public String getGrammarFileName() { return "../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g"; }



     	private MyOcl2GrammarAccess grammarAccess;
     	
        public InternalMyOcl2Parser(TokenStream input, MyOcl2GrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Main";	
       	}
       	
       	@Override
       	protected MyOcl2GrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleMain"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:67:1: entryRuleMain returns [EObject current=null] : iv_ruleMain= ruleMain EOF ;
    public final EObject entryRuleMain() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMain = null;


        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:68:2: (iv_ruleMain= ruleMain EOF )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:69:2: iv_ruleMain= ruleMain EOF
            {
             newCompositeNode(grammarAccess.getMainRule()); 
            pushFollow(FOLLOW_ruleMain_in_entryRuleMain75);
            iv_ruleMain=ruleMain();

            state._fsp--;

             current =iv_ruleMain; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleMain85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMain"


    // $ANTLR start "ruleMain"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:76:1: ruleMain returns [EObject current=null] : ( ( (lv_import_sentence_0_0= ruledeclaration_sentence ) )? ( (lv_Package_1_0= rulepackage_declaration ) ) ) ;
    public final EObject ruleMain() throws RecognitionException {
        EObject current = null;

        EObject lv_import_sentence_0_0 = null;

        EObject lv_Package_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:79:28: ( ( ( (lv_import_sentence_0_0= ruledeclaration_sentence ) )? ( (lv_Package_1_0= rulepackage_declaration ) ) ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:80:1: ( ( (lv_import_sentence_0_0= ruledeclaration_sentence ) )? ( (lv_Package_1_0= rulepackage_declaration ) ) )
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:80:1: ( ( (lv_import_sentence_0_0= ruledeclaration_sentence ) )? ( (lv_Package_1_0= rulepackage_declaration ) ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:80:2: ( (lv_import_sentence_0_0= ruledeclaration_sentence ) )? ( (lv_Package_1_0= rulepackage_declaration ) )
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:80:2: ( (lv_import_sentence_0_0= ruledeclaration_sentence ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==36) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:81:1: (lv_import_sentence_0_0= ruledeclaration_sentence )
                    {
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:81:1: (lv_import_sentence_0_0= ruledeclaration_sentence )
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:82:3: lv_import_sentence_0_0= ruledeclaration_sentence
                    {
                     
                    	        newCompositeNode(grammarAccess.getMainAccess().getImport_sentenceDeclaration_sentenceParserRuleCall_0_0()); 
                    	    
                    pushFollow(FOLLOW_ruledeclaration_sentence_in_ruleMain131);
                    lv_import_sentence_0_0=ruledeclaration_sentence();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getMainRule());
                    	        }
                           		add(
                           			current, 
                           			"import_sentence",
                            		lv_import_sentence_0_0, 
                            		"declaration_sentence");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }

            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:98:3: ( (lv_Package_1_0= rulepackage_declaration ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:99:1: (lv_Package_1_0= rulepackage_declaration )
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:99:1: (lv_Package_1_0= rulepackage_declaration )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:100:3: lv_Package_1_0= rulepackage_declaration
            {
             
            	        newCompositeNode(grammarAccess.getMainAccess().getPackagePackage_declarationParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_rulepackage_declaration_in_ruleMain153);
            lv_Package_1_0=rulepackage_declaration();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getMainRule());
            	        }
                   		set(
                   			current, 
                   			"Package",
                    		lv_Package_1_0, 
                    		"package_declaration");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMain"


    // $ANTLR start "entryRulepackage_declaration"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:124:1: entryRulepackage_declaration returns [EObject current=null] : iv_rulepackage_declaration= rulepackage_declaration EOF ;
    public final EObject entryRulepackage_declaration() throws RecognitionException {
        EObject current = null;

        EObject iv_rulepackage_declaration = null;


        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:125:2: (iv_rulepackage_declaration= rulepackage_declaration EOF )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:126:2: iv_rulepackage_declaration= rulepackage_declaration EOF
            {
             newCompositeNode(grammarAccess.getPackage_declarationRule()); 
            pushFollow(FOLLOW_rulepackage_declaration_in_entryRulepackage_declaration189);
            iv_rulepackage_declaration=rulepackage_declaration();

            state._fsp--;

             current =iv_rulepackage_declaration; 
            match(input,EOF,FOLLOW_EOF_in_entryRulepackage_declaration199); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulepackage_declaration"


    // $ANTLR start "rulepackage_declaration"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:133:1: rulepackage_declaration returns [EObject current=null] : (otherlv_0= 'package' ( (lv_sharedelement_1_0= ruleendstartelement ) ) ( (lv_element_2_0= ruleabstractelement ) )* otherlv_3= 'end' ( (lv_sharedelement_4_0= ruleendstartelement ) ) ) ;
    public final EObject rulepackage_declaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_3=null;
        EObject lv_sharedelement_1_0 = null;

        EObject lv_element_2_0 = null;

        EObject lv_sharedelement_4_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:136:28: ( (otherlv_0= 'package' ( (lv_sharedelement_1_0= ruleendstartelement ) ) ( (lv_element_2_0= ruleabstractelement ) )* otherlv_3= 'end' ( (lv_sharedelement_4_0= ruleendstartelement ) ) ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:137:1: (otherlv_0= 'package' ( (lv_sharedelement_1_0= ruleendstartelement ) ) ( (lv_element_2_0= ruleabstractelement ) )* otherlv_3= 'end' ( (lv_sharedelement_4_0= ruleendstartelement ) ) )
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:137:1: (otherlv_0= 'package' ( (lv_sharedelement_1_0= ruleendstartelement ) ) ( (lv_element_2_0= ruleabstractelement ) )* otherlv_3= 'end' ( (lv_sharedelement_4_0= ruleendstartelement ) ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:137:3: otherlv_0= 'package' ( (lv_sharedelement_1_0= ruleendstartelement ) ) ( (lv_element_2_0= ruleabstractelement ) )* otherlv_3= 'end' ( (lv_sharedelement_4_0= ruleendstartelement ) )
            {
            otherlv_0=(Token)match(input,11,FOLLOW_11_in_rulepackage_declaration236); 

                	newLeafNode(otherlv_0, grammarAccess.getPackage_declarationAccess().getPackageKeyword_0());
                
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:141:1: ( (lv_sharedelement_1_0= ruleendstartelement ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:142:1: (lv_sharedelement_1_0= ruleendstartelement )
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:142:1: (lv_sharedelement_1_0= ruleendstartelement )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:143:3: lv_sharedelement_1_0= ruleendstartelement
            {
             
            	        newCompositeNode(grammarAccess.getPackage_declarationAccess().getSharedelementEndstartelementParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleendstartelement_in_rulepackage_declaration257);
            lv_sharedelement_1_0=ruleendstartelement();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getPackage_declarationRule());
            	        }
                   		set(
                   			current, 
                   			"sharedelement",
                    		lv_sharedelement_1_0, 
                    		"endstartelement");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:159:2: ( (lv_element_2_0= ruleabstractelement ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==13) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:160:1: (lv_element_2_0= ruleabstractelement )
            	    {
            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:160:1: (lv_element_2_0= ruleabstractelement )
            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:161:3: lv_element_2_0= ruleabstractelement
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getPackage_declarationAccess().getElementAbstractelementParserRuleCall_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleabstractelement_in_rulepackage_declaration278);
            	    lv_element_2_0=ruleabstractelement();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getPackage_declarationRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"element",
            	            		lv_element_2_0, 
            	            		"abstractelement");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            otherlv_3=(Token)match(input,12,FOLLOW_12_in_rulepackage_declaration291); 

                	newLeafNode(otherlv_3, grammarAccess.getPackage_declarationAccess().getEndKeyword_3());
                
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:181:1: ( (lv_sharedelement_4_0= ruleendstartelement ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:182:1: (lv_sharedelement_4_0= ruleendstartelement )
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:182:1: (lv_sharedelement_4_0= ruleendstartelement )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:183:3: lv_sharedelement_4_0= ruleendstartelement
            {
             
            	        newCompositeNode(grammarAccess.getPackage_declarationAccess().getSharedelementEndstartelementParserRuleCall_4_0()); 
            	    
            pushFollow(FOLLOW_ruleendstartelement_in_rulepackage_declaration312);
            lv_sharedelement_4_0=ruleendstartelement();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getPackage_declarationRule());
            	        }
                   		set(
                   			current, 
                   			"sharedelement",
                    		lv_sharedelement_4_0, 
                    		"endstartelement");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulepackage_declaration"


    // $ANTLR start "entryRuleendstartelement"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:207:1: entryRuleendstartelement returns [EObject current=null] : iv_ruleendstartelement= ruleendstartelement EOF ;
    public final EObject entryRuleendstartelement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleendstartelement = null;


        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:208:2: (iv_ruleendstartelement= ruleendstartelement EOF )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:209:2: iv_ruleendstartelement= ruleendstartelement EOF
            {
             newCompositeNode(grammarAccess.getEndstartelementRule()); 
            pushFollow(FOLLOW_ruleendstartelement_in_entryRuleendstartelement348);
            iv_ruleendstartelement=ruleendstartelement();

            state._fsp--;

             current =iv_ruleendstartelement; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleendstartelement358); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleendstartelement"


    // $ANTLR start "ruleendstartelement"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:216:1: ruleendstartelement returns [EObject current=null] : ( (lv_name_0_0= RULE_ID ) ) ;
    public final EObject ruleendstartelement() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;

         enterRule(); 
            
        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:219:28: ( ( (lv_name_0_0= RULE_ID ) ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:220:1: ( (lv_name_0_0= RULE_ID ) )
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:220:1: ( (lv_name_0_0= RULE_ID ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:221:1: (lv_name_0_0= RULE_ID )
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:221:1: (lv_name_0_0= RULE_ID )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:222:3: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleendstartelement399); 

            			newLeafNode(lv_name_0_0, grammarAccess.getEndstartelementAccess().getNameIDTerminalRuleCall_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getEndstartelementRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"ID");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleendstartelement"


    // $ANTLR start "entryRuleabstractelement"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:246:1: entryRuleabstractelement returns [EObject current=null] : iv_ruleabstractelement= ruleabstractelement EOF ;
    public final EObject entryRuleabstractelement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleabstractelement = null;


        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:247:2: (iv_ruleabstractelement= ruleabstractelement EOF )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:248:2: iv_ruleabstractelement= ruleabstractelement EOF
            {
             newCompositeNode(grammarAccess.getAbstractelementRule()); 
            pushFollow(FOLLOW_ruleabstractelement_in_entryRuleabstractelement439);
            iv_ruleabstractelement=ruleabstractelement();

            state._fsp--;

             current =iv_ruleabstractelement; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleabstractelement449); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleabstractelement"


    // $ANTLR start "ruleabstractelement"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:255:1: ruleabstractelement returns [EObject current=null] : ( (lv_elements_0_0= rulecontext_declaration ) )+ ;
    public final EObject ruleabstractelement() throws RecognitionException {
        EObject current = null;

        EObject lv_elements_0_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:258:28: ( ( (lv_elements_0_0= rulecontext_declaration ) )+ )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:259:1: ( (lv_elements_0_0= rulecontext_declaration ) )+
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:259:1: ( (lv_elements_0_0= rulecontext_declaration ) )+
            int cnt3=0;
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==13) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:260:1: (lv_elements_0_0= rulecontext_declaration )
            	    {
            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:260:1: (lv_elements_0_0= rulecontext_declaration )
            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:261:3: lv_elements_0_0= rulecontext_declaration
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getAbstractelementAccess().getElementsContext_declarationParserRuleCall_0()); 
            	    	    
            	    pushFollow(FOLLOW_rulecontext_declaration_in_ruleabstractelement494);
            	    lv_elements_0_0=rulecontext_declaration();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getAbstractelementRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"elements",
            	            		lv_elements_0_0, 
            	            		"context_declaration");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt3 >= 1 ) break loop3;
                        EarlyExitException eee =
                            new EarlyExitException(3, input);
                        throw eee;
                }
                cnt3++;
            } while (true);


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleabstractelement"


    // $ANTLR start "entryRulecontext_declaration"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:285:1: entryRulecontext_declaration returns [EObject current=null] : iv_rulecontext_declaration= rulecontext_declaration EOF ;
    public final EObject entryRulecontext_declaration() throws RecognitionException {
        EObject current = null;

        EObject iv_rulecontext_declaration = null;


        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:286:2: (iv_rulecontext_declaration= rulecontext_declaration EOF )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:287:2: iv_rulecontext_declaration= rulecontext_declaration EOF
            {
             newCompositeNode(grammarAccess.getContext_declarationRule()); 
            pushFollow(FOLLOW_rulecontext_declaration_in_entryRulecontext_declaration530);
            iv_rulecontext_declaration=rulecontext_declaration();

            state._fsp--;

             current =iv_rulecontext_declaration; 
            match(input,EOF,FOLLOW_EOF_in_entryRulecontext_declaration540); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulecontext_declaration"


    // $ANTLR start "rulecontext_declaration"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:294:1: rulecontext_declaration returns [EObject current=null] : (otherlv_0= 'context' ( (lv_name_1_0= RULE_ID ) ) ( (lv_Modalite_2_0= ruleMiddleSection ) )? ( (lv_Modalite1_3_0= ruleLastSection ) )* ) ;
    public final EObject rulecontext_declaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        EObject lv_Modalite_2_0 = null;

        EObject lv_Modalite1_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:297:28: ( (otherlv_0= 'context' ( (lv_name_1_0= RULE_ID ) ) ( (lv_Modalite_2_0= ruleMiddleSection ) )? ( (lv_Modalite1_3_0= ruleLastSection ) )* ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:298:1: (otherlv_0= 'context' ( (lv_name_1_0= RULE_ID ) ) ( (lv_Modalite_2_0= ruleMiddleSection ) )? ( (lv_Modalite1_3_0= ruleLastSection ) )* )
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:298:1: (otherlv_0= 'context' ( (lv_name_1_0= RULE_ID ) ) ( (lv_Modalite_2_0= ruleMiddleSection ) )? ( (lv_Modalite1_3_0= ruleLastSection ) )* )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:298:3: otherlv_0= 'context' ( (lv_name_1_0= RULE_ID ) ) ( (lv_Modalite_2_0= ruleMiddleSection ) )? ( (lv_Modalite1_3_0= ruleLastSection ) )*
            {
            otherlv_0=(Token)match(input,13,FOLLOW_13_in_rulecontext_declaration577); 

                	newLeafNode(otherlv_0, grammarAccess.getContext_declarationAccess().getContextKeyword_0());
                
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:302:1: ( (lv_name_1_0= RULE_ID ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:303:1: (lv_name_1_0= RULE_ID )
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:303:1: (lv_name_1_0= RULE_ID )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:304:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_rulecontext_declaration594); 

            			newLeafNode(lv_name_1_0, grammarAccess.getContext_declarationAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getContext_declarationRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:320:2: ( (lv_Modalite_2_0= ruleMiddleSection ) )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==14) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:321:1: (lv_Modalite_2_0= ruleMiddleSection )
                    {
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:321:1: (lv_Modalite_2_0= ruleMiddleSection )
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:322:3: lv_Modalite_2_0= ruleMiddleSection
                    {
                     
                    	        newCompositeNode(grammarAccess.getContext_declarationAccess().getModaliteMiddleSectionParserRuleCall_2_0()); 
                    	    
                    pushFollow(FOLLOW_ruleMiddleSection_in_rulecontext_declaration620);
                    lv_Modalite_2_0=ruleMiddleSection();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getContext_declarationRule());
                    	        }
                           		add(
                           			current, 
                           			"Modalite",
                            		lv_Modalite_2_0, 
                            		"MiddleSection");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }

            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:338:3: ( (lv_Modalite1_3_0= ruleLastSection ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( ((LA5_0>=32 && LA5_0<=35)) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:339:1: (lv_Modalite1_3_0= ruleLastSection )
            	    {
            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:339:1: (lv_Modalite1_3_0= ruleLastSection )
            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:340:3: lv_Modalite1_3_0= ruleLastSection
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getContext_declarationAccess().getModalite1LastSectionParserRuleCall_3_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleLastSection_in_rulecontext_declaration642);
            	    lv_Modalite1_3_0=ruleLastSection();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getContext_declarationRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"Modalite1",
            	            		lv_Modalite1_3_0, 
            	            		"LastSection");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulecontext_declaration"


    // $ANTLR start "entryRuleMiddleSection"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:364:1: entryRuleMiddleSection returns [EObject current=null] : iv_ruleMiddleSection= ruleMiddleSection EOF ;
    public final EObject entryRuleMiddleSection() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMiddleSection = null;


        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:365:2: (iv_ruleMiddleSection= ruleMiddleSection EOF )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:366:2: iv_ruleMiddleSection= ruleMiddleSection EOF
            {
             newCompositeNode(grammarAccess.getMiddleSectionRule()); 
            pushFollow(FOLLOW_ruleMiddleSection_in_entryRuleMiddleSection679);
            iv_ruleMiddleSection=ruleMiddleSection();

            state._fsp--;

             current =iv_ruleMiddleSection; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleMiddleSection689); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMiddleSection"


    // $ANTLR start "ruleMiddleSection"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:373:1: ruleMiddleSection returns [EObject current=null] : (otherlv_0= '::' ( (lv_FirstSection1_1_0= ruleFirstSection ) ) otherlv_2= '(' ( (lv_SecondSection2_3_0= ruleSecondSection ) )? otherlv_4= ')' ( (lv_ThirdSection3_5_0= ruleThirdSection ) )* ) ;
    public final EObject ruleMiddleSection() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_FirstSection1_1_0 = null;

        EObject lv_SecondSection2_3_0 = null;

        EObject lv_ThirdSection3_5_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:376:28: ( (otherlv_0= '::' ( (lv_FirstSection1_1_0= ruleFirstSection ) ) otherlv_2= '(' ( (lv_SecondSection2_3_0= ruleSecondSection ) )? otherlv_4= ')' ( (lv_ThirdSection3_5_0= ruleThirdSection ) )* ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:377:1: (otherlv_0= '::' ( (lv_FirstSection1_1_0= ruleFirstSection ) ) otherlv_2= '(' ( (lv_SecondSection2_3_0= ruleSecondSection ) )? otherlv_4= ')' ( (lv_ThirdSection3_5_0= ruleThirdSection ) )* )
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:377:1: (otherlv_0= '::' ( (lv_FirstSection1_1_0= ruleFirstSection ) ) otherlv_2= '(' ( (lv_SecondSection2_3_0= ruleSecondSection ) )? otherlv_4= ')' ( (lv_ThirdSection3_5_0= ruleThirdSection ) )* )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:377:3: otherlv_0= '::' ( (lv_FirstSection1_1_0= ruleFirstSection ) ) otherlv_2= '(' ( (lv_SecondSection2_3_0= ruleSecondSection ) )? otherlv_4= ')' ( (lv_ThirdSection3_5_0= ruleThirdSection ) )*
            {
            otherlv_0=(Token)match(input,14,FOLLOW_14_in_ruleMiddleSection726); 

                	newLeafNode(otherlv_0, grammarAccess.getMiddleSectionAccess().getColonColonKeyword_0());
                
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:381:1: ( (lv_FirstSection1_1_0= ruleFirstSection ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:382:1: (lv_FirstSection1_1_0= ruleFirstSection )
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:382:1: (lv_FirstSection1_1_0= ruleFirstSection )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:383:3: lv_FirstSection1_1_0= ruleFirstSection
            {
             
            	        newCompositeNode(grammarAccess.getMiddleSectionAccess().getFirstSection1FirstSectionParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleFirstSection_in_ruleMiddleSection747);
            lv_FirstSection1_1_0=ruleFirstSection();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getMiddleSectionRule());
            	        }
                   		add(
                   			current, 
                   			"FirstSection1",
                    		lv_FirstSection1_1_0, 
                    		"FirstSection");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_2=(Token)match(input,15,FOLLOW_15_in_ruleMiddleSection759); 

                	newLeafNode(otherlv_2, grammarAccess.getMiddleSectionAccess().getLeftParenthesisKeyword_2());
                
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:403:1: ( (lv_SecondSection2_3_0= ruleSecondSection ) )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==RULE_ID) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:404:1: (lv_SecondSection2_3_0= ruleSecondSection )
                    {
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:404:1: (lv_SecondSection2_3_0= ruleSecondSection )
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:405:3: lv_SecondSection2_3_0= ruleSecondSection
                    {
                     
                    	        newCompositeNode(grammarAccess.getMiddleSectionAccess().getSecondSection2SecondSectionParserRuleCall_3_0()); 
                    	    
                    pushFollow(FOLLOW_ruleSecondSection_in_ruleMiddleSection780);
                    lv_SecondSection2_3_0=ruleSecondSection();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getMiddleSectionRule());
                    	        }
                           		add(
                           			current, 
                           			"SecondSection2",
                            		lv_SecondSection2_3_0, 
                            		"SecondSection");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,16,FOLLOW_16_in_ruleMiddleSection793); 

                	newLeafNode(otherlv_4, grammarAccess.getMiddleSectionAccess().getRightParenthesisKeyword_4());
                
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:425:1: ( (lv_ThirdSection3_5_0= ruleThirdSection ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==17) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:426:1: (lv_ThirdSection3_5_0= ruleThirdSection )
            	    {
            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:426:1: (lv_ThirdSection3_5_0= ruleThirdSection )
            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:427:3: lv_ThirdSection3_5_0= ruleThirdSection
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getMiddleSectionAccess().getThirdSection3ThirdSectionParserRuleCall_5_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleThirdSection_in_ruleMiddleSection814);
            	    lv_ThirdSection3_5_0=ruleThirdSection();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getMiddleSectionRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"ThirdSection3",
            	            		lv_ThirdSection3_5_0, 
            	            		"ThirdSection");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMiddleSection"


    // $ANTLR start "entryRuleThirdSection"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:451:1: entryRuleThirdSection returns [EObject current=null] : iv_ruleThirdSection= ruleThirdSection EOF ;
    public final EObject entryRuleThirdSection() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleThirdSection = null;


        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:452:2: (iv_ruleThirdSection= ruleThirdSection EOF )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:453:2: iv_ruleThirdSection= ruleThirdSection EOF
            {
             newCompositeNode(grammarAccess.getThirdSectionRule()); 
            pushFollow(FOLLOW_ruleThirdSection_in_entryRuleThirdSection851);
            iv_ruleThirdSection=ruleThirdSection();

            state._fsp--;

             current =iv_ruleThirdSection; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleThirdSection861); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleThirdSection"


    // $ANTLR start "ruleThirdSection"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:460:1: ruleThirdSection returns [EObject current=null] : (otherlv_0= ':' ( (lv_resultat1_1_0= ruleRETOUR1 ) ) ) ;
    public final EObject ruleThirdSection() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_resultat1_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:463:28: ( (otherlv_0= ':' ( (lv_resultat1_1_0= ruleRETOUR1 ) ) ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:464:1: (otherlv_0= ':' ( (lv_resultat1_1_0= ruleRETOUR1 ) ) )
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:464:1: (otherlv_0= ':' ( (lv_resultat1_1_0= ruleRETOUR1 ) ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:464:3: otherlv_0= ':' ( (lv_resultat1_1_0= ruleRETOUR1 ) )
            {
            otherlv_0=(Token)match(input,17,FOLLOW_17_in_ruleThirdSection898); 

                	newLeafNode(otherlv_0, grammarAccess.getThirdSectionAccess().getColonKeyword_0());
                
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:468:1: ( (lv_resultat1_1_0= ruleRETOUR1 ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:469:1: (lv_resultat1_1_0= ruleRETOUR1 )
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:469:1: (lv_resultat1_1_0= ruleRETOUR1 )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:470:3: lv_resultat1_1_0= ruleRETOUR1
            {
             
            	        newCompositeNode(grammarAccess.getThirdSectionAccess().getResultat1RETOUR1ParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleRETOUR1_in_ruleThirdSection919);
            lv_resultat1_1_0=ruleRETOUR1();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getThirdSectionRule());
            	        }
                   		set(
                   			current, 
                   			"resultat1",
                    		lv_resultat1_1_0, 
                    		"RETOUR1");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleThirdSection"


    // $ANTLR start "entryRuleRETOUR1"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:494:1: entryRuleRETOUR1 returns [EObject current=null] : iv_ruleRETOUR1= ruleRETOUR1 EOF ;
    public final EObject entryRuleRETOUR1() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRETOUR1 = null;


        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:495:2: (iv_ruleRETOUR1= ruleRETOUR1 EOF )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:496:2: iv_ruleRETOUR1= ruleRETOUR1 EOF
            {
             newCompositeNode(grammarAccess.getRETOUR1Rule()); 
            pushFollow(FOLLOW_ruleRETOUR1_in_entryRuleRETOUR1955);
            iv_ruleRETOUR1=ruleRETOUR1();

            state._fsp--;

             current =iv_ruleRETOUR1; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRETOUR1965); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRETOUR1"


    // $ANTLR start "ruleRETOUR1"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:503:1: ruleRETOUR1 returns [EObject current=null] : (otherlv_0= 'Double' | otherlv_1= 'Integer' | otherlv_2= 'Boolean' | otherlv_3= 'STRING' | ( (lv_name_4_0= RULE_ID ) ) ) ;
    public final EObject ruleRETOUR1() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token lv_name_4_0=null;

         enterRule(); 
            
        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:506:28: ( (otherlv_0= 'Double' | otherlv_1= 'Integer' | otherlv_2= 'Boolean' | otherlv_3= 'STRING' | ( (lv_name_4_0= RULE_ID ) ) ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:507:1: (otherlv_0= 'Double' | otherlv_1= 'Integer' | otherlv_2= 'Boolean' | otherlv_3= 'STRING' | ( (lv_name_4_0= RULE_ID ) ) )
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:507:1: (otherlv_0= 'Double' | otherlv_1= 'Integer' | otherlv_2= 'Boolean' | otherlv_3= 'STRING' | ( (lv_name_4_0= RULE_ID ) ) )
            int alt8=5;
            switch ( input.LA(1) ) {
            case 18:
                {
                alt8=1;
                }
                break;
            case 19:
                {
                alt8=2;
                }
                break;
            case 20:
                {
                alt8=3;
                }
                break;
            case 21:
                {
                alt8=4;
                }
                break;
            case RULE_ID:
                {
                alt8=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }

            switch (alt8) {
                case 1 :
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:507:3: otherlv_0= 'Double'
                    {
                    otherlv_0=(Token)match(input,18,FOLLOW_18_in_ruleRETOUR11002); 

                        	newLeafNode(otherlv_0, grammarAccess.getRETOUR1Access().getDoubleKeyword_0());
                        

                    }
                    break;
                case 2 :
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:512:7: otherlv_1= 'Integer'
                    {
                    otherlv_1=(Token)match(input,19,FOLLOW_19_in_ruleRETOUR11020); 

                        	newLeafNode(otherlv_1, grammarAccess.getRETOUR1Access().getIntegerKeyword_1());
                        

                    }
                    break;
                case 3 :
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:517:7: otherlv_2= 'Boolean'
                    {
                    otherlv_2=(Token)match(input,20,FOLLOW_20_in_ruleRETOUR11038); 

                        	newLeafNode(otherlv_2, grammarAccess.getRETOUR1Access().getBooleanKeyword_2());
                        

                    }
                    break;
                case 4 :
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:522:7: otherlv_3= 'STRING'
                    {
                    otherlv_3=(Token)match(input,21,FOLLOW_21_in_ruleRETOUR11056); 

                        	newLeafNode(otherlv_3, grammarAccess.getRETOUR1Access().getSTRINGKeyword_3());
                        

                    }
                    break;
                case 5 :
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:527:6: ( (lv_name_4_0= RULE_ID ) )
                    {
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:527:6: ( (lv_name_4_0= RULE_ID ) )
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:528:1: (lv_name_4_0= RULE_ID )
                    {
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:528:1: (lv_name_4_0= RULE_ID )
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:529:3: lv_name_4_0= RULE_ID
                    {
                    lv_name_4_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleRETOUR11079); 

                    			newLeafNode(lv_name_4_0, grammarAccess.getRETOUR1Access().getNameIDTerminalRuleCall_4_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getRETOUR1Rule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"name",
                            		lv_name_4_0, 
                            		"ID");
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRETOUR1"


    // $ANTLR start "entryRuleFirstSection"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:553:1: entryRuleFirstSection returns [EObject current=null] : iv_ruleFirstSection= ruleFirstSection EOF ;
    public final EObject entryRuleFirstSection() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFirstSection = null;


        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:554:2: (iv_ruleFirstSection= ruleFirstSection EOF )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:555:2: iv_ruleFirstSection= ruleFirstSection EOF
            {
             newCompositeNode(grammarAccess.getFirstSectionRule()); 
            pushFollow(FOLLOW_ruleFirstSection_in_entryRuleFirstSection1120);
            iv_ruleFirstSection=ruleFirstSection();

            state._fsp--;

             current =iv_ruleFirstSection; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleFirstSection1130); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFirstSection"


    // $ANTLR start "ruleFirstSection"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:562:1: ruleFirstSection returns [EObject current=null] : ( (lv_name_0_0= RULE_ID ) ) ;
    public final EObject ruleFirstSection() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;

         enterRule(); 
            
        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:565:28: ( ( (lv_name_0_0= RULE_ID ) ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:566:1: ( (lv_name_0_0= RULE_ID ) )
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:566:1: ( (lv_name_0_0= RULE_ID ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:567:1: (lv_name_0_0= RULE_ID )
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:567:1: (lv_name_0_0= RULE_ID )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:568:3: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleFirstSection1171); 

            			newLeafNode(lv_name_0_0, grammarAccess.getFirstSectionAccess().getNameIDTerminalRuleCall_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getFirstSectionRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"ID");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFirstSection"


    // $ANTLR start "entryRuleSecondSection"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:592:1: entryRuleSecondSection returns [EObject current=null] : iv_ruleSecondSection= ruleSecondSection EOF ;
    public final EObject entryRuleSecondSection() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSecondSection = null;


        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:593:2: (iv_ruleSecondSection= ruleSecondSection EOF )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:594:2: iv_ruleSecondSection= ruleSecondSection EOF
            {
             newCompositeNode(grammarAccess.getSecondSectionRule()); 
            pushFollow(FOLLOW_ruleSecondSection_in_entryRuleSecondSection1211);
            iv_ruleSecondSection=ruleSecondSection();

            state._fsp--;

             current =iv_ruleSecondSection; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSecondSection1221); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSecondSection"


    // $ANTLR start "ruleSecondSection"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:601:1: ruleSecondSection returns [EObject current=null] : ( ( (lv_resultat12_0_0= ruleRETOUR12 ) ) otherlv_1= ':' ( (lv_resultat1_2_0= ruleRETOUR1 ) ) ) ;
    public final EObject ruleSecondSection() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_resultat12_0_0 = null;

        EObject lv_resultat1_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:604:28: ( ( ( (lv_resultat12_0_0= ruleRETOUR12 ) ) otherlv_1= ':' ( (lv_resultat1_2_0= ruleRETOUR1 ) ) ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:605:1: ( ( (lv_resultat12_0_0= ruleRETOUR12 ) ) otherlv_1= ':' ( (lv_resultat1_2_0= ruleRETOUR1 ) ) )
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:605:1: ( ( (lv_resultat12_0_0= ruleRETOUR12 ) ) otherlv_1= ':' ( (lv_resultat1_2_0= ruleRETOUR1 ) ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:605:2: ( (lv_resultat12_0_0= ruleRETOUR12 ) ) otherlv_1= ':' ( (lv_resultat1_2_0= ruleRETOUR1 ) )
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:605:2: ( (lv_resultat12_0_0= ruleRETOUR12 ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:606:1: (lv_resultat12_0_0= ruleRETOUR12 )
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:606:1: (lv_resultat12_0_0= ruleRETOUR12 )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:607:3: lv_resultat12_0_0= ruleRETOUR12
            {
             
            	        newCompositeNode(grammarAccess.getSecondSectionAccess().getResultat12RETOUR12ParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_ruleRETOUR12_in_ruleSecondSection1267);
            lv_resultat12_0_0=ruleRETOUR12();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSecondSectionRule());
            	        }
                   		set(
                   			current, 
                   			"resultat12",
                    		lv_resultat12_0_0, 
                    		"RETOUR12");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_1=(Token)match(input,17,FOLLOW_17_in_ruleSecondSection1279); 

                	newLeafNode(otherlv_1, grammarAccess.getSecondSectionAccess().getColonKeyword_1());
                
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:627:1: ( (lv_resultat1_2_0= ruleRETOUR1 ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:628:1: (lv_resultat1_2_0= ruleRETOUR1 )
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:628:1: (lv_resultat1_2_0= ruleRETOUR1 )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:629:3: lv_resultat1_2_0= ruleRETOUR1
            {
             
            	        newCompositeNode(grammarAccess.getSecondSectionAccess().getResultat1RETOUR1ParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_ruleRETOUR1_in_ruleSecondSection1300);
            lv_resultat1_2_0=ruleRETOUR1();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getSecondSectionRule());
            	        }
                   		set(
                   			current, 
                   			"resultat1",
                    		lv_resultat1_2_0, 
                    		"RETOUR1");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSecondSection"


    // $ANTLR start "entryRuleRETOUR12"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:653:1: entryRuleRETOUR12 returns [EObject current=null] : iv_ruleRETOUR12= ruleRETOUR12 EOF ;
    public final EObject entryRuleRETOUR12() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRETOUR12 = null;


        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:654:2: (iv_ruleRETOUR12= ruleRETOUR12 EOF )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:655:2: iv_ruleRETOUR12= ruleRETOUR12 EOF
            {
             newCompositeNode(grammarAccess.getRETOUR12Rule()); 
            pushFollow(FOLLOW_ruleRETOUR12_in_entryRuleRETOUR121336);
            iv_ruleRETOUR12=ruleRETOUR12();

            state._fsp--;

             current =iv_ruleRETOUR12; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRETOUR121346); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRETOUR12"


    // $ANTLR start "ruleRETOUR12"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:662:1: ruleRETOUR12 returns [EObject current=null] : ( (lv_name_0_0= RULE_ID ) ) ;
    public final EObject ruleRETOUR12() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;

         enterRule(); 
            
        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:665:28: ( ( (lv_name_0_0= RULE_ID ) ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:666:1: ( (lv_name_0_0= RULE_ID ) )
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:666:1: ( (lv_name_0_0= RULE_ID ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:667:1: (lv_name_0_0= RULE_ID )
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:667:1: (lv_name_0_0= RULE_ID )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:668:3: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleRETOUR121387); 

            			newLeafNode(lv_name_0_0, grammarAccess.getRETOUR12Access().getNameIDTerminalRuleCall_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getRETOUR12Rule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"ID");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRETOUR12"


    // $ANTLR start "entryRuleLastSection"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:692:1: entryRuleLastSection returns [EObject current=null] : iv_ruleLastSection= ruleLastSection EOF ;
    public final EObject entryRuleLastSection() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLastSection = null;


        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:693:2: (iv_ruleLastSection= ruleLastSection EOF )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:694:2: iv_ruleLastSection= ruleLastSection EOF
            {
             newCompositeNode(grammarAccess.getLastSectionRule()); 
            pushFollow(FOLLOW_ruleLastSection_in_entryRuleLastSection1427);
            iv_ruleLastSection=ruleLastSection();

            state._fsp--;

             current =iv_ruleLastSection; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleLastSection1437); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLastSection"


    // $ANTLR start "ruleLastSection"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:701:1: ruleLastSection returns [EObject current=null] : ( ( (lv_resultat_0_0= ruleRETOUR ) ) otherlv_1= ':' ( ( (lv_Debut1_2_0= ruleDebut ) ) ( (lv_resultat4_3_0= ruleRETOUR4 ) ) ( (lv_Milieu1_4_0= ruleMilieu ) ) ( (lv_Fin1_5_0= ruleFin ) ) )* ) ;
    public final EObject ruleLastSection() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_resultat_0_0 = null;

        EObject lv_Debut1_2_0 = null;

        AntlrDatatypeRuleToken lv_resultat4_3_0 = null;

        EObject lv_Milieu1_4_0 = null;

        EObject lv_Fin1_5_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:704:28: ( ( ( (lv_resultat_0_0= ruleRETOUR ) ) otherlv_1= ':' ( ( (lv_Debut1_2_0= ruleDebut ) ) ( (lv_resultat4_3_0= ruleRETOUR4 ) ) ( (lv_Milieu1_4_0= ruleMilieu ) ) ( (lv_Fin1_5_0= ruleFin ) ) )* ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:705:1: ( ( (lv_resultat_0_0= ruleRETOUR ) ) otherlv_1= ':' ( ( (lv_Debut1_2_0= ruleDebut ) ) ( (lv_resultat4_3_0= ruleRETOUR4 ) ) ( (lv_Milieu1_4_0= ruleMilieu ) ) ( (lv_Fin1_5_0= ruleFin ) ) )* )
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:705:1: ( ( (lv_resultat_0_0= ruleRETOUR ) ) otherlv_1= ':' ( ( (lv_Debut1_2_0= ruleDebut ) ) ( (lv_resultat4_3_0= ruleRETOUR4 ) ) ( (lv_Milieu1_4_0= ruleMilieu ) ) ( (lv_Fin1_5_0= ruleFin ) ) )* )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:705:2: ( (lv_resultat_0_0= ruleRETOUR ) ) otherlv_1= ':' ( ( (lv_Debut1_2_0= ruleDebut ) ) ( (lv_resultat4_3_0= ruleRETOUR4 ) ) ( (lv_Milieu1_4_0= ruleMilieu ) ) ( (lv_Fin1_5_0= ruleFin ) ) )*
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:705:2: ( (lv_resultat_0_0= ruleRETOUR ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:706:1: (lv_resultat_0_0= ruleRETOUR )
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:706:1: (lv_resultat_0_0= ruleRETOUR )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:707:3: lv_resultat_0_0= ruleRETOUR
            {
             
            	        newCompositeNode(grammarAccess.getLastSectionAccess().getResultatRETOURParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_ruleRETOUR_in_ruleLastSection1483);
            lv_resultat_0_0=ruleRETOUR();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getLastSectionRule());
            	        }
                   		set(
                   			current, 
                   			"resultat",
                    		lv_resultat_0_0, 
                    		"RETOUR");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_1=(Token)match(input,17,FOLLOW_17_in_ruleLastSection1495); 

                	newLeafNode(otherlv_1, grammarAccess.getLastSectionAccess().getColonKeyword_1());
                
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:727:1: ( ( (lv_Debut1_2_0= ruleDebut ) ) ( (lv_resultat4_3_0= ruleRETOUR4 ) ) ( (lv_Milieu1_4_0= ruleMilieu ) ) ( (lv_Fin1_5_0= ruleFin ) ) )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==RULE_ID||(LA9_0>=15 && LA9_0<=17)||(LA9_0>=22 && LA9_0<=31)) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:727:2: ( (lv_Debut1_2_0= ruleDebut ) ) ( (lv_resultat4_3_0= ruleRETOUR4 ) ) ( (lv_Milieu1_4_0= ruleMilieu ) ) ( (lv_Fin1_5_0= ruleFin ) )
            	    {
            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:727:2: ( (lv_Debut1_2_0= ruleDebut ) )
            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:728:1: (lv_Debut1_2_0= ruleDebut )
            	    {
            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:728:1: (lv_Debut1_2_0= ruleDebut )
            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:729:3: lv_Debut1_2_0= ruleDebut
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getLastSectionAccess().getDebut1DebutParserRuleCall_2_0_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleDebut_in_ruleLastSection1517);
            	    lv_Debut1_2_0=ruleDebut();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getLastSectionRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"Debut1",
            	            		lv_Debut1_2_0, 
            	            		"Debut");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }

            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:745:2: ( (lv_resultat4_3_0= ruleRETOUR4 ) )
            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:746:1: (lv_resultat4_3_0= ruleRETOUR4 )
            	    {
            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:746:1: (lv_resultat4_3_0= ruleRETOUR4 )
            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:747:3: lv_resultat4_3_0= ruleRETOUR4
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getLastSectionAccess().getResultat4RETOUR4ParserRuleCall_2_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleRETOUR4_in_ruleLastSection1538);
            	    lv_resultat4_3_0=ruleRETOUR4();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getLastSectionRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"resultat4",
            	            		lv_resultat4_3_0, 
            	            		"RETOUR4");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }

            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:763:2: ( (lv_Milieu1_4_0= ruleMilieu ) )
            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:764:1: (lv_Milieu1_4_0= ruleMilieu )
            	    {
            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:764:1: (lv_Milieu1_4_0= ruleMilieu )
            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:765:3: lv_Milieu1_4_0= ruleMilieu
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getLastSectionAccess().getMilieu1MilieuParserRuleCall_2_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleMilieu_in_ruleLastSection1559);
            	    lv_Milieu1_4_0=ruleMilieu();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getLastSectionRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"Milieu1",
            	            		lv_Milieu1_4_0, 
            	            		"Milieu");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }

            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:781:2: ( (lv_Fin1_5_0= ruleFin ) )
            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:782:1: (lv_Fin1_5_0= ruleFin )
            	    {
            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:782:1: (lv_Fin1_5_0= ruleFin )
            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:783:3: lv_Fin1_5_0= ruleFin
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getLastSectionAccess().getFin1FinParserRuleCall_2_3_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleFin_in_ruleLastSection1580);
            	    lv_Fin1_5_0=ruleFin();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getLastSectionRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"Fin1",
            	            		lv_Fin1_5_0, 
            	            		"Fin");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLastSection"


    // $ANTLR start "entryRuleFin"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:807:1: entryRuleFin returns [EObject current=null] : iv_ruleFin= ruleFin EOF ;
    public final EObject entryRuleFin() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFin = null;


        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:808:2: (iv_ruleFin= ruleFin EOF )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:809:2: iv_ruleFin= ruleFin EOF
            {
             newCompositeNode(grammarAccess.getFinRule()); 
            pushFollow(FOLLOW_ruleFin_in_entryRuleFin1618);
            iv_ruleFin=ruleFin();

            state._fsp--;

             current =iv_ruleFin; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleFin1628); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFin"


    // $ANTLR start "ruleFin"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:816:1: ruleFin returns [EObject current=null] : ( (lv_name_0_0= RULE_ID ) )* ;
    public final EObject ruleFin() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;

         enterRule(); 
            
        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:819:28: ( ( (lv_name_0_0= RULE_ID ) )* )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:820:1: ( (lv_name_0_0= RULE_ID ) )*
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:820:1: ( (lv_name_0_0= RULE_ID ) )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==RULE_ID) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:821:1: (lv_name_0_0= RULE_ID )
            	    {
            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:821:1: (lv_name_0_0= RULE_ID )
            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:822:3: lv_name_0_0= RULE_ID
            	    {
            	    lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleFin1669); 

            	    			newLeafNode(lv_name_0_0, grammarAccess.getFinAccess().getNameIDTerminalRuleCall_0()); 
            	    		

            	    	        if (current==null) {
            	    	            current = createModelElement(grammarAccess.getFinRule());
            	    	        }
            	           		setWithLastConsumed(
            	           			current, 
            	           			"name",
            	            		lv_name_0_0, 
            	            		"ID");
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFin"


    // $ANTLR start "entryRuleMilieu"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:846:1: entryRuleMilieu returns [EObject current=null] : iv_ruleMilieu= ruleMilieu EOF ;
    public final EObject entryRuleMilieu() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMilieu = null;


        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:847:2: (iv_ruleMilieu= ruleMilieu EOF )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:848:2: iv_ruleMilieu= ruleMilieu EOF
            {
             newCompositeNode(grammarAccess.getMilieuRule()); 
            pushFollow(FOLLOW_ruleMilieu_in_entryRuleMilieu1710);
            iv_ruleMilieu=ruleMilieu();

            state._fsp--;

             current =iv_ruleMilieu; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleMilieu1720); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMilieu"


    // $ANTLR start "ruleMilieu"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:855:1: ruleMilieu returns [EObject current=null] : ( (lv_name_0_0= RULE_INT ) )? ;
    public final EObject ruleMilieu() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;

         enterRule(); 
            
        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:858:28: ( ( (lv_name_0_0= RULE_INT ) )? )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:859:1: ( (lv_name_0_0= RULE_INT ) )?
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:859:1: ( (lv_name_0_0= RULE_INT ) )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==RULE_INT) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:860:1: (lv_name_0_0= RULE_INT )
                    {
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:860:1: (lv_name_0_0= RULE_INT )
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:861:3: lv_name_0_0= RULE_INT
                    {
                    lv_name_0_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleMilieu1761); 

                    			newLeafNode(lv_name_0_0, grammarAccess.getMilieuAccess().getNameINTTerminalRuleCall_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getMilieuRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"name",
                            		lv_name_0_0, 
                            		"INT");
                    	    

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMilieu"


    // $ANTLR start "entryRuleRETOUR4"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:885:1: entryRuleRETOUR4 returns [String current=null] : iv_ruleRETOUR4= ruleRETOUR4 EOF ;
    public final String entryRuleRETOUR4() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleRETOUR4 = null;


        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:886:2: (iv_ruleRETOUR4= ruleRETOUR4 EOF )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:887:2: iv_ruleRETOUR4= ruleRETOUR4 EOF
            {
             newCompositeNode(grammarAccess.getRETOUR4Rule()); 
            pushFollow(FOLLOW_ruleRETOUR4_in_entryRuleRETOUR41803);
            iv_ruleRETOUR4=ruleRETOUR4();

            state._fsp--;

             current =iv_ruleRETOUR4.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRETOUR41814); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRETOUR4"


    // $ANTLR start "ruleRETOUR4"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:894:1: ruleRETOUR4 returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '>' | kw= '<' | kw= '=' | kw= '+' | kw= '.' | kw= ':' | kw= '@pre' | kw= '.end' | kw= '(' | kw= ')' | kw= 'and' | kw= '-' | kw= '@' ) ;
    public final AntlrDatatypeRuleToken ruleRETOUR4() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:897:28: ( (kw= '>' | kw= '<' | kw= '=' | kw= '+' | kw= '.' | kw= ':' | kw= '@pre' | kw= '.end' | kw= '(' | kw= ')' | kw= 'and' | kw= '-' | kw= '@' ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:898:1: (kw= '>' | kw= '<' | kw= '=' | kw= '+' | kw= '.' | kw= ':' | kw= '@pre' | kw= '.end' | kw= '(' | kw= ')' | kw= 'and' | kw= '-' | kw= '@' )
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:898:1: (kw= '>' | kw= '<' | kw= '=' | kw= '+' | kw= '.' | kw= ':' | kw= '@pre' | kw= '.end' | kw= '(' | kw= ')' | kw= 'and' | kw= '-' | kw= '@' )
            int alt12=13;
            switch ( input.LA(1) ) {
            case 22:
                {
                alt12=1;
                }
                break;
            case 23:
                {
                alt12=2;
                }
                break;
            case 24:
                {
                alt12=3;
                }
                break;
            case 25:
                {
                alt12=4;
                }
                break;
            case 26:
                {
                alt12=5;
                }
                break;
            case 17:
                {
                alt12=6;
                }
                break;
            case 27:
                {
                alt12=7;
                }
                break;
            case 28:
                {
                alt12=8;
                }
                break;
            case 15:
                {
                alt12=9;
                }
                break;
            case 16:
                {
                alt12=10;
                }
                break;
            case 29:
                {
                alt12=11;
                }
                break;
            case 30:
                {
                alt12=12;
                }
                break;
            case 31:
                {
                alt12=13;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }

            switch (alt12) {
                case 1 :
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:899:2: kw= '>'
                    {
                    kw=(Token)match(input,22,FOLLOW_22_in_ruleRETOUR41852); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getRETOUR4Access().getGreaterThanSignKeyword_0()); 
                        

                    }
                    break;
                case 2 :
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:906:2: kw= '<'
                    {
                    kw=(Token)match(input,23,FOLLOW_23_in_ruleRETOUR41871); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getRETOUR4Access().getLessThanSignKeyword_1()); 
                        

                    }
                    break;
                case 3 :
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:913:2: kw= '='
                    {
                    kw=(Token)match(input,24,FOLLOW_24_in_ruleRETOUR41890); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getRETOUR4Access().getEqualsSignKeyword_2()); 
                        

                    }
                    break;
                case 4 :
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:920:2: kw= '+'
                    {
                    kw=(Token)match(input,25,FOLLOW_25_in_ruleRETOUR41909); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getRETOUR4Access().getPlusSignKeyword_3()); 
                        

                    }
                    break;
                case 5 :
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:927:2: kw= '.'
                    {
                    kw=(Token)match(input,26,FOLLOW_26_in_ruleRETOUR41928); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getRETOUR4Access().getFullStopKeyword_4()); 
                        

                    }
                    break;
                case 6 :
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:934:2: kw= ':'
                    {
                    kw=(Token)match(input,17,FOLLOW_17_in_ruleRETOUR41947); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getRETOUR4Access().getColonKeyword_5()); 
                        

                    }
                    break;
                case 7 :
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:941:2: kw= '@pre'
                    {
                    kw=(Token)match(input,27,FOLLOW_27_in_ruleRETOUR41966); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getRETOUR4Access().getPreKeyword_6()); 
                        

                    }
                    break;
                case 8 :
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:948:2: kw= '.end'
                    {
                    kw=(Token)match(input,28,FOLLOW_28_in_ruleRETOUR41985); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getRETOUR4Access().getEndKeyword_7()); 
                        

                    }
                    break;
                case 9 :
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:955:2: kw= '('
                    {
                    kw=(Token)match(input,15,FOLLOW_15_in_ruleRETOUR42004); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getRETOUR4Access().getLeftParenthesisKeyword_8()); 
                        

                    }
                    break;
                case 10 :
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:962:2: kw= ')'
                    {
                    kw=(Token)match(input,16,FOLLOW_16_in_ruleRETOUR42023); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getRETOUR4Access().getRightParenthesisKeyword_9()); 
                        

                    }
                    break;
                case 11 :
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:969:2: kw= 'and'
                    {
                    kw=(Token)match(input,29,FOLLOW_29_in_ruleRETOUR42042); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getRETOUR4Access().getAndKeyword_10()); 
                        

                    }
                    break;
                case 12 :
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:976:2: kw= '-'
                    {
                    kw=(Token)match(input,30,FOLLOW_30_in_ruleRETOUR42061); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getRETOUR4Access().getHyphenMinusKeyword_11()); 
                        

                    }
                    break;
                case 13 :
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:983:2: kw= '@'
                    {
                    kw=(Token)match(input,31,FOLLOW_31_in_ruleRETOUR42080); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getRETOUR4Access().getCommercialAtKeyword_12()); 
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRETOUR4"


    // $ANTLR start "entryRuleRETOUR"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:996:1: entryRuleRETOUR returns [String current=null] : iv_ruleRETOUR= ruleRETOUR EOF ;
    public final String entryRuleRETOUR() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleRETOUR = null;


        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:997:2: (iv_ruleRETOUR= ruleRETOUR EOF )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:998:2: iv_ruleRETOUR= ruleRETOUR EOF
            {
             newCompositeNode(grammarAccess.getRETOURRule()); 
            pushFollow(FOLLOW_ruleRETOUR_in_entryRuleRETOUR2121);
            iv_ruleRETOUR=ruleRETOUR();

            state._fsp--;

             current =iv_ruleRETOUR.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRETOUR2132); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRETOUR"


    // $ANTLR start "ruleRETOUR"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1005:1: ruleRETOUR returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'post' | kw= 'pre' | kw= 'inv' | kw= 'deff' ) ;
    public final AntlrDatatypeRuleToken ruleRETOUR() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1008:28: ( (kw= 'post' | kw= 'pre' | kw= 'inv' | kw= 'deff' ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1009:1: (kw= 'post' | kw= 'pre' | kw= 'inv' | kw= 'deff' )
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1009:1: (kw= 'post' | kw= 'pre' | kw= 'inv' | kw= 'deff' )
            int alt13=4;
            switch ( input.LA(1) ) {
            case 32:
                {
                alt13=1;
                }
                break;
            case 33:
                {
                alt13=2;
                }
                break;
            case 34:
                {
                alt13=3;
                }
                break;
            case 35:
                {
                alt13=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }

            switch (alt13) {
                case 1 :
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1010:2: kw= 'post'
                    {
                    kw=(Token)match(input,32,FOLLOW_32_in_ruleRETOUR2170); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getRETOURAccess().getPostKeyword_0()); 
                        

                    }
                    break;
                case 2 :
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1017:2: kw= 'pre'
                    {
                    kw=(Token)match(input,33,FOLLOW_33_in_ruleRETOUR2189); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getRETOURAccess().getPreKeyword_1()); 
                        

                    }
                    break;
                case 3 :
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1024:2: kw= 'inv'
                    {
                    kw=(Token)match(input,34,FOLLOW_34_in_ruleRETOUR2208); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getRETOURAccess().getInvKeyword_2()); 
                        

                    }
                    break;
                case 4 :
                    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1031:2: kw= 'deff'
                    {
                    kw=(Token)match(input,35,FOLLOW_35_in_ruleRETOUR2227); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getRETOURAccess().getDeffKeyword_3()); 
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRETOUR"


    // $ANTLR start "entryRuleDebut"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1044:1: entryRuleDebut returns [EObject current=null] : iv_ruleDebut= ruleDebut EOF ;
    public final EObject entryRuleDebut() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDebut = null;


        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1045:2: (iv_ruleDebut= ruleDebut EOF )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1046:2: iv_ruleDebut= ruleDebut EOF
            {
             newCompositeNode(grammarAccess.getDebutRule()); 
            pushFollow(FOLLOW_ruleDebut_in_entryRuleDebut2267);
            iv_ruleDebut=ruleDebut();

            state._fsp--;

             current =iv_ruleDebut; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDebut2277); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDebut"


    // $ANTLR start "ruleDebut"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1053:1: ruleDebut returns [EObject current=null] : ( (lv_name_0_0= RULE_ID ) )* ;
    public final EObject ruleDebut() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;

         enterRule(); 
            
        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1056:28: ( ( (lv_name_0_0= RULE_ID ) )* )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1057:1: ( (lv_name_0_0= RULE_ID ) )*
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1057:1: ( (lv_name_0_0= RULE_ID ) )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==RULE_ID) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1058:1: (lv_name_0_0= RULE_ID )
            	    {
            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1058:1: (lv_name_0_0= RULE_ID )
            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1059:3: lv_name_0_0= RULE_ID
            	    {
            	    lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleDebut2318); 

            	    			newLeafNode(lv_name_0_0, grammarAccess.getDebutAccess().getNameIDTerminalRuleCall_0()); 
            	    		

            	    	        if (current==null) {
            	    	            current = createModelElement(grammarAccess.getDebutRule());
            	    	        }
            	           		setWithLastConsumed(
            	           			current, 
            	           			"name",
            	            		lv_name_0_0, 
            	            		"ID");
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDebut"


    // $ANTLR start "entryRuledeclaration_sentence"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1083:1: entryRuledeclaration_sentence returns [EObject current=null] : iv_ruledeclaration_sentence= ruledeclaration_sentence EOF ;
    public final EObject entryRuledeclaration_sentence() throws RecognitionException {
        EObject current = null;

        EObject iv_ruledeclaration_sentence = null;


        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1084:2: (iv_ruledeclaration_sentence= ruledeclaration_sentence EOF )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1085:2: iv_ruledeclaration_sentence= ruledeclaration_sentence EOF
            {
             newCompositeNode(grammarAccess.getDeclaration_sentenceRule()); 
            pushFollow(FOLLOW_ruledeclaration_sentence_in_entryRuledeclaration_sentence2359);
            iv_ruledeclaration_sentence=ruledeclaration_sentence();

            state._fsp--;

             current =iv_ruledeclaration_sentence; 
            match(input,EOF,FOLLOW_EOF_in_entryRuledeclaration_sentence2369); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuledeclaration_sentence"


    // $ANTLR start "ruledeclaration_sentence"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1092:1: ruledeclaration_sentence returns [EObject current=null] : (otherlv_0= 'import' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'http://' ( (lv_deux_3_0= ruleQualified ) ) ( (lv_trois_4_0= ruleQualified1 ) ) ) ;
    public final EObject ruledeclaration_sentence() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        AntlrDatatypeRuleToken lv_deux_3_0 = null;

        AntlrDatatypeRuleToken lv_trois_4_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1095:28: ( (otherlv_0= 'import' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'http://' ( (lv_deux_3_0= ruleQualified ) ) ( (lv_trois_4_0= ruleQualified1 ) ) ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1096:1: (otherlv_0= 'import' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'http://' ( (lv_deux_3_0= ruleQualified ) ) ( (lv_trois_4_0= ruleQualified1 ) ) )
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1096:1: (otherlv_0= 'import' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'http://' ( (lv_deux_3_0= ruleQualified ) ) ( (lv_trois_4_0= ruleQualified1 ) ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1096:3: otherlv_0= 'import' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'http://' ( (lv_deux_3_0= ruleQualified ) ) ( (lv_trois_4_0= ruleQualified1 ) )
            {
            otherlv_0=(Token)match(input,36,FOLLOW_36_in_ruledeclaration_sentence2406); 

                	newLeafNode(otherlv_0, grammarAccess.getDeclaration_sentenceAccess().getImportKeyword_0());
                
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1100:1: ( (lv_name_1_0= RULE_ID ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1101:1: (lv_name_1_0= RULE_ID )
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1101:1: (lv_name_1_0= RULE_ID )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1102:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruledeclaration_sentence2423); 

            			newLeafNode(lv_name_1_0, grammarAccess.getDeclaration_sentenceAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getDeclaration_sentenceRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,37,FOLLOW_37_in_ruledeclaration_sentence2440); 

                	newLeafNode(otherlv_2, grammarAccess.getDeclaration_sentenceAccess().getHttpKeyword_2());
                
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1122:1: ( (lv_deux_3_0= ruleQualified ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1123:1: (lv_deux_3_0= ruleQualified )
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1123:1: (lv_deux_3_0= ruleQualified )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1124:3: lv_deux_3_0= ruleQualified
            {
             
            	        newCompositeNode(grammarAccess.getDeclaration_sentenceAccess().getDeuxQualifiedParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_ruleQualified_in_ruledeclaration_sentence2461);
            lv_deux_3_0=ruleQualified();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getDeclaration_sentenceRule());
            	        }
                   		set(
                   			current, 
                   			"deux",
                    		lv_deux_3_0, 
                    		"Qualified");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1140:2: ( (lv_trois_4_0= ruleQualified1 ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1141:1: (lv_trois_4_0= ruleQualified1 )
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1141:1: (lv_trois_4_0= ruleQualified1 )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1142:3: lv_trois_4_0= ruleQualified1
            {
             
            	        newCompositeNode(grammarAccess.getDeclaration_sentenceAccess().getTroisQualified1ParserRuleCall_4_0()); 
            	    
            pushFollow(FOLLOW_ruleQualified1_in_ruledeclaration_sentence2482);
            lv_trois_4_0=ruleQualified1();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getDeclaration_sentenceRule());
            	        }
                   		set(
                   			current, 
                   			"trois",
                    		lv_trois_4_0, 
                    		"Qualified1");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruledeclaration_sentence"


    // $ANTLR start "entryRuleQualified1"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1166:1: entryRuleQualified1 returns [String current=null] : iv_ruleQualified1= ruleQualified1 EOF ;
    public final String entryRuleQualified1() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualified1 = null;


        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1167:2: (iv_ruleQualified1= ruleQualified1 EOF )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1168:2: iv_ruleQualified1= ruleQualified1 EOF
            {
             newCompositeNode(grammarAccess.getQualified1Rule()); 
            pushFollow(FOLLOW_ruleQualified1_in_entryRuleQualified12519);
            iv_ruleQualified1=ruleQualified1();

            state._fsp--;

             current =iv_ruleQualified1.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQualified12530); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualified1"


    // $ANTLR start "ruleQualified1"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1175:1: ruleQualified1 returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '/' (this_ID_1= RULE_ID kw= '/' )* ) ;
    public final AntlrDatatypeRuleToken ruleQualified1() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_ID_1=null;

         enterRule(); 
            
        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1178:28: ( (kw= '/' (this_ID_1= RULE_ID kw= '/' )* ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1179:1: (kw= '/' (this_ID_1= RULE_ID kw= '/' )* )
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1179:1: (kw= '/' (this_ID_1= RULE_ID kw= '/' )* )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1180:2: kw= '/' (this_ID_1= RULE_ID kw= '/' )*
            {
            kw=(Token)match(input,38,FOLLOW_38_in_ruleQualified12568); 

                    current.merge(kw);
                    newLeafNode(kw, grammarAccess.getQualified1Access().getSolidusKeyword_0()); 
                
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1185:1: (this_ID_1= RULE_ID kw= '/' )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==RULE_ID) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1185:6: this_ID_1= RULE_ID kw= '/'
            	    {
            	    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleQualified12584); 

            	    		current.merge(this_ID_1);
            	        
            	     
            	        newLeafNode(this_ID_1, grammarAccess.getQualified1Access().getIDTerminalRuleCall_1_0()); 
            	        
            	    kw=(Token)match(input,38,FOLLOW_38_in_ruleQualified12602); 

            	            current.merge(kw);
            	            newLeafNode(kw, grammarAccess.getQualified1Access().getSolidusKeyword_1_1()); 
            	        

            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualified1"


    // $ANTLR start "entryRuleQualified"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1206:1: entryRuleQualified returns [String current=null] : iv_ruleQualified= ruleQualified EOF ;
    public final String entryRuleQualified() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualified = null;


        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1207:2: (iv_ruleQualified= ruleQualified EOF )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1208:2: iv_ruleQualified= ruleQualified EOF
            {
             newCompositeNode(grammarAccess.getQualifiedRule()); 
            pushFollow(FOLLOW_ruleQualified_in_entryRuleQualified2645);
            iv_ruleQualified=ruleQualified();

            state._fsp--;

             current =iv_ruleQualified.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQualified2656); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualified"


    // $ANTLR start "ruleQualified"
    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1215:1: ruleQualified returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID kw= '.' (this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleQualified() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;

         enterRule(); 
            
        try {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1218:28: ( (this_ID_0= RULE_ID kw= '.' (this_ID_2= RULE_ID )* ) )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1219:1: (this_ID_0= RULE_ID kw= '.' (this_ID_2= RULE_ID )* )
            {
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1219:1: (this_ID_0= RULE_ID kw= '.' (this_ID_2= RULE_ID )* )
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1219:6: this_ID_0= RULE_ID kw= '.' (this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleQualified2696); 

            		current.merge(this_ID_0);
                
             
                newLeafNode(this_ID_0, grammarAccess.getQualifiedAccess().getIDTerminalRuleCall_0()); 
                
            kw=(Token)match(input,26,FOLLOW_26_in_ruleQualified2714); 

                    current.merge(kw);
                    newLeafNode(kw, grammarAccess.getQualifiedAccess().getFullStopKeyword_1()); 
                
            // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1232:1: (this_ID_2= RULE_ID )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==RULE_ID) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // ../org.xtext.example.myocl2/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyOcl2.g:1232:6: this_ID_2= RULE_ID
            	    {
            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleQualified2730); 

            	    		current.merge(this_ID_2);
            	        
            	     
            	        newLeafNode(this_ID_2, grammarAccess.getQualifiedAccess().getIDTerminalRuleCall_2()); 
            	        

            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualified"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleMain_in_entryRuleMain75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMain85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruledeclaration_sentence_in_ruleMain131 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_rulepackage_declaration_in_ruleMain153 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulepackage_declaration_in_entryRulepackage_declaration189 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulepackage_declaration199 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_rulepackage_declaration236 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleendstartelement_in_rulepackage_declaration257 = new BitSet(new long[]{0x0000000000003000L});
    public static final BitSet FOLLOW_ruleabstractelement_in_rulepackage_declaration278 = new BitSet(new long[]{0x0000000000003000L});
    public static final BitSet FOLLOW_12_in_rulepackage_declaration291 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleendstartelement_in_rulepackage_declaration312 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleendstartelement_in_entryRuleendstartelement348 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleendstartelement358 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleendstartelement399 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleabstractelement_in_entryRuleabstractelement439 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleabstractelement449 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulecontext_declaration_in_ruleabstractelement494 = new BitSet(new long[]{0x0000000000002002L});
    public static final BitSet FOLLOW_rulecontext_declaration_in_entryRulecontext_declaration530 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulecontext_declaration540 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rulecontext_declaration577 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_rulecontext_declaration594 = new BitSet(new long[]{0x0000000F00004002L});
    public static final BitSet FOLLOW_ruleMiddleSection_in_rulecontext_declaration620 = new BitSet(new long[]{0x0000000F00000002L});
    public static final BitSet FOLLOW_ruleLastSection_in_rulecontext_declaration642 = new BitSet(new long[]{0x0000000F00000002L});
    public static final BitSet FOLLOW_ruleMiddleSection_in_entryRuleMiddleSection679 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMiddleSection689 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_ruleMiddleSection726 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleFirstSection_in_ruleMiddleSection747 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_ruleMiddleSection759 = new BitSet(new long[]{0x0000000000010010L});
    public static final BitSet FOLLOW_ruleSecondSection_in_ruleMiddleSection780 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_ruleMiddleSection793 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_ruleThirdSection_in_ruleMiddleSection814 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_ruleThirdSection_in_entryRuleThirdSection851 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleThirdSection861 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_ruleThirdSection898 = new BitSet(new long[]{0x00000000003C0010L});
    public static final BitSet FOLLOW_ruleRETOUR1_in_ruleThirdSection919 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRETOUR1_in_entryRuleRETOUR1955 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRETOUR1965 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_ruleRETOUR11002 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_ruleRETOUR11020 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_ruleRETOUR11038 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_ruleRETOUR11056 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleRETOUR11079 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFirstSection_in_entryRuleFirstSection1120 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFirstSection1130 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleFirstSection1171 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSecondSection_in_entryRuleSecondSection1211 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSecondSection1221 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRETOUR12_in_ruleSecondSection1267 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleSecondSection1279 = new BitSet(new long[]{0x00000000003C0010L});
    public static final BitSet FOLLOW_ruleRETOUR1_in_ruleSecondSection1300 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRETOUR12_in_entryRuleRETOUR121336 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRETOUR121346 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleRETOUR121387 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLastSection_in_entryRuleLastSection1427 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLastSection1437 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRETOUR_in_ruleLastSection1483 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleLastSection1495 = new BitSet(new long[]{0x00000000FFC38012L});
    public static final BitSet FOLLOW_ruleDebut_in_ruleLastSection1517 = new BitSet(new long[]{0x00000000FFC38010L});
    public static final BitSet FOLLOW_ruleRETOUR4_in_ruleLastSection1538 = new BitSet(new long[]{0x00000000FFC38030L});
    public static final BitSet FOLLOW_ruleMilieu_in_ruleLastSection1559 = new BitSet(new long[]{0x00000000FFC38010L});
    public static final BitSet FOLLOW_ruleFin_in_ruleLastSection1580 = new BitSet(new long[]{0x00000000FFC38012L});
    public static final BitSet FOLLOW_ruleFin_in_entryRuleFin1618 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFin1628 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleFin1669 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_ruleMilieu_in_entryRuleMilieu1710 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMilieu1720 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleMilieu1761 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRETOUR4_in_entryRuleRETOUR41803 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRETOUR41814 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_ruleRETOUR41852 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_ruleRETOUR41871 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_ruleRETOUR41890 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_ruleRETOUR41909 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_ruleRETOUR41928 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_ruleRETOUR41947 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_ruleRETOUR41966 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_28_in_ruleRETOUR41985 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_ruleRETOUR42004 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_ruleRETOUR42023 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_29_in_ruleRETOUR42042 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_ruleRETOUR42061 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_ruleRETOUR42080 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRETOUR_in_entryRuleRETOUR2121 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRETOUR2132 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_ruleRETOUR2170 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_33_in_ruleRETOUR2189 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_34_in_ruleRETOUR2208 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_35_in_ruleRETOUR2227 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDebut_in_entryRuleDebut2267 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDebut2277 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleDebut2318 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_ruledeclaration_sentence_in_entryRuledeclaration_sentence2359 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuledeclaration_sentence2369 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_36_in_ruledeclaration_sentence2406 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruledeclaration_sentence2423 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_37_in_ruledeclaration_sentence2440 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleQualified_in_ruledeclaration_sentence2461 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_ruleQualified1_in_ruledeclaration_sentence2482 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualified1_in_entryRuleQualified12519 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQualified12530 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_38_in_ruleQualified12568 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleQualified12584 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_38_in_ruleQualified12602 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_ruleQualified_in_entryRuleQualified2645 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQualified2656 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleQualified2696 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_26_in_ruleQualified2714 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleQualified2730 = new BitSet(new long[]{0x0000000000000012L});

}