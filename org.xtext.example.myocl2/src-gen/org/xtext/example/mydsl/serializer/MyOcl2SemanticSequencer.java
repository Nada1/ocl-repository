package org.xtext.example.mydsl.serializer;

import com.google.inject.Inject;
import com.google.inject.Provider;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.serializer.acceptor.ISemanticSequenceAcceptor;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.diagnostic.ISemanticSequencerDiagnosticProvider;
import org.eclipse.xtext.serializer.diagnostic.ISerializationDiagnostic.Acceptor;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.GenericSequencer;
import org.eclipse.xtext.serializer.sequencer.ISemanticNodeProvider.INodesForEObjectProvider;
import org.eclipse.xtext.serializer.sequencer.ISemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;
import org.xtext.example.mydsl.myOcl2.Debut;
import org.xtext.example.mydsl.myOcl2.Fin;
import org.xtext.example.mydsl.myOcl2.FirstSection;
import org.xtext.example.mydsl.myOcl2.LastSection;
import org.xtext.example.mydsl.myOcl2.Main;
import org.xtext.example.mydsl.myOcl2.MiddleSection;
import org.xtext.example.mydsl.myOcl2.Milieu;
import org.xtext.example.mydsl.myOcl2.MyOcl2Package;
import org.xtext.example.mydsl.myOcl2.RETOUR1;
import org.xtext.example.mydsl.myOcl2.RETOUR12;
import org.xtext.example.mydsl.myOcl2.SecondSection;
import org.xtext.example.mydsl.myOcl2.ThirdSection;
import org.xtext.example.mydsl.myOcl2.abstractelement;
import org.xtext.example.mydsl.myOcl2.context_declaration;
import org.xtext.example.mydsl.myOcl2.declaration_sentence;
import org.xtext.example.mydsl.myOcl2.endstartelement;
import org.xtext.example.mydsl.myOcl2.package_declaration;
import org.xtext.example.mydsl.services.MyOcl2GrammarAccess;

@SuppressWarnings("all")
public class MyOcl2SemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private MyOcl2GrammarAccess grammarAccess;
	
	public void createSequence(EObject context, EObject semanticObject) {
		if(semanticObject.eClass().getEPackage() == MyOcl2Package.eINSTANCE) switch(semanticObject.eClass().getClassifierID()) {
			case MyOcl2Package.DEBUT:
				if(context == grammarAccess.getDebutRule()) {
					sequence_Debut(context, (Debut) semanticObject); 
					return; 
				}
				else break;
			case MyOcl2Package.FIN:
				if(context == grammarAccess.getFinRule()) {
					sequence_Fin(context, (Fin) semanticObject); 
					return; 
				}
				else break;
			case MyOcl2Package.FIRST_SECTION:
				if(context == grammarAccess.getFirstSectionRule()) {
					sequence_FirstSection(context, (FirstSection) semanticObject); 
					return; 
				}
				else break;
			case MyOcl2Package.LAST_SECTION:
				if(context == grammarAccess.getLastSectionRule()) {
					sequence_LastSection(context, (LastSection) semanticObject); 
					return; 
				}
				else break;
			case MyOcl2Package.MAIN:
				if(context == grammarAccess.getMainRule()) {
					sequence_Main(context, (Main) semanticObject); 
					return; 
				}
				else break;
			case MyOcl2Package.MIDDLE_SECTION:
				if(context == grammarAccess.getMiddleSectionRule()) {
					sequence_MiddleSection(context, (MiddleSection) semanticObject); 
					return; 
				}
				else break;
			case MyOcl2Package.MILIEU:
				if(context == grammarAccess.getMilieuRule()) {
					sequence_Milieu(context, (Milieu) semanticObject); 
					return; 
				}
				else break;
			case MyOcl2Package.RETOUR1:
				if(context == grammarAccess.getRETOUR1Rule()) {
					sequence_RETOUR1(context, (RETOUR1) semanticObject); 
					return; 
				}
				else break;
			case MyOcl2Package.RETOUR12:
				if(context == grammarAccess.getRETOUR12Rule()) {
					sequence_RETOUR12(context, (RETOUR12) semanticObject); 
					return; 
				}
				else break;
			case MyOcl2Package.SECOND_SECTION:
				if(context == grammarAccess.getSecondSectionRule()) {
					sequence_SecondSection(context, (SecondSection) semanticObject); 
					return; 
				}
				else break;
			case MyOcl2Package.THIRD_SECTION:
				if(context == grammarAccess.getThirdSectionRule()) {
					sequence_ThirdSection(context, (ThirdSection) semanticObject); 
					return; 
				}
				else break;
			case MyOcl2Package.ABSTRACTELEMENT:
				if(context == grammarAccess.getAbstractelementRule()) {
					sequence_abstractelement(context, (abstractelement) semanticObject); 
					return; 
				}
				else break;
			case MyOcl2Package.CONTEXT_DECLARATION:
				if(context == grammarAccess.getContext_declarationRule()) {
					sequence_context_declaration(context, (context_declaration) semanticObject); 
					return; 
				}
				else break;
			case MyOcl2Package.DECLARATION_SENTENCE:
				if(context == grammarAccess.getDeclaration_sentenceRule()) {
					sequence_declaration_sentence(context, (declaration_sentence) semanticObject); 
					return; 
				}
				else break;
			case MyOcl2Package.ENDSTARTELEMENT:
				if(context == grammarAccess.getEndstartelementRule()) {
					sequence_endstartelement(context, (endstartelement) semanticObject); 
					return; 
				}
				else break;
			case MyOcl2Package.PACKAGE_DECLARATION:
				if(context == grammarAccess.getPackage_declarationRule()) {
					sequence_package_declaration(context, (package_declaration) semanticObject); 
					return; 
				}
				else break;
			}
		if (errorAcceptor != null) errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Constraint:
	 *     name=ID*
	 */
	protected void sequence_Debut(EObject context, Debut semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     name=ID*
	 */
	protected void sequence_Fin(EObject context, Fin semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     name=ID
	 */
	protected void sequence_FirstSection(EObject context, FirstSection semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, MyOcl2Package.Literals.FIRST_SECTION__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MyOcl2Package.Literals.FIRST_SECTION__NAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getFirstSectionAccess().getNameIDTerminalRuleCall_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (resultat=RETOUR (Debut1=Debut resultat4=RETOUR4 Milieu1=Milieu Fin1=Fin)*)
	 */
	protected void sequence_LastSection(EObject context, LastSection semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (import_sentence+=declaration_sentence? Package=package_declaration)
	 */
	protected void sequence_Main(EObject context, Main semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (FirstSection1+=FirstSection SecondSection2+=SecondSection? ThirdSection3+=ThirdSection*)
	 */
	protected void sequence_MiddleSection(EObject context, MiddleSection semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     name=INT?
	 */
	protected void sequence_Milieu(EObject context, Milieu semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     name=ID
	 */
	protected void sequence_RETOUR12(EObject context, RETOUR12 semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, MyOcl2Package.Literals.RETOUR12__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MyOcl2Package.Literals.RETOUR12__NAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getRETOUR12Access().getNameIDTerminalRuleCall_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     name=ID?
	 */
	protected void sequence_RETOUR1(EObject context, RETOUR1 semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (resultat12=RETOUR12 resultat1=RETOUR1)
	 */
	protected void sequence_SecondSection(EObject context, SecondSection semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, MyOcl2Package.Literals.SECOND_SECTION__RESULTAT12) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MyOcl2Package.Literals.SECOND_SECTION__RESULTAT12));
			if(transientValues.isValueTransient(semanticObject, MyOcl2Package.Literals.SECOND_SECTION__RESULTAT1) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MyOcl2Package.Literals.SECOND_SECTION__RESULTAT1));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getSecondSectionAccess().getResultat12RETOUR12ParserRuleCall_0_0(), semanticObject.getResultat12());
		feeder.accept(grammarAccess.getSecondSectionAccess().getResultat1RETOUR1ParserRuleCall_2_0(), semanticObject.getResultat1());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     resultat1=RETOUR1
	 */
	protected void sequence_ThirdSection(EObject context, ThirdSection semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, MyOcl2Package.Literals.THIRD_SECTION__RESULTAT1) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MyOcl2Package.Literals.THIRD_SECTION__RESULTAT1));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getThirdSectionAccess().getResultat1RETOUR1ParserRuleCall_1_0(), semanticObject.getResultat1());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     elements+=context_declaration+
	 */
	protected void sequence_abstractelement(EObject context, abstractelement semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID Modalite+=MiddleSection? Modalite1+=LastSection*)
	 */
	protected void sequence_context_declaration(EObject context, context_declaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID deux=Qualified trois=Qualified1)
	 */
	protected void sequence_declaration_sentence(EObject context, declaration_sentence semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, MyOcl2Package.Literals.DECLARATION_SENTENCE__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MyOcl2Package.Literals.DECLARATION_SENTENCE__NAME));
			if(transientValues.isValueTransient(semanticObject, MyOcl2Package.Literals.DECLARATION_SENTENCE__DEUX) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MyOcl2Package.Literals.DECLARATION_SENTENCE__DEUX));
			if(transientValues.isValueTransient(semanticObject, MyOcl2Package.Literals.DECLARATION_SENTENCE__TROIS) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MyOcl2Package.Literals.DECLARATION_SENTENCE__TROIS));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getDeclaration_sentenceAccess().getNameIDTerminalRuleCall_1_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getDeclaration_sentenceAccess().getDeuxQualifiedParserRuleCall_3_0(), semanticObject.getDeux());
		feeder.accept(grammarAccess.getDeclaration_sentenceAccess().getTroisQualified1ParserRuleCall_4_0(), semanticObject.getTrois());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     name=ID
	 */
	protected void sequence_endstartelement(EObject context, endstartelement semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, MyOcl2Package.Literals.ENDSTARTELEMENT__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MyOcl2Package.Literals.ENDSTARTELEMENT__NAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getEndstartelementAccess().getNameIDTerminalRuleCall_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (sharedelement=endstartelement element+=abstractelement* sharedelement=endstartelement)
	 */
	protected void sequence_package_declaration(EObject context, package_declaration semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
}
