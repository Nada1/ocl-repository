package org.xtext.example.mydsl.serializer;

import com.google.inject.Inject;
import java.util.List;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.IGrammarAccess;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.AbstractElementAlias;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.AlternativeAlias;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.TokenAlias;
import org.eclipse.xtext.serializer.analysis.ISyntacticSequencerPDAProvider.ISynNavigable;
import org.eclipse.xtext.serializer.analysis.ISyntacticSequencerPDAProvider.ISynTransition;
import org.eclipse.xtext.serializer.sequencer.AbstractSyntacticSequencer;
import org.xtext.example.mydsl.services.MyOcl2GrammarAccess;

@SuppressWarnings("all")
public class MyOcl2SyntacticSequencer extends AbstractSyntacticSequencer {

	protected MyOcl2GrammarAccess grammarAccess;
	protected AbstractElementAlias match_RETOUR1_BooleanKeyword_2_or_DoubleKeyword_0_or_IntegerKeyword_1_or_STRINGKeyword_3;
	
	@Inject
	protected void init(IGrammarAccess access) {
		grammarAccess = (MyOcl2GrammarAccess) access;
		match_RETOUR1_BooleanKeyword_2_or_DoubleKeyword_0_or_IntegerKeyword_1_or_STRINGKeyword_3 = new AlternativeAlias(false, false, new TokenAlias(false, false, grammarAccess.getRETOUR1Access().getBooleanKeyword_2()), new TokenAlias(false, false, grammarAccess.getRETOUR1Access().getDoubleKeyword_0()), new TokenAlias(false, false, grammarAccess.getRETOUR1Access().getIntegerKeyword_1()), new TokenAlias(false, false, grammarAccess.getRETOUR1Access().getSTRINGKeyword_3()));
	}
	
	@Override
	protected String getUnassignedRuleCallToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		return "";
	}
	
	
	@Override
	protected void emitUnassignedTokens(EObject semanticObject, ISynTransition transition, INode fromNode, INode toNode) {
		if (transition.getAmbiguousSyntaxes().isEmpty()) return;
		List<INode> transitionNodes = collectNodes(fromNode, toNode);
		for (AbstractElementAlias syntax : transition.getAmbiguousSyntaxes()) {
			List<INode> syntaxNodes = getNodesFor(transitionNodes, syntax);
			if(match_RETOUR1_BooleanKeyword_2_or_DoubleKeyword_0_or_IntegerKeyword_1_or_STRINGKeyword_3.equals(syntax))
				emit_RETOUR1_BooleanKeyword_2_or_DoubleKeyword_0_or_IntegerKeyword_1_or_STRINGKeyword_3(semanticObject, getLastNavigableState(), syntaxNodes);
			else acceptNodes(getLastNavigableState(), syntaxNodes);
		}
	}

	/**
	 * Syntax:
	 *     'STRING' | 'Integer' | 'Boolean' | 'Double'
	 */
	protected void emit_RETOUR1_BooleanKeyword_2_or_DoubleKeyword_0_or_IntegerKeyword_1_or_STRINGKeyword_3(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
}
