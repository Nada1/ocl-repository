/**
 */
package org.xtext.example.mydsl.myOcl2;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Second Section</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.SecondSection#getResultat12 <em>Resultat12</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.SecondSection#getResultat1 <em>Resultat1</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.example.mydsl.myOcl2.MyOcl2Package#getSecondSection()
 * @model
 * @generated
 */
public interface SecondSection extends EObject
{
  /**
   * Returns the value of the '<em><b>Resultat12</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Resultat12</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Resultat12</em>' containment reference.
   * @see #setResultat12(RETOUR12)
   * @see org.xtext.example.mydsl.myOcl2.MyOcl2Package#getSecondSection_Resultat12()
   * @model containment="true"
   * @generated
   */
  RETOUR12 getResultat12();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.myOcl2.SecondSection#getResultat12 <em>Resultat12</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Resultat12</em>' containment reference.
   * @see #getResultat12()
   * @generated
   */
  void setResultat12(RETOUR12 value);

  /**
   * Returns the value of the '<em><b>Resultat1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Resultat1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Resultat1</em>' containment reference.
   * @see #setResultat1(RETOUR1)
   * @see org.xtext.example.mydsl.myOcl2.MyOcl2Package#getSecondSection_Resultat1()
   * @model containment="true"
   * @generated
   */
  RETOUR1 getResultat1();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.myOcl2.SecondSection#getResultat1 <em>Resultat1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Resultat1</em>' containment reference.
   * @see #getResultat1()
   * @generated
   */
  void setResultat1(RETOUR1 value);

} // SecondSection
