/**
 */
package org.xtext.example.mydsl.myOcl2;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>package declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.package_declaration#getSharedelement <em>Sharedelement</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.package_declaration#getElement <em>Element</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.example.mydsl.myOcl2.MyOcl2Package#getpackage_declaration()
 * @model
 * @generated
 */
public interface package_declaration extends EObject
{
  /**
   * Returns the value of the '<em><b>Sharedelement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sharedelement</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sharedelement</em>' containment reference.
   * @see #setSharedelement(endstartelement)
   * @see org.xtext.example.mydsl.myOcl2.MyOcl2Package#getpackage_declaration_Sharedelement()
   * @model containment="true"
   * @generated
   */
  endstartelement getSharedelement();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.myOcl2.package_declaration#getSharedelement <em>Sharedelement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Sharedelement</em>' containment reference.
   * @see #getSharedelement()
   * @generated
   */
  void setSharedelement(endstartelement value);

  /**
   * Returns the value of the '<em><b>Element</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.example.mydsl.myOcl2.abstractelement}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Element</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Element</em>' containment reference list.
   * @see org.xtext.example.mydsl.myOcl2.MyOcl2Package#getpackage_declaration_Element()
   * @model containment="true"
   * @generated
   */
  EList<abstractelement> getElement();

} // package_declaration
