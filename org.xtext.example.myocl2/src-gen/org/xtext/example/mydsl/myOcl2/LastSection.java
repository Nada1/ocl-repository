/**
 */
package org.xtext.example.mydsl.myOcl2;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Last Section</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.LastSection#getResultat <em>Resultat</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.LastSection#getDebut1 <em>Debut1</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.LastSection#getResultat4 <em>Resultat4</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.LastSection#getMilieu1 <em>Milieu1</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.LastSection#getFin1 <em>Fin1</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.example.mydsl.myOcl2.MyOcl2Package#getLastSection()
 * @model
 * @generated
 */
public interface LastSection extends EObject
{
  /**
   * Returns the value of the '<em><b>Resultat</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Resultat</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Resultat</em>' attribute.
   * @see #setResultat(String)
   * @see org.xtext.example.mydsl.myOcl2.MyOcl2Package#getLastSection_Resultat()
   * @model
   * @generated
   */
  String getResultat();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.myOcl2.LastSection#getResultat <em>Resultat</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Resultat</em>' attribute.
   * @see #getResultat()
   * @generated
   */
  void setResultat(String value);

  /**
   * Returns the value of the '<em><b>Debut1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Debut1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Debut1</em>' containment reference.
   * @see #setDebut1(Debut)
   * @see org.xtext.example.mydsl.myOcl2.MyOcl2Package#getLastSection_Debut1()
   * @model containment="true"
   * @generated
   */
  Debut getDebut1();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.myOcl2.LastSection#getDebut1 <em>Debut1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Debut1</em>' containment reference.
   * @see #getDebut1()
   * @generated
   */
  void setDebut1(Debut value);

  /**
   * Returns the value of the '<em><b>Resultat4</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Resultat4</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Resultat4</em>' attribute.
   * @see #setResultat4(String)
   * @see org.xtext.example.mydsl.myOcl2.MyOcl2Package#getLastSection_Resultat4()
   * @model
   * @generated
   */
  String getResultat4();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.myOcl2.LastSection#getResultat4 <em>Resultat4</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Resultat4</em>' attribute.
   * @see #getResultat4()
   * @generated
   */
  void setResultat4(String value);

  /**
   * Returns the value of the '<em><b>Milieu1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Milieu1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Milieu1</em>' containment reference.
   * @see #setMilieu1(Milieu)
   * @see org.xtext.example.mydsl.myOcl2.MyOcl2Package#getLastSection_Milieu1()
   * @model containment="true"
   * @generated
   */
  Milieu getMilieu1();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.myOcl2.LastSection#getMilieu1 <em>Milieu1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Milieu1</em>' containment reference.
   * @see #getMilieu1()
   * @generated
   */
  void setMilieu1(Milieu value);

  /**
   * Returns the value of the '<em><b>Fin1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Fin1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Fin1</em>' containment reference.
   * @see #setFin1(Fin)
   * @see org.xtext.example.mydsl.myOcl2.MyOcl2Package#getLastSection_Fin1()
   * @model containment="true"
   * @generated
   */
  Fin getFin1();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.myOcl2.LastSection#getFin1 <em>Fin1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Fin1</em>' containment reference.
   * @see #getFin1()
   * @generated
   */
  void setFin1(Fin value);

} // LastSection
