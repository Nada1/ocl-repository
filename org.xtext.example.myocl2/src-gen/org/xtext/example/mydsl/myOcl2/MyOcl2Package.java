/**
 */
package org.xtext.example.mydsl.myOcl2;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.xtext.example.mydsl.myOcl2.MyOcl2Factory
 * @model kind="package"
 * @generated
 */
public interface MyOcl2Package extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "myOcl2";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.xtext.org/example/mydsl/MyOcl2";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "myOcl2";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  MyOcl2Package eINSTANCE = org.xtext.example.mydsl.myOcl2.impl.MyOcl2PackageImpl.init();

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myOcl2.impl.MainImpl <em>Main</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myOcl2.impl.MainImpl
   * @see org.xtext.example.mydsl.myOcl2.impl.MyOcl2PackageImpl#getMain()
   * @generated
   */
  int MAIN = 0;

  /**
   * The feature id for the '<em><b>Import sentence</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAIN__IMPORT_SENTENCE = 0;

  /**
   * The feature id for the '<em><b>Package</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAIN__PACKAGE = 1;

  /**
   * The number of structural features of the '<em>Main</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MAIN_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myOcl2.impl.package_declarationImpl <em>package declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myOcl2.impl.package_declarationImpl
   * @see org.xtext.example.mydsl.myOcl2.impl.MyOcl2PackageImpl#getpackage_declaration()
   * @generated
   */
  int PACKAGE_DECLARATION = 1;

  /**
   * The feature id for the '<em><b>Sharedelement</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PACKAGE_DECLARATION__SHAREDELEMENT = 0;

  /**
   * The feature id for the '<em><b>Element</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PACKAGE_DECLARATION__ELEMENT = 1;

  /**
   * The number of structural features of the '<em>package declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PACKAGE_DECLARATION_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myOcl2.impl.endstartelementImpl <em>endstartelement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myOcl2.impl.endstartelementImpl
   * @see org.xtext.example.mydsl.myOcl2.impl.MyOcl2PackageImpl#getendstartelement()
   * @generated
   */
  int ENDSTARTELEMENT = 2;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENDSTARTELEMENT__NAME = 0;

  /**
   * The number of structural features of the '<em>endstartelement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ENDSTARTELEMENT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myOcl2.impl.abstractelementImpl <em>abstractelement</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myOcl2.impl.abstractelementImpl
   * @see org.xtext.example.mydsl.myOcl2.impl.MyOcl2PackageImpl#getabstractelement()
   * @generated
   */
  int ABSTRACTELEMENT = 3;

  /**
   * The feature id for the '<em><b>Elements</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ABSTRACTELEMENT__ELEMENTS = 0;

  /**
   * The number of structural features of the '<em>abstractelement</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ABSTRACTELEMENT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myOcl2.impl.context_declarationImpl <em>context declaration</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myOcl2.impl.context_declarationImpl
   * @see org.xtext.example.mydsl.myOcl2.impl.MyOcl2PackageImpl#getcontext_declaration()
   * @generated
   */
  int CONTEXT_DECLARATION = 4;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTEXT_DECLARATION__NAME = 0;

  /**
   * The feature id for the '<em><b>Modalite</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTEXT_DECLARATION__MODALITE = 1;

  /**
   * The feature id for the '<em><b>Modalite1</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTEXT_DECLARATION__MODALITE1 = 2;

  /**
   * The number of structural features of the '<em>context declaration</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTEXT_DECLARATION_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myOcl2.impl.MiddleSectionImpl <em>Middle Section</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myOcl2.impl.MiddleSectionImpl
   * @see org.xtext.example.mydsl.myOcl2.impl.MyOcl2PackageImpl#getMiddleSection()
   * @generated
   */
  int MIDDLE_SECTION = 5;

  /**
   * The feature id for the '<em><b>First Section1</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MIDDLE_SECTION__FIRST_SECTION1 = 0;

  /**
   * The feature id for the '<em><b>Second Section2</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MIDDLE_SECTION__SECOND_SECTION2 = 1;

  /**
   * The feature id for the '<em><b>Third Section3</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MIDDLE_SECTION__THIRD_SECTION3 = 2;

  /**
   * The number of structural features of the '<em>Middle Section</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MIDDLE_SECTION_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myOcl2.impl.ThirdSectionImpl <em>Third Section</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myOcl2.impl.ThirdSectionImpl
   * @see org.xtext.example.mydsl.myOcl2.impl.MyOcl2PackageImpl#getThirdSection()
   * @generated
   */
  int THIRD_SECTION = 6;

  /**
   * The feature id for the '<em><b>Resultat1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THIRD_SECTION__RESULTAT1 = 0;

  /**
   * The number of structural features of the '<em>Third Section</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int THIRD_SECTION_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myOcl2.impl.RETOUR1Impl <em>RETOUR1</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myOcl2.impl.RETOUR1Impl
   * @see org.xtext.example.mydsl.myOcl2.impl.MyOcl2PackageImpl#getRETOUR1()
   * @generated
   */
  int RETOUR1 = 7;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RETOUR1__NAME = 0;

  /**
   * The number of structural features of the '<em>RETOUR1</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RETOUR1_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myOcl2.impl.FirstSectionImpl <em>First Section</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myOcl2.impl.FirstSectionImpl
   * @see org.xtext.example.mydsl.myOcl2.impl.MyOcl2PackageImpl#getFirstSection()
   * @generated
   */
  int FIRST_SECTION = 8;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIRST_SECTION__NAME = 0;

  /**
   * The number of structural features of the '<em>First Section</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIRST_SECTION_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myOcl2.impl.SecondSectionImpl <em>Second Section</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myOcl2.impl.SecondSectionImpl
   * @see org.xtext.example.mydsl.myOcl2.impl.MyOcl2PackageImpl#getSecondSection()
   * @generated
   */
  int SECOND_SECTION = 9;

  /**
   * The feature id for the '<em><b>Resultat12</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SECOND_SECTION__RESULTAT12 = 0;

  /**
   * The feature id for the '<em><b>Resultat1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SECOND_SECTION__RESULTAT1 = 1;

  /**
   * The number of structural features of the '<em>Second Section</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SECOND_SECTION_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myOcl2.impl.RETOUR12Impl <em>RETOUR12</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myOcl2.impl.RETOUR12Impl
   * @see org.xtext.example.mydsl.myOcl2.impl.MyOcl2PackageImpl#getRETOUR12()
   * @generated
   */
  int RETOUR12 = 10;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RETOUR12__NAME = 0;

  /**
   * The number of structural features of the '<em>RETOUR12</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RETOUR12_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myOcl2.impl.LastSectionImpl <em>Last Section</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myOcl2.impl.LastSectionImpl
   * @see org.xtext.example.mydsl.myOcl2.impl.MyOcl2PackageImpl#getLastSection()
   * @generated
   */
  int LAST_SECTION = 11;

  /**
   * The feature id for the '<em><b>Resultat</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LAST_SECTION__RESULTAT = 0;

  /**
   * The feature id for the '<em><b>Debut1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LAST_SECTION__DEBUT1 = 1;

  /**
   * The feature id for the '<em><b>Resultat4</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LAST_SECTION__RESULTAT4 = 2;

  /**
   * The feature id for the '<em><b>Milieu1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LAST_SECTION__MILIEU1 = 3;

  /**
   * The feature id for the '<em><b>Fin1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LAST_SECTION__FIN1 = 4;

  /**
   * The number of structural features of the '<em>Last Section</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LAST_SECTION_FEATURE_COUNT = 5;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myOcl2.impl.FinImpl <em>Fin</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myOcl2.impl.FinImpl
   * @see org.xtext.example.mydsl.myOcl2.impl.MyOcl2PackageImpl#getFin()
   * @generated
   */
  int FIN = 12;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIN__NAME = 0;

  /**
   * The number of structural features of the '<em>Fin</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FIN_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myOcl2.impl.MilieuImpl <em>Milieu</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myOcl2.impl.MilieuImpl
   * @see org.xtext.example.mydsl.myOcl2.impl.MyOcl2PackageImpl#getMilieu()
   * @generated
   */
  int MILIEU = 13;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MILIEU__NAME = 0;

  /**
   * The number of structural features of the '<em>Milieu</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MILIEU_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myOcl2.impl.DebutImpl <em>Debut</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myOcl2.impl.DebutImpl
   * @see org.xtext.example.mydsl.myOcl2.impl.MyOcl2PackageImpl#getDebut()
   * @generated
   */
  int DEBUT = 14;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEBUT__NAME = 0;

  /**
   * The number of structural features of the '<em>Debut</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DEBUT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myOcl2.impl.declaration_sentenceImpl <em>declaration sentence</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myOcl2.impl.declaration_sentenceImpl
   * @see org.xtext.example.mydsl.myOcl2.impl.MyOcl2PackageImpl#getdeclaration_sentence()
   * @generated
   */
  int DECLARATION_SENTENCE = 15;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECLARATION_SENTENCE__NAME = 0;

  /**
   * The feature id for the '<em><b>Deux</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECLARATION_SENTENCE__DEUX = 1;

  /**
   * The feature id for the '<em><b>Trois</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECLARATION_SENTENCE__TROIS = 2;

  /**
   * The number of structural features of the '<em>declaration sentence</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECLARATION_SENTENCE_FEATURE_COUNT = 3;


  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myOcl2.Main <em>Main</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Main</em>'.
   * @see org.xtext.example.mydsl.myOcl2.Main
   * @generated
   */
  EClass getMain();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.example.mydsl.myOcl2.Main#getImport_sentence <em>Import sentence</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Import sentence</em>'.
   * @see org.xtext.example.mydsl.myOcl2.Main#getImport_sentence()
   * @see #getMain()
   * @generated
   */
  EReference getMain_Import_sentence();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myOcl2.Main#getPackage <em>Package</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Package</em>'.
   * @see org.xtext.example.mydsl.myOcl2.Main#getPackage()
   * @see #getMain()
   * @generated
   */
  EReference getMain_Package();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myOcl2.package_declaration <em>package declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>package declaration</em>'.
   * @see org.xtext.example.mydsl.myOcl2.package_declaration
   * @generated
   */
  EClass getpackage_declaration();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myOcl2.package_declaration#getSharedelement <em>Sharedelement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Sharedelement</em>'.
   * @see org.xtext.example.mydsl.myOcl2.package_declaration#getSharedelement()
   * @see #getpackage_declaration()
   * @generated
   */
  EReference getpackage_declaration_Sharedelement();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.example.mydsl.myOcl2.package_declaration#getElement <em>Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Element</em>'.
   * @see org.xtext.example.mydsl.myOcl2.package_declaration#getElement()
   * @see #getpackage_declaration()
   * @generated
   */
  EReference getpackage_declaration_Element();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myOcl2.endstartelement <em>endstartelement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>endstartelement</em>'.
   * @see org.xtext.example.mydsl.myOcl2.endstartelement
   * @generated
   */
  EClass getendstartelement();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myOcl2.endstartelement#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.xtext.example.mydsl.myOcl2.endstartelement#getName()
   * @see #getendstartelement()
   * @generated
   */
  EAttribute getendstartelement_Name();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myOcl2.abstractelement <em>abstractelement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>abstractelement</em>'.
   * @see org.xtext.example.mydsl.myOcl2.abstractelement
   * @generated
   */
  EClass getabstractelement();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.example.mydsl.myOcl2.abstractelement#getElements <em>Elements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Elements</em>'.
   * @see org.xtext.example.mydsl.myOcl2.abstractelement#getElements()
   * @see #getabstractelement()
   * @generated
   */
  EReference getabstractelement_Elements();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myOcl2.context_declaration <em>context declaration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>context declaration</em>'.
   * @see org.xtext.example.mydsl.myOcl2.context_declaration
   * @generated
   */
  EClass getcontext_declaration();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myOcl2.context_declaration#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.xtext.example.mydsl.myOcl2.context_declaration#getName()
   * @see #getcontext_declaration()
   * @generated
   */
  EAttribute getcontext_declaration_Name();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.example.mydsl.myOcl2.context_declaration#getModalite <em>Modalite</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Modalite</em>'.
   * @see org.xtext.example.mydsl.myOcl2.context_declaration#getModalite()
   * @see #getcontext_declaration()
   * @generated
   */
  EReference getcontext_declaration_Modalite();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.example.mydsl.myOcl2.context_declaration#getModalite1 <em>Modalite1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Modalite1</em>'.
   * @see org.xtext.example.mydsl.myOcl2.context_declaration#getModalite1()
   * @see #getcontext_declaration()
   * @generated
   */
  EReference getcontext_declaration_Modalite1();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myOcl2.MiddleSection <em>Middle Section</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Middle Section</em>'.
   * @see org.xtext.example.mydsl.myOcl2.MiddleSection
   * @generated
   */
  EClass getMiddleSection();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.example.mydsl.myOcl2.MiddleSection#getFirstSection1 <em>First Section1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>First Section1</em>'.
   * @see org.xtext.example.mydsl.myOcl2.MiddleSection#getFirstSection1()
   * @see #getMiddleSection()
   * @generated
   */
  EReference getMiddleSection_FirstSection1();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.example.mydsl.myOcl2.MiddleSection#getSecondSection2 <em>Second Section2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Second Section2</em>'.
   * @see org.xtext.example.mydsl.myOcl2.MiddleSection#getSecondSection2()
   * @see #getMiddleSection()
   * @generated
   */
  EReference getMiddleSection_SecondSection2();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.example.mydsl.myOcl2.MiddleSection#getThirdSection3 <em>Third Section3</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Third Section3</em>'.
   * @see org.xtext.example.mydsl.myOcl2.MiddleSection#getThirdSection3()
   * @see #getMiddleSection()
   * @generated
   */
  EReference getMiddleSection_ThirdSection3();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myOcl2.ThirdSection <em>Third Section</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Third Section</em>'.
   * @see org.xtext.example.mydsl.myOcl2.ThirdSection
   * @generated
   */
  EClass getThirdSection();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myOcl2.ThirdSection#getResultat1 <em>Resultat1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Resultat1</em>'.
   * @see org.xtext.example.mydsl.myOcl2.ThirdSection#getResultat1()
   * @see #getThirdSection()
   * @generated
   */
  EReference getThirdSection_Resultat1();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myOcl2.RETOUR1 <em>RETOUR1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>RETOUR1</em>'.
   * @see org.xtext.example.mydsl.myOcl2.RETOUR1
   * @generated
   */
  EClass getRETOUR1();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myOcl2.RETOUR1#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.xtext.example.mydsl.myOcl2.RETOUR1#getName()
   * @see #getRETOUR1()
   * @generated
   */
  EAttribute getRETOUR1_Name();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myOcl2.FirstSection <em>First Section</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>First Section</em>'.
   * @see org.xtext.example.mydsl.myOcl2.FirstSection
   * @generated
   */
  EClass getFirstSection();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myOcl2.FirstSection#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.xtext.example.mydsl.myOcl2.FirstSection#getName()
   * @see #getFirstSection()
   * @generated
   */
  EAttribute getFirstSection_Name();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myOcl2.SecondSection <em>Second Section</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Second Section</em>'.
   * @see org.xtext.example.mydsl.myOcl2.SecondSection
   * @generated
   */
  EClass getSecondSection();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myOcl2.SecondSection#getResultat12 <em>Resultat12</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Resultat12</em>'.
   * @see org.xtext.example.mydsl.myOcl2.SecondSection#getResultat12()
   * @see #getSecondSection()
   * @generated
   */
  EReference getSecondSection_Resultat12();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myOcl2.SecondSection#getResultat1 <em>Resultat1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Resultat1</em>'.
   * @see org.xtext.example.mydsl.myOcl2.SecondSection#getResultat1()
   * @see #getSecondSection()
   * @generated
   */
  EReference getSecondSection_Resultat1();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myOcl2.RETOUR12 <em>RETOUR12</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>RETOUR12</em>'.
   * @see org.xtext.example.mydsl.myOcl2.RETOUR12
   * @generated
   */
  EClass getRETOUR12();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myOcl2.RETOUR12#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.xtext.example.mydsl.myOcl2.RETOUR12#getName()
   * @see #getRETOUR12()
   * @generated
   */
  EAttribute getRETOUR12_Name();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myOcl2.LastSection <em>Last Section</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Last Section</em>'.
   * @see org.xtext.example.mydsl.myOcl2.LastSection
   * @generated
   */
  EClass getLastSection();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myOcl2.LastSection#getResultat <em>Resultat</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Resultat</em>'.
   * @see org.xtext.example.mydsl.myOcl2.LastSection#getResultat()
   * @see #getLastSection()
   * @generated
   */
  EAttribute getLastSection_Resultat();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myOcl2.LastSection#getDebut1 <em>Debut1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Debut1</em>'.
   * @see org.xtext.example.mydsl.myOcl2.LastSection#getDebut1()
   * @see #getLastSection()
   * @generated
   */
  EReference getLastSection_Debut1();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myOcl2.LastSection#getResultat4 <em>Resultat4</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Resultat4</em>'.
   * @see org.xtext.example.mydsl.myOcl2.LastSection#getResultat4()
   * @see #getLastSection()
   * @generated
   */
  EAttribute getLastSection_Resultat4();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myOcl2.LastSection#getMilieu1 <em>Milieu1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Milieu1</em>'.
   * @see org.xtext.example.mydsl.myOcl2.LastSection#getMilieu1()
   * @see #getLastSection()
   * @generated
   */
  EReference getLastSection_Milieu1();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myOcl2.LastSection#getFin1 <em>Fin1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Fin1</em>'.
   * @see org.xtext.example.mydsl.myOcl2.LastSection#getFin1()
   * @see #getLastSection()
   * @generated
   */
  EReference getLastSection_Fin1();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myOcl2.Fin <em>Fin</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Fin</em>'.
   * @see org.xtext.example.mydsl.myOcl2.Fin
   * @generated
   */
  EClass getFin();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myOcl2.Fin#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.xtext.example.mydsl.myOcl2.Fin#getName()
   * @see #getFin()
   * @generated
   */
  EAttribute getFin_Name();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myOcl2.Milieu <em>Milieu</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Milieu</em>'.
   * @see org.xtext.example.mydsl.myOcl2.Milieu
   * @generated
   */
  EClass getMilieu();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myOcl2.Milieu#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.xtext.example.mydsl.myOcl2.Milieu#getName()
   * @see #getMilieu()
   * @generated
   */
  EAttribute getMilieu_Name();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myOcl2.Debut <em>Debut</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Debut</em>'.
   * @see org.xtext.example.mydsl.myOcl2.Debut
   * @generated
   */
  EClass getDebut();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myOcl2.Debut#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.xtext.example.mydsl.myOcl2.Debut#getName()
   * @see #getDebut()
   * @generated
   */
  EAttribute getDebut_Name();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myOcl2.declaration_sentence <em>declaration sentence</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>declaration sentence</em>'.
   * @see org.xtext.example.mydsl.myOcl2.declaration_sentence
   * @generated
   */
  EClass getdeclaration_sentence();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myOcl2.declaration_sentence#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.xtext.example.mydsl.myOcl2.declaration_sentence#getName()
   * @see #getdeclaration_sentence()
   * @generated
   */
  EAttribute getdeclaration_sentence_Name();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myOcl2.declaration_sentence#getDeux <em>Deux</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Deux</em>'.
   * @see org.xtext.example.mydsl.myOcl2.declaration_sentence#getDeux()
   * @see #getdeclaration_sentence()
   * @generated
   */
  EAttribute getdeclaration_sentence_Deux();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myOcl2.declaration_sentence#getTrois <em>Trois</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Trois</em>'.
   * @see org.xtext.example.mydsl.myOcl2.declaration_sentence#getTrois()
   * @see #getdeclaration_sentence()
   * @generated
   */
  EAttribute getdeclaration_sentence_Trois();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  MyOcl2Factory getMyOcl2Factory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myOcl2.impl.MainImpl <em>Main</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myOcl2.impl.MainImpl
     * @see org.xtext.example.mydsl.myOcl2.impl.MyOcl2PackageImpl#getMain()
     * @generated
     */
    EClass MAIN = eINSTANCE.getMain();

    /**
     * The meta object literal for the '<em><b>Import sentence</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MAIN__IMPORT_SENTENCE = eINSTANCE.getMain_Import_sentence();

    /**
     * The meta object literal for the '<em><b>Package</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MAIN__PACKAGE = eINSTANCE.getMain_Package();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myOcl2.impl.package_declarationImpl <em>package declaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myOcl2.impl.package_declarationImpl
     * @see org.xtext.example.mydsl.myOcl2.impl.MyOcl2PackageImpl#getpackage_declaration()
     * @generated
     */
    EClass PACKAGE_DECLARATION = eINSTANCE.getpackage_declaration();

    /**
     * The meta object literal for the '<em><b>Sharedelement</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PACKAGE_DECLARATION__SHAREDELEMENT = eINSTANCE.getpackage_declaration_Sharedelement();

    /**
     * The meta object literal for the '<em><b>Element</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PACKAGE_DECLARATION__ELEMENT = eINSTANCE.getpackage_declaration_Element();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myOcl2.impl.endstartelementImpl <em>endstartelement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myOcl2.impl.endstartelementImpl
     * @see org.xtext.example.mydsl.myOcl2.impl.MyOcl2PackageImpl#getendstartelement()
     * @generated
     */
    EClass ENDSTARTELEMENT = eINSTANCE.getendstartelement();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ENDSTARTELEMENT__NAME = eINSTANCE.getendstartelement_Name();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myOcl2.impl.abstractelementImpl <em>abstractelement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myOcl2.impl.abstractelementImpl
     * @see org.xtext.example.mydsl.myOcl2.impl.MyOcl2PackageImpl#getabstractelement()
     * @generated
     */
    EClass ABSTRACTELEMENT = eINSTANCE.getabstractelement();

    /**
     * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ABSTRACTELEMENT__ELEMENTS = eINSTANCE.getabstractelement_Elements();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myOcl2.impl.context_declarationImpl <em>context declaration</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myOcl2.impl.context_declarationImpl
     * @see org.xtext.example.mydsl.myOcl2.impl.MyOcl2PackageImpl#getcontext_declaration()
     * @generated
     */
    EClass CONTEXT_DECLARATION = eINSTANCE.getcontext_declaration();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CONTEXT_DECLARATION__NAME = eINSTANCE.getcontext_declaration_Name();

    /**
     * The meta object literal for the '<em><b>Modalite</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CONTEXT_DECLARATION__MODALITE = eINSTANCE.getcontext_declaration_Modalite();

    /**
     * The meta object literal for the '<em><b>Modalite1</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CONTEXT_DECLARATION__MODALITE1 = eINSTANCE.getcontext_declaration_Modalite1();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myOcl2.impl.MiddleSectionImpl <em>Middle Section</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myOcl2.impl.MiddleSectionImpl
     * @see org.xtext.example.mydsl.myOcl2.impl.MyOcl2PackageImpl#getMiddleSection()
     * @generated
     */
    EClass MIDDLE_SECTION = eINSTANCE.getMiddleSection();

    /**
     * The meta object literal for the '<em><b>First Section1</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MIDDLE_SECTION__FIRST_SECTION1 = eINSTANCE.getMiddleSection_FirstSection1();

    /**
     * The meta object literal for the '<em><b>Second Section2</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MIDDLE_SECTION__SECOND_SECTION2 = eINSTANCE.getMiddleSection_SecondSection2();

    /**
     * The meta object literal for the '<em><b>Third Section3</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MIDDLE_SECTION__THIRD_SECTION3 = eINSTANCE.getMiddleSection_ThirdSection3();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myOcl2.impl.ThirdSectionImpl <em>Third Section</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myOcl2.impl.ThirdSectionImpl
     * @see org.xtext.example.mydsl.myOcl2.impl.MyOcl2PackageImpl#getThirdSection()
     * @generated
     */
    EClass THIRD_SECTION = eINSTANCE.getThirdSection();

    /**
     * The meta object literal for the '<em><b>Resultat1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference THIRD_SECTION__RESULTAT1 = eINSTANCE.getThirdSection_Resultat1();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myOcl2.impl.RETOUR1Impl <em>RETOUR1</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myOcl2.impl.RETOUR1Impl
     * @see org.xtext.example.mydsl.myOcl2.impl.MyOcl2PackageImpl#getRETOUR1()
     * @generated
     */
    EClass RETOUR1 = eINSTANCE.getRETOUR1();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RETOUR1__NAME = eINSTANCE.getRETOUR1_Name();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myOcl2.impl.FirstSectionImpl <em>First Section</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myOcl2.impl.FirstSectionImpl
     * @see org.xtext.example.mydsl.myOcl2.impl.MyOcl2PackageImpl#getFirstSection()
     * @generated
     */
    EClass FIRST_SECTION = eINSTANCE.getFirstSection();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute FIRST_SECTION__NAME = eINSTANCE.getFirstSection_Name();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myOcl2.impl.SecondSectionImpl <em>Second Section</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myOcl2.impl.SecondSectionImpl
     * @see org.xtext.example.mydsl.myOcl2.impl.MyOcl2PackageImpl#getSecondSection()
     * @generated
     */
    EClass SECOND_SECTION = eINSTANCE.getSecondSection();

    /**
     * The meta object literal for the '<em><b>Resultat12</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SECOND_SECTION__RESULTAT12 = eINSTANCE.getSecondSection_Resultat12();

    /**
     * The meta object literal for the '<em><b>Resultat1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SECOND_SECTION__RESULTAT1 = eINSTANCE.getSecondSection_Resultat1();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myOcl2.impl.RETOUR12Impl <em>RETOUR12</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myOcl2.impl.RETOUR12Impl
     * @see org.xtext.example.mydsl.myOcl2.impl.MyOcl2PackageImpl#getRETOUR12()
     * @generated
     */
    EClass RETOUR12 = eINSTANCE.getRETOUR12();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RETOUR12__NAME = eINSTANCE.getRETOUR12_Name();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myOcl2.impl.LastSectionImpl <em>Last Section</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myOcl2.impl.LastSectionImpl
     * @see org.xtext.example.mydsl.myOcl2.impl.MyOcl2PackageImpl#getLastSection()
     * @generated
     */
    EClass LAST_SECTION = eINSTANCE.getLastSection();

    /**
     * The meta object literal for the '<em><b>Resultat</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute LAST_SECTION__RESULTAT = eINSTANCE.getLastSection_Resultat();

    /**
     * The meta object literal for the '<em><b>Debut1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LAST_SECTION__DEBUT1 = eINSTANCE.getLastSection_Debut1();

    /**
     * The meta object literal for the '<em><b>Resultat4</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute LAST_SECTION__RESULTAT4 = eINSTANCE.getLastSection_Resultat4();

    /**
     * The meta object literal for the '<em><b>Milieu1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LAST_SECTION__MILIEU1 = eINSTANCE.getLastSection_Milieu1();

    /**
     * The meta object literal for the '<em><b>Fin1</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LAST_SECTION__FIN1 = eINSTANCE.getLastSection_Fin1();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myOcl2.impl.FinImpl <em>Fin</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myOcl2.impl.FinImpl
     * @see org.xtext.example.mydsl.myOcl2.impl.MyOcl2PackageImpl#getFin()
     * @generated
     */
    EClass FIN = eINSTANCE.getFin();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute FIN__NAME = eINSTANCE.getFin_Name();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myOcl2.impl.MilieuImpl <em>Milieu</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myOcl2.impl.MilieuImpl
     * @see org.xtext.example.mydsl.myOcl2.impl.MyOcl2PackageImpl#getMilieu()
     * @generated
     */
    EClass MILIEU = eINSTANCE.getMilieu();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute MILIEU__NAME = eINSTANCE.getMilieu_Name();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myOcl2.impl.DebutImpl <em>Debut</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myOcl2.impl.DebutImpl
     * @see org.xtext.example.mydsl.myOcl2.impl.MyOcl2PackageImpl#getDebut()
     * @generated
     */
    EClass DEBUT = eINSTANCE.getDebut();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DEBUT__NAME = eINSTANCE.getDebut_Name();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myOcl2.impl.declaration_sentenceImpl <em>declaration sentence</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myOcl2.impl.declaration_sentenceImpl
     * @see org.xtext.example.mydsl.myOcl2.impl.MyOcl2PackageImpl#getdeclaration_sentence()
     * @generated
     */
    EClass DECLARATION_SENTENCE = eINSTANCE.getdeclaration_sentence();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DECLARATION_SENTENCE__NAME = eINSTANCE.getdeclaration_sentence_Name();

    /**
     * The meta object literal for the '<em><b>Deux</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DECLARATION_SENTENCE__DEUX = eINSTANCE.getdeclaration_sentence_Deux();

    /**
     * The meta object literal for the '<em><b>Trois</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DECLARATION_SENTENCE__TROIS = eINSTANCE.getdeclaration_sentence_Trois();

  }

} //MyOcl2Package
