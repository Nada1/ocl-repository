/**
 */
package org.xtext.example.mydsl.myOcl2.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.xtext.example.mydsl.myOcl2.FirstSection;
import org.xtext.example.mydsl.myOcl2.MiddleSection;
import org.xtext.example.mydsl.myOcl2.MyOcl2Package;
import org.xtext.example.mydsl.myOcl2.SecondSection;
import org.xtext.example.mydsl.myOcl2.ThirdSection;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Middle Section</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.impl.MiddleSectionImpl#getFirstSection1 <em>First Section1</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.impl.MiddleSectionImpl#getSecondSection2 <em>Second Section2</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.impl.MiddleSectionImpl#getThirdSection3 <em>Third Section3</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class MiddleSectionImpl extends MinimalEObjectImpl.Container implements MiddleSection
{
  /**
   * The cached value of the '{@link #getFirstSection1() <em>First Section1</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFirstSection1()
   * @generated
   * @ordered
   */
  protected EList<FirstSection> firstSection1;

  /**
   * The cached value of the '{@link #getSecondSection2() <em>Second Section2</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSecondSection2()
   * @generated
   * @ordered
   */
  protected EList<SecondSection> secondSection2;

  /**
   * The cached value of the '{@link #getThirdSection3() <em>Third Section3</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getThirdSection3()
   * @generated
   * @ordered
   */
  protected EList<ThirdSection> thirdSection3;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected MiddleSectionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return MyOcl2Package.Literals.MIDDLE_SECTION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<FirstSection> getFirstSection1()
  {
    if (firstSection1 == null)
    {
      firstSection1 = new EObjectContainmentEList<FirstSection>(FirstSection.class, this, MyOcl2Package.MIDDLE_SECTION__FIRST_SECTION1);
    }
    return firstSection1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<SecondSection> getSecondSection2()
  {
    if (secondSection2 == null)
    {
      secondSection2 = new EObjectContainmentEList<SecondSection>(SecondSection.class, this, MyOcl2Package.MIDDLE_SECTION__SECOND_SECTION2);
    }
    return secondSection2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ThirdSection> getThirdSection3()
  {
    if (thirdSection3 == null)
    {
      thirdSection3 = new EObjectContainmentEList<ThirdSection>(ThirdSection.class, this, MyOcl2Package.MIDDLE_SECTION__THIRD_SECTION3);
    }
    return thirdSection3;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case MyOcl2Package.MIDDLE_SECTION__FIRST_SECTION1:
        return ((InternalEList<?>)getFirstSection1()).basicRemove(otherEnd, msgs);
      case MyOcl2Package.MIDDLE_SECTION__SECOND_SECTION2:
        return ((InternalEList<?>)getSecondSection2()).basicRemove(otherEnd, msgs);
      case MyOcl2Package.MIDDLE_SECTION__THIRD_SECTION3:
        return ((InternalEList<?>)getThirdSection3()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case MyOcl2Package.MIDDLE_SECTION__FIRST_SECTION1:
        return getFirstSection1();
      case MyOcl2Package.MIDDLE_SECTION__SECOND_SECTION2:
        return getSecondSection2();
      case MyOcl2Package.MIDDLE_SECTION__THIRD_SECTION3:
        return getThirdSection3();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case MyOcl2Package.MIDDLE_SECTION__FIRST_SECTION1:
        getFirstSection1().clear();
        getFirstSection1().addAll((Collection<? extends FirstSection>)newValue);
        return;
      case MyOcl2Package.MIDDLE_SECTION__SECOND_SECTION2:
        getSecondSection2().clear();
        getSecondSection2().addAll((Collection<? extends SecondSection>)newValue);
        return;
      case MyOcl2Package.MIDDLE_SECTION__THIRD_SECTION3:
        getThirdSection3().clear();
        getThirdSection3().addAll((Collection<? extends ThirdSection>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case MyOcl2Package.MIDDLE_SECTION__FIRST_SECTION1:
        getFirstSection1().clear();
        return;
      case MyOcl2Package.MIDDLE_SECTION__SECOND_SECTION2:
        getSecondSection2().clear();
        return;
      case MyOcl2Package.MIDDLE_SECTION__THIRD_SECTION3:
        getThirdSection3().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case MyOcl2Package.MIDDLE_SECTION__FIRST_SECTION1:
        return firstSection1 != null && !firstSection1.isEmpty();
      case MyOcl2Package.MIDDLE_SECTION__SECOND_SECTION2:
        return secondSection2 != null && !secondSection2.isEmpty();
      case MyOcl2Package.MIDDLE_SECTION__THIRD_SECTION3:
        return thirdSection3 != null && !thirdSection3.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //MiddleSectionImpl
