/**
 */
package org.xtext.example.mydsl.myOcl2.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.xtext.example.mydsl.myOcl2.MyOcl2Package;
import org.xtext.example.mydsl.myOcl2.abstractelement;
import org.xtext.example.mydsl.myOcl2.endstartelement;
import org.xtext.example.mydsl.myOcl2.package_declaration;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>package declaration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.impl.package_declarationImpl#getSharedelement <em>Sharedelement</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.impl.package_declarationImpl#getElement <em>Element</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class package_declarationImpl extends MinimalEObjectImpl.Container implements package_declaration
{
  /**
   * The cached value of the '{@link #getSharedelement() <em>Sharedelement</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSharedelement()
   * @generated
   * @ordered
   */
  protected endstartelement sharedelement;

  /**
   * The cached value of the '{@link #getElement() <em>Element</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getElement()
   * @generated
   * @ordered
   */
  protected EList<abstractelement> element;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected package_declarationImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return MyOcl2Package.Literals.PACKAGE_DECLARATION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public endstartelement getSharedelement()
  {
    return sharedelement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSharedelement(endstartelement newSharedelement, NotificationChain msgs)
  {
    endstartelement oldSharedelement = sharedelement;
    sharedelement = newSharedelement;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MyOcl2Package.PACKAGE_DECLARATION__SHAREDELEMENT, oldSharedelement, newSharedelement);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSharedelement(endstartelement newSharedelement)
  {
    if (newSharedelement != sharedelement)
    {
      NotificationChain msgs = null;
      if (sharedelement != null)
        msgs = ((InternalEObject)sharedelement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MyOcl2Package.PACKAGE_DECLARATION__SHAREDELEMENT, null, msgs);
      if (newSharedelement != null)
        msgs = ((InternalEObject)newSharedelement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MyOcl2Package.PACKAGE_DECLARATION__SHAREDELEMENT, null, msgs);
      msgs = basicSetSharedelement(newSharedelement, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MyOcl2Package.PACKAGE_DECLARATION__SHAREDELEMENT, newSharedelement, newSharedelement));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<abstractelement> getElement()
  {
    if (element == null)
    {
      element = new EObjectContainmentEList<abstractelement>(abstractelement.class, this, MyOcl2Package.PACKAGE_DECLARATION__ELEMENT);
    }
    return element;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case MyOcl2Package.PACKAGE_DECLARATION__SHAREDELEMENT:
        return basicSetSharedelement(null, msgs);
      case MyOcl2Package.PACKAGE_DECLARATION__ELEMENT:
        return ((InternalEList<?>)getElement()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case MyOcl2Package.PACKAGE_DECLARATION__SHAREDELEMENT:
        return getSharedelement();
      case MyOcl2Package.PACKAGE_DECLARATION__ELEMENT:
        return getElement();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case MyOcl2Package.PACKAGE_DECLARATION__SHAREDELEMENT:
        setSharedelement((endstartelement)newValue);
        return;
      case MyOcl2Package.PACKAGE_DECLARATION__ELEMENT:
        getElement().clear();
        getElement().addAll((Collection<? extends abstractelement>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case MyOcl2Package.PACKAGE_DECLARATION__SHAREDELEMENT:
        setSharedelement((endstartelement)null);
        return;
      case MyOcl2Package.PACKAGE_DECLARATION__ELEMENT:
        getElement().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case MyOcl2Package.PACKAGE_DECLARATION__SHAREDELEMENT:
        return sharedelement != null;
      case MyOcl2Package.PACKAGE_DECLARATION__ELEMENT:
        return element != null && !element.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //package_declarationImpl
