/**
 */
package org.xtext.example.mydsl.myOcl2.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.xtext.example.mydsl.myOcl2.Debut;
import org.xtext.example.mydsl.myOcl2.Fin;
import org.xtext.example.mydsl.myOcl2.FirstSection;
import org.xtext.example.mydsl.myOcl2.LastSection;
import org.xtext.example.mydsl.myOcl2.Main;
import org.xtext.example.mydsl.myOcl2.MiddleSection;
import org.xtext.example.mydsl.myOcl2.Milieu;
import org.xtext.example.mydsl.myOcl2.MyOcl2Factory;
import org.xtext.example.mydsl.myOcl2.MyOcl2Package;
import org.xtext.example.mydsl.myOcl2.SecondSection;
import org.xtext.example.mydsl.myOcl2.ThirdSection;
import org.xtext.example.mydsl.myOcl2.abstractelement;
import org.xtext.example.mydsl.myOcl2.context_declaration;
import org.xtext.example.mydsl.myOcl2.declaration_sentence;
import org.xtext.example.mydsl.myOcl2.endstartelement;
import org.xtext.example.mydsl.myOcl2.package_declaration;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MyOcl2PackageImpl extends EPackageImpl implements MyOcl2Package
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass mainEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass package_declarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass endstartelementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass abstractelementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass context_declarationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass middleSectionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass thirdSectionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass retour1EClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass firstSectionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass secondSectionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass retour12EClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass lastSectionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass finEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass milieuEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass debutEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass declaration_sentenceEClass = null;

  /**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see org.xtext.example.mydsl.myOcl2.MyOcl2Package#eNS_URI
   * @see #init()
   * @generated
   */
  private MyOcl2PackageImpl()
  {
    super(eNS_URI, MyOcl2Factory.eINSTANCE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static boolean isInited = false;

  /**
   * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
   * 
   * <p>This method is used to initialize {@link MyOcl2Package#eINSTANCE} when that field is accessed.
   * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #eNS_URI
   * @see #createPackageContents()
   * @see #initializePackageContents()
   * @generated
   */
  public static MyOcl2Package init()
  {
    if (isInited) return (MyOcl2Package)EPackage.Registry.INSTANCE.getEPackage(MyOcl2Package.eNS_URI);

    // Obtain or create and register package
    MyOcl2PackageImpl theMyOcl2Package = (MyOcl2PackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof MyOcl2PackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new MyOcl2PackageImpl());

    isInited = true;

    // Create package meta-data objects
    theMyOcl2Package.createPackageContents();

    // Initialize created meta-data
    theMyOcl2Package.initializePackageContents();

    // Mark meta-data to indicate it can't be changed
    theMyOcl2Package.freeze();

  
    // Update the registry and return the package
    EPackage.Registry.INSTANCE.put(MyOcl2Package.eNS_URI, theMyOcl2Package);
    return theMyOcl2Package;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getMain()
  {
    return mainEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMain_Import_sentence()
  {
    return (EReference)mainEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMain_Package()
  {
    return (EReference)mainEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getpackage_declaration()
  {
    return package_declarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getpackage_declaration_Sharedelement()
  {
    return (EReference)package_declarationEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getpackage_declaration_Element()
  {
    return (EReference)package_declarationEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getendstartelement()
  {
    return endstartelementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getendstartelement_Name()
  {
    return (EAttribute)endstartelementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getabstractelement()
  {
    return abstractelementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getabstractelement_Elements()
  {
    return (EReference)abstractelementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getcontext_declaration()
  {
    return context_declarationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getcontext_declaration_Name()
  {
    return (EAttribute)context_declarationEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getcontext_declaration_Modalite()
  {
    return (EReference)context_declarationEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getcontext_declaration_Modalite1()
  {
    return (EReference)context_declarationEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getMiddleSection()
  {
    return middleSectionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMiddleSection_FirstSection1()
  {
    return (EReference)middleSectionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMiddleSection_SecondSection2()
  {
    return (EReference)middleSectionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getMiddleSection_ThirdSection3()
  {
    return (EReference)middleSectionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getThirdSection()
  {
    return thirdSectionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getThirdSection_Resultat1()
  {
    return (EReference)thirdSectionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRETOUR1()
  {
    return retour1EClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getRETOUR1_Name()
  {
    return (EAttribute)retour1EClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFirstSection()
  {
    return firstSectionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getFirstSection_Name()
  {
    return (EAttribute)firstSectionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSecondSection()
  {
    return secondSectionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSecondSection_Resultat12()
  {
    return (EReference)secondSectionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSecondSection_Resultat1()
  {
    return (EReference)secondSectionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRETOUR12()
  {
    return retour12EClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getRETOUR12_Name()
  {
    return (EAttribute)retour12EClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLastSection()
  {
    return lastSectionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getLastSection_Resultat()
  {
    return (EAttribute)lastSectionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLastSection_Debut1()
  {
    return (EReference)lastSectionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getLastSection_Resultat4()
  {
    return (EAttribute)lastSectionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLastSection_Milieu1()
  {
    return (EReference)lastSectionEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLastSection_Fin1()
  {
    return (EReference)lastSectionEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFin()
  {
    return finEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getFin_Name()
  {
    return (EAttribute)finEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getMilieu()
  {
    return milieuEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getMilieu_Name()
  {
    return (EAttribute)milieuEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDebut()
  {
    return debutEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getDebut_Name()
  {
    return (EAttribute)debutEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getdeclaration_sentence()
  {
    return declaration_sentenceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getdeclaration_sentence_Name()
  {
    return (EAttribute)declaration_sentenceEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getdeclaration_sentence_Deux()
  {
    return (EAttribute)declaration_sentenceEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getdeclaration_sentence_Trois()
  {
    return (EAttribute)declaration_sentenceEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MyOcl2Factory getMyOcl2Factory()
  {
    return (MyOcl2Factory)getEFactoryInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isCreated = false;

  /**
   * Creates the meta-model objects for the package.  This method is
   * guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void createPackageContents()
  {
    if (isCreated) return;
    isCreated = true;

    // Create classes and their features
    mainEClass = createEClass(MAIN);
    createEReference(mainEClass, MAIN__IMPORT_SENTENCE);
    createEReference(mainEClass, MAIN__PACKAGE);

    package_declarationEClass = createEClass(PACKAGE_DECLARATION);
    createEReference(package_declarationEClass, PACKAGE_DECLARATION__SHAREDELEMENT);
    createEReference(package_declarationEClass, PACKAGE_DECLARATION__ELEMENT);

    endstartelementEClass = createEClass(ENDSTARTELEMENT);
    createEAttribute(endstartelementEClass, ENDSTARTELEMENT__NAME);

    abstractelementEClass = createEClass(ABSTRACTELEMENT);
    createEReference(abstractelementEClass, ABSTRACTELEMENT__ELEMENTS);

    context_declarationEClass = createEClass(CONTEXT_DECLARATION);
    createEAttribute(context_declarationEClass, CONTEXT_DECLARATION__NAME);
    createEReference(context_declarationEClass, CONTEXT_DECLARATION__MODALITE);
    createEReference(context_declarationEClass, CONTEXT_DECLARATION__MODALITE1);

    middleSectionEClass = createEClass(MIDDLE_SECTION);
    createEReference(middleSectionEClass, MIDDLE_SECTION__FIRST_SECTION1);
    createEReference(middleSectionEClass, MIDDLE_SECTION__SECOND_SECTION2);
    createEReference(middleSectionEClass, MIDDLE_SECTION__THIRD_SECTION3);

    thirdSectionEClass = createEClass(THIRD_SECTION);
    createEReference(thirdSectionEClass, THIRD_SECTION__RESULTAT1);

    retour1EClass = createEClass(RETOUR1);
    createEAttribute(retour1EClass, RETOUR1__NAME);

    firstSectionEClass = createEClass(FIRST_SECTION);
    createEAttribute(firstSectionEClass, FIRST_SECTION__NAME);

    secondSectionEClass = createEClass(SECOND_SECTION);
    createEReference(secondSectionEClass, SECOND_SECTION__RESULTAT12);
    createEReference(secondSectionEClass, SECOND_SECTION__RESULTAT1);

    retour12EClass = createEClass(RETOUR12);
    createEAttribute(retour12EClass, RETOUR12__NAME);

    lastSectionEClass = createEClass(LAST_SECTION);
    createEAttribute(lastSectionEClass, LAST_SECTION__RESULTAT);
    createEReference(lastSectionEClass, LAST_SECTION__DEBUT1);
    createEAttribute(lastSectionEClass, LAST_SECTION__RESULTAT4);
    createEReference(lastSectionEClass, LAST_SECTION__MILIEU1);
    createEReference(lastSectionEClass, LAST_SECTION__FIN1);

    finEClass = createEClass(FIN);
    createEAttribute(finEClass, FIN__NAME);

    milieuEClass = createEClass(MILIEU);
    createEAttribute(milieuEClass, MILIEU__NAME);

    debutEClass = createEClass(DEBUT);
    createEAttribute(debutEClass, DEBUT__NAME);

    declaration_sentenceEClass = createEClass(DECLARATION_SENTENCE);
    createEAttribute(declaration_sentenceEClass, DECLARATION_SENTENCE__NAME);
    createEAttribute(declaration_sentenceEClass, DECLARATION_SENTENCE__DEUX);
    createEAttribute(declaration_sentenceEClass, DECLARATION_SENTENCE__TROIS);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isInitialized = false;

  /**
   * Complete the initialization of the package and its meta-model.  This
   * method is guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void initializePackageContents()
  {
    if (isInitialized) return;
    isInitialized = true;

    // Initialize package
    setName(eNAME);
    setNsPrefix(eNS_PREFIX);
    setNsURI(eNS_URI);

    // Create type parameters

    // Set bounds for type parameters

    // Add supertypes to classes

    // Initialize classes and features; add operations and parameters
    initEClass(mainEClass, Main.class, "Main", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getMain_Import_sentence(), this.getdeclaration_sentence(), null, "import_sentence", null, 0, -1, Main.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getMain_Package(), this.getpackage_declaration(), null, "Package", null, 0, 1, Main.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(package_declarationEClass, package_declaration.class, "package_declaration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getpackage_declaration_Sharedelement(), this.getendstartelement(), null, "sharedelement", null, 0, 1, package_declaration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getpackage_declaration_Element(), this.getabstractelement(), null, "element", null, 0, -1, package_declaration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(endstartelementEClass, endstartelement.class, "endstartelement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getendstartelement_Name(), ecorePackage.getEString(), "name", null, 0, 1, endstartelement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(abstractelementEClass, abstractelement.class, "abstractelement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getabstractelement_Elements(), this.getcontext_declaration(), null, "elements", null, 0, -1, abstractelement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(context_declarationEClass, context_declaration.class, "context_declaration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getcontext_declaration_Name(), ecorePackage.getEString(), "name", null, 0, 1, context_declaration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getcontext_declaration_Modalite(), this.getMiddleSection(), null, "Modalite", null, 0, -1, context_declaration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getcontext_declaration_Modalite1(), this.getLastSection(), null, "Modalite1", null, 0, -1, context_declaration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(middleSectionEClass, MiddleSection.class, "MiddleSection", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getMiddleSection_FirstSection1(), this.getFirstSection(), null, "FirstSection1", null, 0, -1, MiddleSection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getMiddleSection_SecondSection2(), this.getSecondSection(), null, "SecondSection2", null, 0, -1, MiddleSection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getMiddleSection_ThirdSection3(), this.getThirdSection(), null, "ThirdSection3", null, 0, -1, MiddleSection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(thirdSectionEClass, ThirdSection.class, "ThirdSection", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getThirdSection_Resultat1(), this.getRETOUR1(), null, "resultat1", null, 0, 1, ThirdSection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(retour1EClass, org.xtext.example.mydsl.myOcl2.RETOUR1.class, "RETOUR1", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getRETOUR1_Name(), ecorePackage.getEString(), "name", null, 0, 1, org.xtext.example.mydsl.myOcl2.RETOUR1.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(firstSectionEClass, FirstSection.class, "FirstSection", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getFirstSection_Name(), ecorePackage.getEString(), "name", null, 0, 1, FirstSection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(secondSectionEClass, SecondSection.class, "SecondSection", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getSecondSection_Resultat12(), this.getRETOUR12(), null, "resultat12", null, 0, 1, SecondSection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSecondSection_Resultat1(), this.getRETOUR1(), null, "resultat1", null, 0, 1, SecondSection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(retour12EClass, org.xtext.example.mydsl.myOcl2.RETOUR12.class, "RETOUR12", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getRETOUR12_Name(), ecorePackage.getEString(), "name", null, 0, 1, org.xtext.example.mydsl.myOcl2.RETOUR12.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(lastSectionEClass, LastSection.class, "LastSection", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getLastSection_Resultat(), ecorePackage.getEString(), "resultat", null, 0, 1, LastSection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getLastSection_Debut1(), this.getDebut(), null, "Debut1", null, 0, 1, LastSection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getLastSection_Resultat4(), ecorePackage.getEString(), "resultat4", null, 0, 1, LastSection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getLastSection_Milieu1(), this.getMilieu(), null, "Milieu1", null, 0, 1, LastSection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getLastSection_Fin1(), this.getFin(), null, "Fin1", null, 0, 1, LastSection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(finEClass, Fin.class, "Fin", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getFin_Name(), ecorePackage.getEString(), "name", null, 0, 1, Fin.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(milieuEClass, Milieu.class, "Milieu", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getMilieu_Name(), ecorePackage.getEInt(), "name", null, 0, 1, Milieu.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(debutEClass, Debut.class, "Debut", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getDebut_Name(), ecorePackage.getEString(), "name", null, 0, 1, Debut.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(declaration_sentenceEClass, declaration_sentence.class, "declaration_sentence", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getdeclaration_sentence_Name(), ecorePackage.getEString(), "name", null, 0, 1, declaration_sentence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getdeclaration_sentence_Deux(), ecorePackage.getEString(), "deux", null, 0, 1, declaration_sentence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getdeclaration_sentence_Trois(), ecorePackage.getEString(), "trois", null, 0, 1, declaration_sentence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    // Create resource
    createResource(eNS_URI);
  }

} //MyOcl2PackageImpl
