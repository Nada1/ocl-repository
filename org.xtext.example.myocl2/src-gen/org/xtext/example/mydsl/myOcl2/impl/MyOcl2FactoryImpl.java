/**
 */
package org.xtext.example.mydsl.myOcl2.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.xtext.example.mydsl.myOcl2.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MyOcl2FactoryImpl extends EFactoryImpl implements MyOcl2Factory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static MyOcl2Factory init()
  {
    try
    {
      MyOcl2Factory theMyOcl2Factory = (MyOcl2Factory)EPackage.Registry.INSTANCE.getEFactory(MyOcl2Package.eNS_URI);
      if (theMyOcl2Factory != null)
      {
        return theMyOcl2Factory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new MyOcl2FactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MyOcl2FactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case MyOcl2Package.MAIN: return createMain();
      case MyOcl2Package.PACKAGE_DECLARATION: return createpackage_declaration();
      case MyOcl2Package.ENDSTARTELEMENT: return createendstartelement();
      case MyOcl2Package.ABSTRACTELEMENT: return createabstractelement();
      case MyOcl2Package.CONTEXT_DECLARATION: return createcontext_declaration();
      case MyOcl2Package.MIDDLE_SECTION: return createMiddleSection();
      case MyOcl2Package.THIRD_SECTION: return createThirdSection();
      case MyOcl2Package.RETOUR1: return createRETOUR1();
      case MyOcl2Package.FIRST_SECTION: return createFirstSection();
      case MyOcl2Package.SECOND_SECTION: return createSecondSection();
      case MyOcl2Package.RETOUR12: return createRETOUR12();
      case MyOcl2Package.LAST_SECTION: return createLastSection();
      case MyOcl2Package.FIN: return createFin();
      case MyOcl2Package.MILIEU: return createMilieu();
      case MyOcl2Package.DEBUT: return createDebut();
      case MyOcl2Package.DECLARATION_SENTENCE: return createdeclaration_sentence();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Main createMain()
  {
    MainImpl main = new MainImpl();
    return main;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public package_declaration createpackage_declaration()
  {
    package_declarationImpl package_declaration = new package_declarationImpl();
    return package_declaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public endstartelement createendstartelement()
  {
    endstartelementImpl endstartelement = new endstartelementImpl();
    return endstartelement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public abstractelement createabstractelement()
  {
    abstractelementImpl abstractelement = new abstractelementImpl();
    return abstractelement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public context_declaration createcontext_declaration()
  {
    context_declarationImpl context_declaration = new context_declarationImpl();
    return context_declaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MiddleSection createMiddleSection()
  {
    MiddleSectionImpl middleSection = new MiddleSectionImpl();
    return middleSection;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ThirdSection createThirdSection()
  {
    ThirdSectionImpl thirdSection = new ThirdSectionImpl();
    return thirdSection;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RETOUR1 createRETOUR1()
  {
    RETOUR1Impl retour1 = new RETOUR1Impl();
    return retour1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FirstSection createFirstSection()
  {
    FirstSectionImpl firstSection = new FirstSectionImpl();
    return firstSection;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SecondSection createSecondSection()
  {
    SecondSectionImpl secondSection = new SecondSectionImpl();
    return secondSection;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RETOUR12 createRETOUR12()
  {
    RETOUR12Impl retour12 = new RETOUR12Impl();
    return retour12;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LastSection createLastSection()
  {
    LastSectionImpl lastSection = new LastSectionImpl();
    return lastSection;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Fin createFin()
  {
    FinImpl fin = new FinImpl();
    return fin;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Milieu createMilieu()
  {
    MilieuImpl milieu = new MilieuImpl();
    return milieu;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Debut createDebut()
  {
    DebutImpl debut = new DebutImpl();
    return debut;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public declaration_sentence createdeclaration_sentence()
  {
    declaration_sentenceImpl declaration_sentence = new declaration_sentenceImpl();
    return declaration_sentence;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MyOcl2Package getMyOcl2Package()
  {
    return (MyOcl2Package)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static MyOcl2Package getPackage()
  {
    return MyOcl2Package.eINSTANCE;
  }

} //MyOcl2FactoryImpl
