/**
 */
package org.xtext.example.mydsl.myOcl2.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.xtext.example.mydsl.myOcl2.LastSection;
import org.xtext.example.mydsl.myOcl2.MiddleSection;
import org.xtext.example.mydsl.myOcl2.MyOcl2Package;
import org.xtext.example.mydsl.myOcl2.context_declaration;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>context declaration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.impl.context_declarationImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.impl.context_declarationImpl#getModalite <em>Modalite</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.impl.context_declarationImpl#getModalite1 <em>Modalite1</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class context_declarationImpl extends MinimalEObjectImpl.Container implements context_declaration
{
  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The cached value of the '{@link #getModalite() <em>Modalite</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getModalite()
   * @generated
   * @ordered
   */
  protected EList<MiddleSection> modalite;

  /**
   * The cached value of the '{@link #getModalite1() <em>Modalite1</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getModalite1()
   * @generated
   * @ordered
   */
  protected EList<LastSection> modalite1;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected context_declarationImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return MyOcl2Package.Literals.CONTEXT_DECLARATION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MyOcl2Package.CONTEXT_DECLARATION__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<MiddleSection> getModalite()
  {
    if (modalite == null)
    {
      modalite = new EObjectContainmentEList<MiddleSection>(MiddleSection.class, this, MyOcl2Package.CONTEXT_DECLARATION__MODALITE);
    }
    return modalite;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<LastSection> getModalite1()
  {
    if (modalite1 == null)
    {
      modalite1 = new EObjectContainmentEList<LastSection>(LastSection.class, this, MyOcl2Package.CONTEXT_DECLARATION__MODALITE1);
    }
    return modalite1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case MyOcl2Package.CONTEXT_DECLARATION__MODALITE:
        return ((InternalEList<?>)getModalite()).basicRemove(otherEnd, msgs);
      case MyOcl2Package.CONTEXT_DECLARATION__MODALITE1:
        return ((InternalEList<?>)getModalite1()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case MyOcl2Package.CONTEXT_DECLARATION__NAME:
        return getName();
      case MyOcl2Package.CONTEXT_DECLARATION__MODALITE:
        return getModalite();
      case MyOcl2Package.CONTEXT_DECLARATION__MODALITE1:
        return getModalite1();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case MyOcl2Package.CONTEXT_DECLARATION__NAME:
        setName((String)newValue);
        return;
      case MyOcl2Package.CONTEXT_DECLARATION__MODALITE:
        getModalite().clear();
        getModalite().addAll((Collection<? extends MiddleSection>)newValue);
        return;
      case MyOcl2Package.CONTEXT_DECLARATION__MODALITE1:
        getModalite1().clear();
        getModalite1().addAll((Collection<? extends LastSection>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case MyOcl2Package.CONTEXT_DECLARATION__NAME:
        setName(NAME_EDEFAULT);
        return;
      case MyOcl2Package.CONTEXT_DECLARATION__MODALITE:
        getModalite().clear();
        return;
      case MyOcl2Package.CONTEXT_DECLARATION__MODALITE1:
        getModalite1().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case MyOcl2Package.CONTEXT_DECLARATION__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case MyOcl2Package.CONTEXT_DECLARATION__MODALITE:
        return modalite != null && !modalite.isEmpty();
      case MyOcl2Package.CONTEXT_DECLARATION__MODALITE1:
        return modalite1 != null && !modalite1.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(')');
    return result.toString();
  }

} //context_declarationImpl
