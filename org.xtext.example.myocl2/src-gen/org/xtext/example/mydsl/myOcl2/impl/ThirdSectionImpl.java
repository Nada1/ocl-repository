/**
 */
package org.xtext.example.mydsl.myOcl2.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.xtext.example.mydsl.myOcl2.MyOcl2Package;
import org.xtext.example.mydsl.myOcl2.RETOUR1;
import org.xtext.example.mydsl.myOcl2.ThirdSection;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Third Section</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.impl.ThirdSectionImpl#getResultat1 <em>Resultat1</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ThirdSectionImpl extends MinimalEObjectImpl.Container implements ThirdSection
{
  /**
   * The cached value of the '{@link #getResultat1() <em>Resultat1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getResultat1()
   * @generated
   * @ordered
   */
  protected RETOUR1 resultat1;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ThirdSectionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return MyOcl2Package.Literals.THIRD_SECTION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RETOUR1 getResultat1()
  {
    return resultat1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetResultat1(RETOUR1 newResultat1, NotificationChain msgs)
  {
    RETOUR1 oldResultat1 = resultat1;
    resultat1 = newResultat1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MyOcl2Package.THIRD_SECTION__RESULTAT1, oldResultat1, newResultat1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setResultat1(RETOUR1 newResultat1)
  {
    if (newResultat1 != resultat1)
    {
      NotificationChain msgs = null;
      if (resultat1 != null)
        msgs = ((InternalEObject)resultat1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MyOcl2Package.THIRD_SECTION__RESULTAT1, null, msgs);
      if (newResultat1 != null)
        msgs = ((InternalEObject)newResultat1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MyOcl2Package.THIRD_SECTION__RESULTAT1, null, msgs);
      msgs = basicSetResultat1(newResultat1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MyOcl2Package.THIRD_SECTION__RESULTAT1, newResultat1, newResultat1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case MyOcl2Package.THIRD_SECTION__RESULTAT1:
        return basicSetResultat1(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case MyOcl2Package.THIRD_SECTION__RESULTAT1:
        return getResultat1();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case MyOcl2Package.THIRD_SECTION__RESULTAT1:
        setResultat1((RETOUR1)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case MyOcl2Package.THIRD_SECTION__RESULTAT1:
        setResultat1((RETOUR1)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case MyOcl2Package.THIRD_SECTION__RESULTAT1:
        return resultat1 != null;
    }
    return super.eIsSet(featureID);
  }

} //ThirdSectionImpl
