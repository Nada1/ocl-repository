/**
 */
package org.xtext.example.mydsl.myOcl2.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.xtext.example.mydsl.myOcl2.MyOcl2Package;
import org.xtext.example.mydsl.myOcl2.declaration_sentence;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>declaration sentence</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.impl.declaration_sentenceImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.impl.declaration_sentenceImpl#getDeux <em>Deux</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.impl.declaration_sentenceImpl#getTrois <em>Trois</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class declaration_sentenceImpl extends MinimalEObjectImpl.Container implements declaration_sentence
{
  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The default value of the '{@link #getDeux() <em>Deux</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDeux()
   * @generated
   * @ordered
   */
  protected static final String DEUX_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getDeux() <em>Deux</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDeux()
   * @generated
   * @ordered
   */
  protected String deux = DEUX_EDEFAULT;

  /**
   * The default value of the '{@link #getTrois() <em>Trois</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTrois()
   * @generated
   * @ordered
   */
  protected static final String TROIS_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getTrois() <em>Trois</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTrois()
   * @generated
   * @ordered
   */
  protected String trois = TROIS_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected declaration_sentenceImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return MyOcl2Package.Literals.DECLARATION_SENTENCE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MyOcl2Package.DECLARATION_SENTENCE__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDeux()
  {
    return deux;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDeux(String newDeux)
  {
    String oldDeux = deux;
    deux = newDeux;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MyOcl2Package.DECLARATION_SENTENCE__DEUX, oldDeux, deux));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getTrois()
  {
    return trois;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTrois(String newTrois)
  {
    String oldTrois = trois;
    trois = newTrois;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MyOcl2Package.DECLARATION_SENTENCE__TROIS, oldTrois, trois));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case MyOcl2Package.DECLARATION_SENTENCE__NAME:
        return getName();
      case MyOcl2Package.DECLARATION_SENTENCE__DEUX:
        return getDeux();
      case MyOcl2Package.DECLARATION_SENTENCE__TROIS:
        return getTrois();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case MyOcl2Package.DECLARATION_SENTENCE__NAME:
        setName((String)newValue);
        return;
      case MyOcl2Package.DECLARATION_SENTENCE__DEUX:
        setDeux((String)newValue);
        return;
      case MyOcl2Package.DECLARATION_SENTENCE__TROIS:
        setTrois((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case MyOcl2Package.DECLARATION_SENTENCE__NAME:
        setName(NAME_EDEFAULT);
        return;
      case MyOcl2Package.DECLARATION_SENTENCE__DEUX:
        setDeux(DEUX_EDEFAULT);
        return;
      case MyOcl2Package.DECLARATION_SENTENCE__TROIS:
        setTrois(TROIS_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case MyOcl2Package.DECLARATION_SENTENCE__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case MyOcl2Package.DECLARATION_SENTENCE__DEUX:
        return DEUX_EDEFAULT == null ? deux != null : !DEUX_EDEFAULT.equals(deux);
      case MyOcl2Package.DECLARATION_SENTENCE__TROIS:
        return TROIS_EDEFAULT == null ? trois != null : !TROIS_EDEFAULT.equals(trois);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(", deux: ");
    result.append(deux);
    result.append(", trois: ");
    result.append(trois);
    result.append(')');
    return result.toString();
  }

} //declaration_sentenceImpl
