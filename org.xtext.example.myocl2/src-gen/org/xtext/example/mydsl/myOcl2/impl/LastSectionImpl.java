/**
 */
package org.xtext.example.mydsl.myOcl2.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.xtext.example.mydsl.myOcl2.Debut;
import org.xtext.example.mydsl.myOcl2.Fin;
import org.xtext.example.mydsl.myOcl2.LastSection;
import org.xtext.example.mydsl.myOcl2.Milieu;
import org.xtext.example.mydsl.myOcl2.MyOcl2Package;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Last Section</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.impl.LastSectionImpl#getResultat <em>Resultat</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.impl.LastSectionImpl#getDebut1 <em>Debut1</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.impl.LastSectionImpl#getResultat4 <em>Resultat4</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.impl.LastSectionImpl#getMilieu1 <em>Milieu1</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.impl.LastSectionImpl#getFin1 <em>Fin1</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class LastSectionImpl extends MinimalEObjectImpl.Container implements LastSection
{
  /**
   * The default value of the '{@link #getResultat() <em>Resultat</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getResultat()
   * @generated
   * @ordered
   */
  protected static final String RESULTAT_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getResultat() <em>Resultat</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getResultat()
   * @generated
   * @ordered
   */
  protected String resultat = RESULTAT_EDEFAULT;

  /**
   * The cached value of the '{@link #getDebut1() <em>Debut1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDebut1()
   * @generated
   * @ordered
   */
  protected Debut debut1;

  /**
   * The default value of the '{@link #getResultat4() <em>Resultat4</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getResultat4()
   * @generated
   * @ordered
   */
  protected static final String RESULTAT4_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getResultat4() <em>Resultat4</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getResultat4()
   * @generated
   * @ordered
   */
  protected String resultat4 = RESULTAT4_EDEFAULT;

  /**
   * The cached value of the '{@link #getMilieu1() <em>Milieu1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMilieu1()
   * @generated
   * @ordered
   */
  protected Milieu milieu1;

  /**
   * The cached value of the '{@link #getFin1() <em>Fin1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFin1()
   * @generated
   * @ordered
   */
  protected Fin fin1;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected LastSectionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return MyOcl2Package.Literals.LAST_SECTION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getResultat()
  {
    return resultat;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setResultat(String newResultat)
  {
    String oldResultat = resultat;
    resultat = newResultat;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MyOcl2Package.LAST_SECTION__RESULTAT, oldResultat, resultat));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Debut getDebut1()
  {
    return debut1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDebut1(Debut newDebut1, NotificationChain msgs)
  {
    Debut oldDebut1 = debut1;
    debut1 = newDebut1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MyOcl2Package.LAST_SECTION__DEBUT1, oldDebut1, newDebut1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDebut1(Debut newDebut1)
  {
    if (newDebut1 != debut1)
    {
      NotificationChain msgs = null;
      if (debut1 != null)
        msgs = ((InternalEObject)debut1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MyOcl2Package.LAST_SECTION__DEBUT1, null, msgs);
      if (newDebut1 != null)
        msgs = ((InternalEObject)newDebut1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MyOcl2Package.LAST_SECTION__DEBUT1, null, msgs);
      msgs = basicSetDebut1(newDebut1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MyOcl2Package.LAST_SECTION__DEBUT1, newDebut1, newDebut1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getResultat4()
  {
    return resultat4;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setResultat4(String newResultat4)
  {
    String oldResultat4 = resultat4;
    resultat4 = newResultat4;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MyOcl2Package.LAST_SECTION__RESULTAT4, oldResultat4, resultat4));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Milieu getMilieu1()
  {
    return milieu1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetMilieu1(Milieu newMilieu1, NotificationChain msgs)
  {
    Milieu oldMilieu1 = milieu1;
    milieu1 = newMilieu1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MyOcl2Package.LAST_SECTION__MILIEU1, oldMilieu1, newMilieu1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMilieu1(Milieu newMilieu1)
  {
    if (newMilieu1 != milieu1)
    {
      NotificationChain msgs = null;
      if (milieu1 != null)
        msgs = ((InternalEObject)milieu1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MyOcl2Package.LAST_SECTION__MILIEU1, null, msgs);
      if (newMilieu1 != null)
        msgs = ((InternalEObject)newMilieu1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MyOcl2Package.LAST_SECTION__MILIEU1, null, msgs);
      msgs = basicSetMilieu1(newMilieu1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MyOcl2Package.LAST_SECTION__MILIEU1, newMilieu1, newMilieu1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Fin getFin1()
  {
    return fin1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetFin1(Fin newFin1, NotificationChain msgs)
  {
    Fin oldFin1 = fin1;
    fin1 = newFin1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MyOcl2Package.LAST_SECTION__FIN1, oldFin1, newFin1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFin1(Fin newFin1)
  {
    if (newFin1 != fin1)
    {
      NotificationChain msgs = null;
      if (fin1 != null)
        msgs = ((InternalEObject)fin1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MyOcl2Package.LAST_SECTION__FIN1, null, msgs);
      if (newFin1 != null)
        msgs = ((InternalEObject)newFin1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MyOcl2Package.LAST_SECTION__FIN1, null, msgs);
      msgs = basicSetFin1(newFin1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MyOcl2Package.LAST_SECTION__FIN1, newFin1, newFin1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case MyOcl2Package.LAST_SECTION__DEBUT1:
        return basicSetDebut1(null, msgs);
      case MyOcl2Package.LAST_SECTION__MILIEU1:
        return basicSetMilieu1(null, msgs);
      case MyOcl2Package.LAST_SECTION__FIN1:
        return basicSetFin1(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case MyOcl2Package.LAST_SECTION__RESULTAT:
        return getResultat();
      case MyOcl2Package.LAST_SECTION__DEBUT1:
        return getDebut1();
      case MyOcl2Package.LAST_SECTION__RESULTAT4:
        return getResultat4();
      case MyOcl2Package.LAST_SECTION__MILIEU1:
        return getMilieu1();
      case MyOcl2Package.LAST_SECTION__FIN1:
        return getFin1();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case MyOcl2Package.LAST_SECTION__RESULTAT:
        setResultat((String)newValue);
        return;
      case MyOcl2Package.LAST_SECTION__DEBUT1:
        setDebut1((Debut)newValue);
        return;
      case MyOcl2Package.LAST_SECTION__RESULTAT4:
        setResultat4((String)newValue);
        return;
      case MyOcl2Package.LAST_SECTION__MILIEU1:
        setMilieu1((Milieu)newValue);
        return;
      case MyOcl2Package.LAST_SECTION__FIN1:
        setFin1((Fin)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case MyOcl2Package.LAST_SECTION__RESULTAT:
        setResultat(RESULTAT_EDEFAULT);
        return;
      case MyOcl2Package.LAST_SECTION__DEBUT1:
        setDebut1((Debut)null);
        return;
      case MyOcl2Package.LAST_SECTION__RESULTAT4:
        setResultat4(RESULTAT4_EDEFAULT);
        return;
      case MyOcl2Package.LAST_SECTION__MILIEU1:
        setMilieu1((Milieu)null);
        return;
      case MyOcl2Package.LAST_SECTION__FIN1:
        setFin1((Fin)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case MyOcl2Package.LAST_SECTION__RESULTAT:
        return RESULTAT_EDEFAULT == null ? resultat != null : !RESULTAT_EDEFAULT.equals(resultat);
      case MyOcl2Package.LAST_SECTION__DEBUT1:
        return debut1 != null;
      case MyOcl2Package.LAST_SECTION__RESULTAT4:
        return RESULTAT4_EDEFAULT == null ? resultat4 != null : !RESULTAT4_EDEFAULT.equals(resultat4);
      case MyOcl2Package.LAST_SECTION__MILIEU1:
        return milieu1 != null;
      case MyOcl2Package.LAST_SECTION__FIN1:
        return fin1 != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (resultat: ");
    result.append(resultat);
    result.append(", resultat4: ");
    result.append(resultat4);
    result.append(')');
    return result.toString();
  }

} //LastSectionImpl
