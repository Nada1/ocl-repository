/**
 */
package org.xtext.example.mydsl.myOcl2.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.xtext.example.mydsl.myOcl2.MyOcl2Package;
import org.xtext.example.mydsl.myOcl2.RETOUR1;
import org.xtext.example.mydsl.myOcl2.RETOUR12;
import org.xtext.example.mydsl.myOcl2.SecondSection;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Second Section</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.impl.SecondSectionImpl#getResultat12 <em>Resultat12</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.impl.SecondSectionImpl#getResultat1 <em>Resultat1</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SecondSectionImpl extends MinimalEObjectImpl.Container implements SecondSection
{
  /**
   * The cached value of the '{@link #getResultat12() <em>Resultat12</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getResultat12()
   * @generated
   * @ordered
   */
  protected RETOUR12 resultat12;

  /**
   * The cached value of the '{@link #getResultat1() <em>Resultat1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getResultat1()
   * @generated
   * @ordered
   */
  protected RETOUR1 resultat1;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SecondSectionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return MyOcl2Package.Literals.SECOND_SECTION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RETOUR12 getResultat12()
  {
    return resultat12;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetResultat12(RETOUR12 newResultat12, NotificationChain msgs)
  {
    RETOUR12 oldResultat12 = resultat12;
    resultat12 = newResultat12;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MyOcl2Package.SECOND_SECTION__RESULTAT12, oldResultat12, newResultat12);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setResultat12(RETOUR12 newResultat12)
  {
    if (newResultat12 != resultat12)
    {
      NotificationChain msgs = null;
      if (resultat12 != null)
        msgs = ((InternalEObject)resultat12).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MyOcl2Package.SECOND_SECTION__RESULTAT12, null, msgs);
      if (newResultat12 != null)
        msgs = ((InternalEObject)newResultat12).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MyOcl2Package.SECOND_SECTION__RESULTAT12, null, msgs);
      msgs = basicSetResultat12(newResultat12, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MyOcl2Package.SECOND_SECTION__RESULTAT12, newResultat12, newResultat12));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RETOUR1 getResultat1()
  {
    return resultat1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetResultat1(RETOUR1 newResultat1, NotificationChain msgs)
  {
    RETOUR1 oldResultat1 = resultat1;
    resultat1 = newResultat1;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MyOcl2Package.SECOND_SECTION__RESULTAT1, oldResultat1, newResultat1);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setResultat1(RETOUR1 newResultat1)
  {
    if (newResultat1 != resultat1)
    {
      NotificationChain msgs = null;
      if (resultat1 != null)
        msgs = ((InternalEObject)resultat1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MyOcl2Package.SECOND_SECTION__RESULTAT1, null, msgs);
      if (newResultat1 != null)
        msgs = ((InternalEObject)newResultat1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MyOcl2Package.SECOND_SECTION__RESULTAT1, null, msgs);
      msgs = basicSetResultat1(newResultat1, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MyOcl2Package.SECOND_SECTION__RESULTAT1, newResultat1, newResultat1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case MyOcl2Package.SECOND_SECTION__RESULTAT12:
        return basicSetResultat12(null, msgs);
      case MyOcl2Package.SECOND_SECTION__RESULTAT1:
        return basicSetResultat1(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case MyOcl2Package.SECOND_SECTION__RESULTAT12:
        return getResultat12();
      case MyOcl2Package.SECOND_SECTION__RESULTAT1:
        return getResultat1();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case MyOcl2Package.SECOND_SECTION__RESULTAT12:
        setResultat12((RETOUR12)newValue);
        return;
      case MyOcl2Package.SECOND_SECTION__RESULTAT1:
        setResultat1((RETOUR1)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case MyOcl2Package.SECOND_SECTION__RESULTAT12:
        setResultat12((RETOUR12)null);
        return;
      case MyOcl2Package.SECOND_SECTION__RESULTAT1:
        setResultat1((RETOUR1)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case MyOcl2Package.SECOND_SECTION__RESULTAT12:
        return resultat12 != null;
      case MyOcl2Package.SECOND_SECTION__RESULTAT1:
        return resultat1 != null;
    }
    return super.eIsSet(featureID);
  }

} //SecondSectionImpl
