/**
 */
package org.xtext.example.mydsl.myOcl2;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Middle Section</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.MiddleSection#getFirstSection1 <em>First Section1</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.MiddleSection#getSecondSection2 <em>Second Section2</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.MiddleSection#getThirdSection3 <em>Third Section3</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.example.mydsl.myOcl2.MyOcl2Package#getMiddleSection()
 * @model
 * @generated
 */
public interface MiddleSection extends EObject
{
  /**
   * Returns the value of the '<em><b>First Section1</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.example.mydsl.myOcl2.FirstSection}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>First Section1</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>First Section1</em>' containment reference list.
   * @see org.xtext.example.mydsl.myOcl2.MyOcl2Package#getMiddleSection_FirstSection1()
   * @model containment="true"
   * @generated
   */
  EList<FirstSection> getFirstSection1();

  /**
   * Returns the value of the '<em><b>Second Section2</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.example.mydsl.myOcl2.SecondSection}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Second Section2</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Second Section2</em>' containment reference list.
   * @see org.xtext.example.mydsl.myOcl2.MyOcl2Package#getMiddleSection_SecondSection2()
   * @model containment="true"
   * @generated
   */
  EList<SecondSection> getSecondSection2();

  /**
   * Returns the value of the '<em><b>Third Section3</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.example.mydsl.myOcl2.ThirdSection}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Third Section3</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Third Section3</em>' containment reference list.
   * @see org.xtext.example.mydsl.myOcl2.MyOcl2Package#getMiddleSection_ThirdSection3()
   * @model containment="true"
   * @generated
   */
  EList<ThirdSection> getThirdSection3();

} // MiddleSection
