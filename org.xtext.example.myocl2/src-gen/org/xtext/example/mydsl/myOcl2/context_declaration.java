/**
 */
package org.xtext.example.mydsl.myOcl2;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>context declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.context_declaration#getName <em>Name</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.context_declaration#getModalite <em>Modalite</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.context_declaration#getModalite1 <em>Modalite1</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.example.mydsl.myOcl2.MyOcl2Package#getcontext_declaration()
 * @model
 * @generated
 */
public interface context_declaration extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see org.xtext.example.mydsl.myOcl2.MyOcl2Package#getcontext_declaration_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.myOcl2.context_declaration#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Modalite</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.example.mydsl.myOcl2.MiddleSection}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Modalite</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Modalite</em>' containment reference list.
   * @see org.xtext.example.mydsl.myOcl2.MyOcl2Package#getcontext_declaration_Modalite()
   * @model containment="true"
   * @generated
   */
  EList<MiddleSection> getModalite();

  /**
   * Returns the value of the '<em><b>Modalite1</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.example.mydsl.myOcl2.LastSection}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Modalite1</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Modalite1</em>' containment reference list.
   * @see org.xtext.example.mydsl.myOcl2.MyOcl2Package#getcontext_declaration_Modalite1()
   * @model containment="true"
   * @generated
   */
  EList<LastSection> getModalite1();

} // context_declaration
