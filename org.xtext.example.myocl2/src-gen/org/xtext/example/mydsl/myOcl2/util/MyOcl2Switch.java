/**
 */
package org.xtext.example.mydsl.myOcl2.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.xtext.example.mydsl.myOcl2.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.xtext.example.mydsl.myOcl2.MyOcl2Package
 * @generated
 */
public class MyOcl2Switch<T> extends Switch<T>
{
  /**
   * The cached model package
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static MyOcl2Package modelPackage;

  /**
   * Creates an instance of the switch.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MyOcl2Switch()
  {
    if (modelPackage == null)
    {
      modelPackage = MyOcl2Package.eINSTANCE;
    }
  }

  /**
   * Checks whether this is a switch for the given package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @parameter ePackage the package in question.
   * @return whether this is a switch for the given package.
   * @generated
   */
  @Override
  protected boolean isSwitchFor(EPackage ePackage)
  {
    return ePackage == modelPackage;
  }

  /**
   * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the first non-null result returned by a <code>caseXXX</code> call.
   * @generated
   */
  @Override
  protected T doSwitch(int classifierID, EObject theEObject)
  {
    switch (classifierID)
    {
      case MyOcl2Package.MAIN:
      {
        Main main = (Main)theEObject;
        T result = caseMain(main);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MyOcl2Package.PACKAGE_DECLARATION:
      {
        package_declaration package_declaration = (package_declaration)theEObject;
        T result = casepackage_declaration(package_declaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MyOcl2Package.ENDSTARTELEMENT:
      {
        endstartelement endstartelement = (endstartelement)theEObject;
        T result = caseendstartelement(endstartelement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MyOcl2Package.ABSTRACTELEMENT:
      {
        abstractelement abstractelement = (abstractelement)theEObject;
        T result = caseabstractelement(abstractelement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MyOcl2Package.CONTEXT_DECLARATION:
      {
        context_declaration context_declaration = (context_declaration)theEObject;
        T result = casecontext_declaration(context_declaration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MyOcl2Package.MIDDLE_SECTION:
      {
        MiddleSection middleSection = (MiddleSection)theEObject;
        T result = caseMiddleSection(middleSection);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MyOcl2Package.THIRD_SECTION:
      {
        ThirdSection thirdSection = (ThirdSection)theEObject;
        T result = caseThirdSection(thirdSection);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MyOcl2Package.RETOUR1:
      {
        RETOUR1 retour1 = (RETOUR1)theEObject;
        T result = caseRETOUR1(retour1);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MyOcl2Package.FIRST_SECTION:
      {
        FirstSection firstSection = (FirstSection)theEObject;
        T result = caseFirstSection(firstSection);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MyOcl2Package.SECOND_SECTION:
      {
        SecondSection secondSection = (SecondSection)theEObject;
        T result = caseSecondSection(secondSection);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MyOcl2Package.RETOUR12:
      {
        RETOUR12 retour12 = (RETOUR12)theEObject;
        T result = caseRETOUR12(retour12);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MyOcl2Package.LAST_SECTION:
      {
        LastSection lastSection = (LastSection)theEObject;
        T result = caseLastSection(lastSection);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MyOcl2Package.FIN:
      {
        Fin fin = (Fin)theEObject;
        T result = caseFin(fin);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MyOcl2Package.MILIEU:
      {
        Milieu milieu = (Milieu)theEObject;
        T result = caseMilieu(milieu);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MyOcl2Package.DEBUT:
      {
        Debut debut = (Debut)theEObject;
        T result = caseDebut(debut);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MyOcl2Package.DECLARATION_SENTENCE:
      {
        declaration_sentence declaration_sentence = (declaration_sentence)theEObject;
        T result = casedeclaration_sentence(declaration_sentence);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      default: return defaultCase(theEObject);
    }
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Main</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Main</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMain(Main object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>package declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>package declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casepackage_declaration(package_declaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>endstartelement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>endstartelement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseendstartelement(endstartelement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>abstractelement</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>abstractelement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseabstractelement(abstractelement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>context declaration</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>context declaration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casecontext_declaration(context_declaration object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Middle Section</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Middle Section</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMiddleSection(MiddleSection object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Third Section</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Third Section</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseThirdSection(ThirdSection object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>RETOUR1</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>RETOUR1</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRETOUR1(RETOUR1 object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>First Section</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>First Section</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFirstSection(FirstSection object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Second Section</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Second Section</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSecondSection(SecondSection object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>RETOUR12</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>RETOUR12</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRETOUR12(RETOUR12 object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Last Section</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Last Section</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLastSection(LastSection object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Fin</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Fin</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseFin(Fin object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Milieu</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Milieu</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMilieu(Milieu object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Debut</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Debut</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDebut(Debut object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>declaration sentence</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>declaration sentence</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casedeclaration_sentence(declaration_sentence object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch, but this is the last case anyway.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject)
   * @generated
   */
  @Override
  public T defaultCase(EObject object)
  {
    return null;
  }

} //MyOcl2Switch
