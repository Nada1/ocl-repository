/**
 */
package org.xtext.example.mydsl.myOcl2;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>declaration sentence</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.declaration_sentence#getName <em>Name</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.declaration_sentence#getDeux <em>Deux</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.declaration_sentence#getTrois <em>Trois</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.example.mydsl.myOcl2.MyOcl2Package#getdeclaration_sentence()
 * @model
 * @generated
 */
public interface declaration_sentence extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see org.xtext.example.mydsl.myOcl2.MyOcl2Package#getdeclaration_sentence_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.myOcl2.declaration_sentence#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Deux</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Deux</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Deux</em>' attribute.
   * @see #setDeux(String)
   * @see org.xtext.example.mydsl.myOcl2.MyOcl2Package#getdeclaration_sentence_Deux()
   * @model
   * @generated
   */
  String getDeux();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.myOcl2.declaration_sentence#getDeux <em>Deux</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Deux</em>' attribute.
   * @see #getDeux()
   * @generated
   */
  void setDeux(String value);

  /**
   * Returns the value of the '<em><b>Trois</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Trois</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Trois</em>' attribute.
   * @see #setTrois(String)
   * @see org.xtext.example.mydsl.myOcl2.MyOcl2Package#getdeclaration_sentence_Trois()
   * @model
   * @generated
   */
  String getTrois();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.myOcl2.declaration_sentence#getTrois <em>Trois</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Trois</em>' attribute.
   * @see #getTrois()
   * @generated
   */
  void setTrois(String value);

} // declaration_sentence
