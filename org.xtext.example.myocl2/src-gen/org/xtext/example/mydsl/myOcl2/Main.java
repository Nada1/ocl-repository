/**
 */
package org.xtext.example.mydsl.myOcl2;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Main</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.Main#getImport_sentence <em>Import sentence</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myOcl2.Main#getPackage <em>Package</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.example.mydsl.myOcl2.MyOcl2Package#getMain()
 * @model
 * @generated
 */
public interface Main extends EObject
{
  /**
   * Returns the value of the '<em><b>Import sentence</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.example.mydsl.myOcl2.declaration_sentence}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Import sentence</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Import sentence</em>' containment reference list.
   * @see org.xtext.example.mydsl.myOcl2.MyOcl2Package#getMain_Import_sentence()
   * @model containment="true"
   * @generated
   */
  EList<declaration_sentence> getImport_sentence();

  /**
   * Returns the value of the '<em><b>Package</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Package</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Package</em>' containment reference.
   * @see #setPackage(package_declaration)
   * @see org.xtext.example.mydsl.myOcl2.MyOcl2Package#getMain_Package()
   * @model containment="true"
   * @generated
   */
  package_declaration getPackage();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.myOcl2.Main#getPackage <em>Package</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Package</em>' containment reference.
   * @see #getPackage()
   * @generated
   */
  void setPackage(package_declaration value);

} // Main
