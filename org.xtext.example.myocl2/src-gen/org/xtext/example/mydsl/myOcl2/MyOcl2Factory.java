/**
 */
package org.xtext.example.mydsl.myOcl2;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.xtext.example.mydsl.myOcl2.MyOcl2Package
 * @generated
 */
public interface MyOcl2Factory extends EFactory
{
  /**
   * The singleton instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  MyOcl2Factory eINSTANCE = org.xtext.example.mydsl.myOcl2.impl.MyOcl2FactoryImpl.init();

  /**
   * Returns a new object of class '<em>Main</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Main</em>'.
   * @generated
   */
  Main createMain();

  /**
   * Returns a new object of class '<em>package declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>package declaration</em>'.
   * @generated
   */
  package_declaration createpackage_declaration();

  /**
   * Returns a new object of class '<em>endstartelement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>endstartelement</em>'.
   * @generated
   */
  endstartelement createendstartelement();

  /**
   * Returns a new object of class '<em>abstractelement</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>abstractelement</em>'.
   * @generated
   */
  abstractelement createabstractelement();

  /**
   * Returns a new object of class '<em>context declaration</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>context declaration</em>'.
   * @generated
   */
  context_declaration createcontext_declaration();

  /**
   * Returns a new object of class '<em>Middle Section</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Middle Section</em>'.
   * @generated
   */
  MiddleSection createMiddleSection();

  /**
   * Returns a new object of class '<em>Third Section</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Third Section</em>'.
   * @generated
   */
  ThirdSection createThirdSection();

  /**
   * Returns a new object of class '<em>RETOUR1</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>RETOUR1</em>'.
   * @generated
   */
  RETOUR1 createRETOUR1();

  /**
   * Returns a new object of class '<em>First Section</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>First Section</em>'.
   * @generated
   */
  FirstSection createFirstSection();

  /**
   * Returns a new object of class '<em>Second Section</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Second Section</em>'.
   * @generated
   */
  SecondSection createSecondSection();

  /**
   * Returns a new object of class '<em>RETOUR12</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>RETOUR12</em>'.
   * @generated
   */
  RETOUR12 createRETOUR12();

  /**
   * Returns a new object of class '<em>Last Section</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Last Section</em>'.
   * @generated
   */
  LastSection createLastSection();

  /**
   * Returns a new object of class '<em>Fin</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Fin</em>'.
   * @generated
   */
  Fin createFin();

  /**
   * Returns a new object of class '<em>Milieu</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Milieu</em>'.
   * @generated
   */
  Milieu createMilieu();

  /**
   * Returns a new object of class '<em>Debut</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Debut</em>'.
   * @generated
   */
  Debut createDebut();

  /**
   * Returns a new object of class '<em>declaration sentence</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>declaration sentence</em>'.
   * @generated
   */
  declaration_sentence createdeclaration_sentence();

  /**
   * Returns the package supported by this factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the package supported by this factory.
   * @generated
   */
  MyOcl2Package getMyOcl2Package();

} //MyOcl2Factory
