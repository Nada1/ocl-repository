package org.xtext.example.mydsl.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import org.xtext.example.mydsl.services.MyOcl2GrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMyOcl2Parser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Double'", "'Integer'", "'Boolean'", "'STRING'", "'>'", "'<'", "'='", "'+'", "'.'", "':'", "'@pre'", "'.end'", "'('", "')'", "'and'", "'-'", "'@'", "'post'", "'pre'", "'inv'", "'deff'", "'package'", "'end'", "'context'", "'::'", "'import'", "'http://'", "'/'"
    };
    public static final int RULE_ID=4;
    public static final int T__29=29;
    public static final int T__28=28;
    public static final int T__27=27;
    public static final int T__26=26;
    public static final int T__25=25;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int RULE_SL_COMMENT=8;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__30=30;
    public static final int T__19=19;
    public static final int T__31=31;
    public static final int RULE_STRING=6;
    public static final int T__32=32;
    public static final int T__33=33;
    public static final int T__16=16;
    public static final int T__34=34;
    public static final int T__15=15;
    public static final int T__35=35;
    public static final int T__18=18;
    public static final int T__36=36;
    public static final int T__17=17;
    public static final int T__37=37;
    public static final int T__12=12;
    public static final int T__38=38;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=5;
    public static final int RULE_WS=9;

    // delegates
    // delegators


        public InternalMyOcl2Parser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMyOcl2Parser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMyOcl2Parser.tokenNames; }
    public String getGrammarFileName() { return "../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g"; }


     
     	private MyOcl2GrammarAccess grammarAccess;
     	
        public void setGrammarAccess(MyOcl2GrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleMain"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:60:1: entryRuleMain : ruleMain EOF ;
    public final void entryRuleMain() throws RecognitionException {
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:61:1: ( ruleMain EOF )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:62:1: ruleMain EOF
            {
             before(grammarAccess.getMainRule()); 
            pushFollow(FOLLOW_ruleMain_in_entryRuleMain61);
            ruleMain();

            state._fsp--;

             after(grammarAccess.getMainRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleMain68); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMain"


    // $ANTLR start "ruleMain"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:69:1: ruleMain : ( ( rule__Main__Group__0 ) ) ;
    public final void ruleMain() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:73:2: ( ( ( rule__Main__Group__0 ) ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:74:1: ( ( rule__Main__Group__0 ) )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:74:1: ( ( rule__Main__Group__0 ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:75:1: ( rule__Main__Group__0 )
            {
             before(grammarAccess.getMainAccess().getGroup()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:76:1: ( rule__Main__Group__0 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:76:2: rule__Main__Group__0
            {
            pushFollow(FOLLOW_rule__Main__Group__0_in_ruleMain94);
            rule__Main__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getMainAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMain"


    // $ANTLR start "entryRulepackage_declaration"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:88:1: entryRulepackage_declaration : rulepackage_declaration EOF ;
    public final void entryRulepackage_declaration() throws RecognitionException {
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:89:1: ( rulepackage_declaration EOF )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:90:1: rulepackage_declaration EOF
            {
             before(grammarAccess.getPackage_declarationRule()); 
            pushFollow(FOLLOW_rulepackage_declaration_in_entryRulepackage_declaration121);
            rulepackage_declaration();

            state._fsp--;

             after(grammarAccess.getPackage_declarationRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulepackage_declaration128); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulepackage_declaration"


    // $ANTLR start "rulepackage_declaration"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:97:1: rulepackage_declaration : ( ( rule__Package_declaration__Group__0 ) ) ;
    public final void rulepackage_declaration() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:101:2: ( ( ( rule__Package_declaration__Group__0 ) ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:102:1: ( ( rule__Package_declaration__Group__0 ) )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:102:1: ( ( rule__Package_declaration__Group__0 ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:103:1: ( rule__Package_declaration__Group__0 )
            {
             before(grammarAccess.getPackage_declarationAccess().getGroup()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:104:1: ( rule__Package_declaration__Group__0 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:104:2: rule__Package_declaration__Group__0
            {
            pushFollow(FOLLOW_rule__Package_declaration__Group__0_in_rulepackage_declaration154);
            rule__Package_declaration__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPackage_declarationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulepackage_declaration"


    // $ANTLR start "entryRuleendstartelement"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:116:1: entryRuleendstartelement : ruleendstartelement EOF ;
    public final void entryRuleendstartelement() throws RecognitionException {
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:117:1: ( ruleendstartelement EOF )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:118:1: ruleendstartelement EOF
            {
             before(grammarAccess.getEndstartelementRule()); 
            pushFollow(FOLLOW_ruleendstartelement_in_entryRuleendstartelement181);
            ruleendstartelement();

            state._fsp--;

             after(grammarAccess.getEndstartelementRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleendstartelement188); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleendstartelement"


    // $ANTLR start "ruleendstartelement"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:125:1: ruleendstartelement : ( ( rule__Endstartelement__NameAssignment ) ) ;
    public final void ruleendstartelement() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:129:2: ( ( ( rule__Endstartelement__NameAssignment ) ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:130:1: ( ( rule__Endstartelement__NameAssignment ) )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:130:1: ( ( rule__Endstartelement__NameAssignment ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:131:1: ( rule__Endstartelement__NameAssignment )
            {
             before(grammarAccess.getEndstartelementAccess().getNameAssignment()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:132:1: ( rule__Endstartelement__NameAssignment )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:132:2: rule__Endstartelement__NameAssignment
            {
            pushFollow(FOLLOW_rule__Endstartelement__NameAssignment_in_ruleendstartelement214);
            rule__Endstartelement__NameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getEndstartelementAccess().getNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleendstartelement"


    // $ANTLR start "entryRuleabstractelement"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:144:1: entryRuleabstractelement : ruleabstractelement EOF ;
    public final void entryRuleabstractelement() throws RecognitionException {
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:145:1: ( ruleabstractelement EOF )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:146:1: ruleabstractelement EOF
            {
             before(grammarAccess.getAbstractelementRule()); 
            pushFollow(FOLLOW_ruleabstractelement_in_entryRuleabstractelement241);
            ruleabstractelement();

            state._fsp--;

             after(grammarAccess.getAbstractelementRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleabstractelement248); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleabstractelement"


    // $ANTLR start "ruleabstractelement"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:153:1: ruleabstractelement : ( ( ( rule__Abstractelement__ElementsAssignment ) ) ( ( rule__Abstractelement__ElementsAssignment )* ) ) ;
    public final void ruleabstractelement() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:157:2: ( ( ( ( rule__Abstractelement__ElementsAssignment ) ) ( ( rule__Abstractelement__ElementsAssignment )* ) ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:158:1: ( ( ( rule__Abstractelement__ElementsAssignment ) ) ( ( rule__Abstractelement__ElementsAssignment )* ) )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:158:1: ( ( ( rule__Abstractelement__ElementsAssignment ) ) ( ( rule__Abstractelement__ElementsAssignment )* ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:159:1: ( ( rule__Abstractelement__ElementsAssignment ) ) ( ( rule__Abstractelement__ElementsAssignment )* )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:159:1: ( ( rule__Abstractelement__ElementsAssignment ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:160:1: ( rule__Abstractelement__ElementsAssignment )
            {
             before(grammarAccess.getAbstractelementAccess().getElementsAssignment()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:161:1: ( rule__Abstractelement__ElementsAssignment )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:161:2: rule__Abstractelement__ElementsAssignment
            {
            pushFollow(FOLLOW_rule__Abstractelement__ElementsAssignment_in_ruleabstractelement276);
            rule__Abstractelement__ElementsAssignment();

            state._fsp--;


            }

             after(grammarAccess.getAbstractelementAccess().getElementsAssignment()); 

            }

            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:164:1: ( ( rule__Abstractelement__ElementsAssignment )* )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:165:1: ( rule__Abstractelement__ElementsAssignment )*
            {
             before(grammarAccess.getAbstractelementAccess().getElementsAssignment()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:166:1: ( rule__Abstractelement__ElementsAssignment )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==34) ) {
                    int LA1_2 = input.LA(2);

                    if ( (LA1_2==RULE_ID) ) {
                        alt1=1;
                    }


                }


                switch (alt1) {
            	case 1 :
            	    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:166:2: rule__Abstractelement__ElementsAssignment
            	    {
            	    pushFollow(FOLLOW_rule__Abstractelement__ElementsAssignment_in_ruleabstractelement288);
            	    rule__Abstractelement__ElementsAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getAbstractelementAccess().getElementsAssignment()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleabstractelement"


    // $ANTLR start "entryRulecontext_declaration"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:179:1: entryRulecontext_declaration : rulecontext_declaration EOF ;
    public final void entryRulecontext_declaration() throws RecognitionException {
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:180:1: ( rulecontext_declaration EOF )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:181:1: rulecontext_declaration EOF
            {
             before(grammarAccess.getContext_declarationRule()); 
            pushFollow(FOLLOW_rulecontext_declaration_in_entryRulecontext_declaration318);
            rulecontext_declaration();

            state._fsp--;

             after(grammarAccess.getContext_declarationRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulecontext_declaration325); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulecontext_declaration"


    // $ANTLR start "rulecontext_declaration"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:188:1: rulecontext_declaration : ( ( rule__Context_declaration__Group__0 ) ) ;
    public final void rulecontext_declaration() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:192:2: ( ( ( rule__Context_declaration__Group__0 ) ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:193:1: ( ( rule__Context_declaration__Group__0 ) )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:193:1: ( ( rule__Context_declaration__Group__0 ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:194:1: ( rule__Context_declaration__Group__0 )
            {
             before(grammarAccess.getContext_declarationAccess().getGroup()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:195:1: ( rule__Context_declaration__Group__0 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:195:2: rule__Context_declaration__Group__0
            {
            pushFollow(FOLLOW_rule__Context_declaration__Group__0_in_rulecontext_declaration351);
            rule__Context_declaration__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getContext_declarationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulecontext_declaration"


    // $ANTLR start "entryRuleMiddleSection"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:207:1: entryRuleMiddleSection : ruleMiddleSection EOF ;
    public final void entryRuleMiddleSection() throws RecognitionException {
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:208:1: ( ruleMiddleSection EOF )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:209:1: ruleMiddleSection EOF
            {
             before(grammarAccess.getMiddleSectionRule()); 
            pushFollow(FOLLOW_ruleMiddleSection_in_entryRuleMiddleSection378);
            ruleMiddleSection();

            state._fsp--;

             after(grammarAccess.getMiddleSectionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleMiddleSection385); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMiddleSection"


    // $ANTLR start "ruleMiddleSection"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:216:1: ruleMiddleSection : ( ( rule__MiddleSection__Group__0 ) ) ;
    public final void ruleMiddleSection() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:220:2: ( ( ( rule__MiddleSection__Group__0 ) ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:221:1: ( ( rule__MiddleSection__Group__0 ) )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:221:1: ( ( rule__MiddleSection__Group__0 ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:222:1: ( rule__MiddleSection__Group__0 )
            {
             before(grammarAccess.getMiddleSectionAccess().getGroup()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:223:1: ( rule__MiddleSection__Group__0 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:223:2: rule__MiddleSection__Group__0
            {
            pushFollow(FOLLOW_rule__MiddleSection__Group__0_in_ruleMiddleSection411);
            rule__MiddleSection__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getMiddleSectionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMiddleSection"


    // $ANTLR start "entryRuleThirdSection"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:235:1: entryRuleThirdSection : ruleThirdSection EOF ;
    public final void entryRuleThirdSection() throws RecognitionException {
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:236:1: ( ruleThirdSection EOF )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:237:1: ruleThirdSection EOF
            {
             before(grammarAccess.getThirdSectionRule()); 
            pushFollow(FOLLOW_ruleThirdSection_in_entryRuleThirdSection438);
            ruleThirdSection();

            state._fsp--;

             after(grammarAccess.getThirdSectionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleThirdSection445); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleThirdSection"


    // $ANTLR start "ruleThirdSection"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:244:1: ruleThirdSection : ( ( rule__ThirdSection__Group__0 ) ) ;
    public final void ruleThirdSection() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:248:2: ( ( ( rule__ThirdSection__Group__0 ) ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:249:1: ( ( rule__ThirdSection__Group__0 ) )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:249:1: ( ( rule__ThirdSection__Group__0 ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:250:1: ( rule__ThirdSection__Group__0 )
            {
             before(grammarAccess.getThirdSectionAccess().getGroup()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:251:1: ( rule__ThirdSection__Group__0 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:251:2: rule__ThirdSection__Group__0
            {
            pushFollow(FOLLOW_rule__ThirdSection__Group__0_in_ruleThirdSection471);
            rule__ThirdSection__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getThirdSectionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleThirdSection"


    // $ANTLR start "entryRuleRETOUR1"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:263:1: entryRuleRETOUR1 : ruleRETOUR1 EOF ;
    public final void entryRuleRETOUR1() throws RecognitionException {
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:264:1: ( ruleRETOUR1 EOF )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:265:1: ruleRETOUR1 EOF
            {
             before(grammarAccess.getRETOUR1Rule()); 
            pushFollow(FOLLOW_ruleRETOUR1_in_entryRuleRETOUR1498);
            ruleRETOUR1();

            state._fsp--;

             after(grammarAccess.getRETOUR1Rule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRETOUR1505); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRETOUR1"


    // $ANTLR start "ruleRETOUR1"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:272:1: ruleRETOUR1 : ( ( rule__RETOUR1__Alternatives ) ) ;
    public final void ruleRETOUR1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:276:2: ( ( ( rule__RETOUR1__Alternatives ) ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:277:1: ( ( rule__RETOUR1__Alternatives ) )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:277:1: ( ( rule__RETOUR1__Alternatives ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:278:1: ( rule__RETOUR1__Alternatives )
            {
             before(grammarAccess.getRETOUR1Access().getAlternatives()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:279:1: ( rule__RETOUR1__Alternatives )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:279:2: rule__RETOUR1__Alternatives
            {
            pushFollow(FOLLOW_rule__RETOUR1__Alternatives_in_ruleRETOUR1531);
            rule__RETOUR1__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getRETOUR1Access().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRETOUR1"


    // $ANTLR start "entryRuleFirstSection"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:291:1: entryRuleFirstSection : ruleFirstSection EOF ;
    public final void entryRuleFirstSection() throws RecognitionException {
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:292:1: ( ruleFirstSection EOF )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:293:1: ruleFirstSection EOF
            {
             before(grammarAccess.getFirstSectionRule()); 
            pushFollow(FOLLOW_ruleFirstSection_in_entryRuleFirstSection558);
            ruleFirstSection();

            state._fsp--;

             after(grammarAccess.getFirstSectionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleFirstSection565); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFirstSection"


    // $ANTLR start "ruleFirstSection"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:300:1: ruleFirstSection : ( ( rule__FirstSection__NameAssignment ) ) ;
    public final void ruleFirstSection() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:304:2: ( ( ( rule__FirstSection__NameAssignment ) ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:305:1: ( ( rule__FirstSection__NameAssignment ) )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:305:1: ( ( rule__FirstSection__NameAssignment ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:306:1: ( rule__FirstSection__NameAssignment )
            {
             before(grammarAccess.getFirstSectionAccess().getNameAssignment()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:307:1: ( rule__FirstSection__NameAssignment )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:307:2: rule__FirstSection__NameAssignment
            {
            pushFollow(FOLLOW_rule__FirstSection__NameAssignment_in_ruleFirstSection591);
            rule__FirstSection__NameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getFirstSectionAccess().getNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFirstSection"


    // $ANTLR start "entryRuleSecondSection"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:319:1: entryRuleSecondSection : ruleSecondSection EOF ;
    public final void entryRuleSecondSection() throws RecognitionException {
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:320:1: ( ruleSecondSection EOF )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:321:1: ruleSecondSection EOF
            {
             before(grammarAccess.getSecondSectionRule()); 
            pushFollow(FOLLOW_ruleSecondSection_in_entryRuleSecondSection618);
            ruleSecondSection();

            state._fsp--;

             after(grammarAccess.getSecondSectionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSecondSection625); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSecondSection"


    // $ANTLR start "ruleSecondSection"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:328:1: ruleSecondSection : ( ( rule__SecondSection__Group__0 ) ) ;
    public final void ruleSecondSection() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:332:2: ( ( ( rule__SecondSection__Group__0 ) ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:333:1: ( ( rule__SecondSection__Group__0 ) )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:333:1: ( ( rule__SecondSection__Group__0 ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:334:1: ( rule__SecondSection__Group__0 )
            {
             before(grammarAccess.getSecondSectionAccess().getGroup()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:335:1: ( rule__SecondSection__Group__0 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:335:2: rule__SecondSection__Group__0
            {
            pushFollow(FOLLOW_rule__SecondSection__Group__0_in_ruleSecondSection651);
            rule__SecondSection__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSecondSectionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSecondSection"


    // $ANTLR start "entryRuleRETOUR12"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:347:1: entryRuleRETOUR12 : ruleRETOUR12 EOF ;
    public final void entryRuleRETOUR12() throws RecognitionException {
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:348:1: ( ruleRETOUR12 EOF )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:349:1: ruleRETOUR12 EOF
            {
             before(grammarAccess.getRETOUR12Rule()); 
            pushFollow(FOLLOW_ruleRETOUR12_in_entryRuleRETOUR12678);
            ruleRETOUR12();

            state._fsp--;

             after(grammarAccess.getRETOUR12Rule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRETOUR12685); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRETOUR12"


    // $ANTLR start "ruleRETOUR12"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:356:1: ruleRETOUR12 : ( ( rule__RETOUR12__NameAssignment ) ) ;
    public final void ruleRETOUR12() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:360:2: ( ( ( rule__RETOUR12__NameAssignment ) ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:361:1: ( ( rule__RETOUR12__NameAssignment ) )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:361:1: ( ( rule__RETOUR12__NameAssignment ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:362:1: ( rule__RETOUR12__NameAssignment )
            {
             before(grammarAccess.getRETOUR12Access().getNameAssignment()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:363:1: ( rule__RETOUR12__NameAssignment )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:363:2: rule__RETOUR12__NameAssignment
            {
            pushFollow(FOLLOW_rule__RETOUR12__NameAssignment_in_ruleRETOUR12711);
            rule__RETOUR12__NameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getRETOUR12Access().getNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRETOUR12"


    // $ANTLR start "entryRuleLastSection"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:375:1: entryRuleLastSection : ruleLastSection EOF ;
    public final void entryRuleLastSection() throws RecognitionException {
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:376:1: ( ruleLastSection EOF )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:377:1: ruleLastSection EOF
            {
             before(grammarAccess.getLastSectionRule()); 
            pushFollow(FOLLOW_ruleLastSection_in_entryRuleLastSection738);
            ruleLastSection();

            state._fsp--;

             after(grammarAccess.getLastSectionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleLastSection745); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLastSection"


    // $ANTLR start "ruleLastSection"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:384:1: ruleLastSection : ( ( rule__LastSection__Group__0 ) ) ;
    public final void ruleLastSection() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:388:2: ( ( ( rule__LastSection__Group__0 ) ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:389:1: ( ( rule__LastSection__Group__0 ) )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:389:1: ( ( rule__LastSection__Group__0 ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:390:1: ( rule__LastSection__Group__0 )
            {
             before(grammarAccess.getLastSectionAccess().getGroup()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:391:1: ( rule__LastSection__Group__0 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:391:2: rule__LastSection__Group__0
            {
            pushFollow(FOLLOW_rule__LastSection__Group__0_in_ruleLastSection771);
            rule__LastSection__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLastSectionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLastSection"


    // $ANTLR start "entryRuleFin"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:403:1: entryRuleFin : ruleFin EOF ;
    public final void entryRuleFin() throws RecognitionException {
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:404:1: ( ruleFin EOF )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:405:1: ruleFin EOF
            {
             before(grammarAccess.getFinRule()); 
            pushFollow(FOLLOW_ruleFin_in_entryRuleFin798);
            ruleFin();

            state._fsp--;

             after(grammarAccess.getFinRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleFin805); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFin"


    // $ANTLR start "ruleFin"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:412:1: ruleFin : ( ( rule__Fin__NameAssignment )* ) ;
    public final void ruleFin() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:416:2: ( ( ( rule__Fin__NameAssignment )* ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:417:1: ( ( rule__Fin__NameAssignment )* )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:417:1: ( ( rule__Fin__NameAssignment )* )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:418:1: ( rule__Fin__NameAssignment )*
            {
             before(grammarAccess.getFinAccess().getNameAssignment()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:419:1: ( rule__Fin__NameAssignment )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==RULE_ID) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:419:2: rule__Fin__NameAssignment
            	    {
            	    pushFollow(FOLLOW_rule__Fin__NameAssignment_in_ruleFin831);
            	    rule__Fin__NameAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

             after(grammarAccess.getFinAccess().getNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFin"


    // $ANTLR start "entryRuleMilieu"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:431:1: entryRuleMilieu : ruleMilieu EOF ;
    public final void entryRuleMilieu() throws RecognitionException {
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:432:1: ( ruleMilieu EOF )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:433:1: ruleMilieu EOF
            {
             before(grammarAccess.getMilieuRule()); 
            pushFollow(FOLLOW_ruleMilieu_in_entryRuleMilieu859);
            ruleMilieu();

            state._fsp--;

             after(grammarAccess.getMilieuRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleMilieu866); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMilieu"


    // $ANTLR start "ruleMilieu"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:440:1: ruleMilieu : ( ( rule__Milieu__NameAssignment )? ) ;
    public final void ruleMilieu() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:444:2: ( ( ( rule__Milieu__NameAssignment )? ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:445:1: ( ( rule__Milieu__NameAssignment )? )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:445:1: ( ( rule__Milieu__NameAssignment )? )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:446:1: ( rule__Milieu__NameAssignment )?
            {
             before(grammarAccess.getMilieuAccess().getNameAssignment()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:447:1: ( rule__Milieu__NameAssignment )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==RULE_INT) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:447:2: rule__Milieu__NameAssignment
                    {
                    pushFollow(FOLLOW_rule__Milieu__NameAssignment_in_ruleMilieu892);
                    rule__Milieu__NameAssignment();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getMilieuAccess().getNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMilieu"


    // $ANTLR start "entryRuleRETOUR4"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:459:1: entryRuleRETOUR4 : ruleRETOUR4 EOF ;
    public final void entryRuleRETOUR4() throws RecognitionException {
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:460:1: ( ruleRETOUR4 EOF )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:461:1: ruleRETOUR4 EOF
            {
             before(grammarAccess.getRETOUR4Rule()); 
            pushFollow(FOLLOW_ruleRETOUR4_in_entryRuleRETOUR4920);
            ruleRETOUR4();

            state._fsp--;

             after(grammarAccess.getRETOUR4Rule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRETOUR4927); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRETOUR4"


    // $ANTLR start "ruleRETOUR4"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:468:1: ruleRETOUR4 : ( ( rule__RETOUR4__Alternatives ) ) ;
    public final void ruleRETOUR4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:472:2: ( ( ( rule__RETOUR4__Alternatives ) ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:473:1: ( ( rule__RETOUR4__Alternatives ) )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:473:1: ( ( rule__RETOUR4__Alternatives ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:474:1: ( rule__RETOUR4__Alternatives )
            {
             before(grammarAccess.getRETOUR4Access().getAlternatives()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:475:1: ( rule__RETOUR4__Alternatives )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:475:2: rule__RETOUR4__Alternatives
            {
            pushFollow(FOLLOW_rule__RETOUR4__Alternatives_in_ruleRETOUR4953);
            rule__RETOUR4__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getRETOUR4Access().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRETOUR4"


    // $ANTLR start "entryRuleRETOUR"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:487:1: entryRuleRETOUR : ruleRETOUR EOF ;
    public final void entryRuleRETOUR() throws RecognitionException {
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:488:1: ( ruleRETOUR EOF )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:489:1: ruleRETOUR EOF
            {
             before(grammarAccess.getRETOURRule()); 
            pushFollow(FOLLOW_ruleRETOUR_in_entryRuleRETOUR980);
            ruleRETOUR();

            state._fsp--;

             after(grammarAccess.getRETOURRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRETOUR987); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRETOUR"


    // $ANTLR start "ruleRETOUR"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:496:1: ruleRETOUR : ( ( rule__RETOUR__Alternatives ) ) ;
    public final void ruleRETOUR() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:500:2: ( ( ( rule__RETOUR__Alternatives ) ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:501:1: ( ( rule__RETOUR__Alternatives ) )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:501:1: ( ( rule__RETOUR__Alternatives ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:502:1: ( rule__RETOUR__Alternatives )
            {
             before(grammarAccess.getRETOURAccess().getAlternatives()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:503:1: ( rule__RETOUR__Alternatives )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:503:2: rule__RETOUR__Alternatives
            {
            pushFollow(FOLLOW_rule__RETOUR__Alternatives_in_ruleRETOUR1013);
            rule__RETOUR__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getRETOURAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRETOUR"


    // $ANTLR start "entryRuleDebut"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:515:1: entryRuleDebut : ruleDebut EOF ;
    public final void entryRuleDebut() throws RecognitionException {
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:516:1: ( ruleDebut EOF )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:517:1: ruleDebut EOF
            {
             before(grammarAccess.getDebutRule()); 
            pushFollow(FOLLOW_ruleDebut_in_entryRuleDebut1040);
            ruleDebut();

            state._fsp--;

             after(grammarAccess.getDebutRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDebut1047); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDebut"


    // $ANTLR start "ruleDebut"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:524:1: ruleDebut : ( ( rule__Debut__NameAssignment )* ) ;
    public final void ruleDebut() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:528:2: ( ( ( rule__Debut__NameAssignment )* ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:529:1: ( ( rule__Debut__NameAssignment )* )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:529:1: ( ( rule__Debut__NameAssignment )* )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:530:1: ( rule__Debut__NameAssignment )*
            {
             before(grammarAccess.getDebutAccess().getNameAssignment()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:531:1: ( rule__Debut__NameAssignment )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==RULE_ID) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:531:2: rule__Debut__NameAssignment
            	    {
            	    pushFollow(FOLLOW_rule__Debut__NameAssignment_in_ruleDebut1073);
            	    rule__Debut__NameAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

             after(grammarAccess.getDebutAccess().getNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDebut"


    // $ANTLR start "entryRuledeclaration_sentence"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:543:1: entryRuledeclaration_sentence : ruledeclaration_sentence EOF ;
    public final void entryRuledeclaration_sentence() throws RecognitionException {
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:544:1: ( ruledeclaration_sentence EOF )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:545:1: ruledeclaration_sentence EOF
            {
             before(grammarAccess.getDeclaration_sentenceRule()); 
            pushFollow(FOLLOW_ruledeclaration_sentence_in_entryRuledeclaration_sentence1101);
            ruledeclaration_sentence();

            state._fsp--;

             after(grammarAccess.getDeclaration_sentenceRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuledeclaration_sentence1108); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuledeclaration_sentence"


    // $ANTLR start "ruledeclaration_sentence"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:552:1: ruledeclaration_sentence : ( ( rule__Declaration_sentence__Group__0 ) ) ;
    public final void ruledeclaration_sentence() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:556:2: ( ( ( rule__Declaration_sentence__Group__0 ) ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:557:1: ( ( rule__Declaration_sentence__Group__0 ) )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:557:1: ( ( rule__Declaration_sentence__Group__0 ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:558:1: ( rule__Declaration_sentence__Group__0 )
            {
             before(grammarAccess.getDeclaration_sentenceAccess().getGroup()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:559:1: ( rule__Declaration_sentence__Group__0 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:559:2: rule__Declaration_sentence__Group__0
            {
            pushFollow(FOLLOW_rule__Declaration_sentence__Group__0_in_ruledeclaration_sentence1134);
            rule__Declaration_sentence__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDeclaration_sentenceAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruledeclaration_sentence"


    // $ANTLR start "entryRuleQualified1"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:571:1: entryRuleQualified1 : ruleQualified1 EOF ;
    public final void entryRuleQualified1() throws RecognitionException {
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:572:1: ( ruleQualified1 EOF )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:573:1: ruleQualified1 EOF
            {
             before(grammarAccess.getQualified1Rule()); 
            pushFollow(FOLLOW_ruleQualified1_in_entryRuleQualified11161);
            ruleQualified1();

            state._fsp--;

             after(grammarAccess.getQualified1Rule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQualified11168); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQualified1"


    // $ANTLR start "ruleQualified1"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:580:1: ruleQualified1 : ( ( rule__Qualified1__Group__0 ) ) ;
    public final void ruleQualified1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:584:2: ( ( ( rule__Qualified1__Group__0 ) ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:585:1: ( ( rule__Qualified1__Group__0 ) )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:585:1: ( ( rule__Qualified1__Group__0 ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:586:1: ( rule__Qualified1__Group__0 )
            {
             before(grammarAccess.getQualified1Access().getGroup()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:587:1: ( rule__Qualified1__Group__0 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:587:2: rule__Qualified1__Group__0
            {
            pushFollow(FOLLOW_rule__Qualified1__Group__0_in_ruleQualified11194);
            rule__Qualified1__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQualified1Access().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQualified1"


    // $ANTLR start "entryRuleQualified"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:599:1: entryRuleQualified : ruleQualified EOF ;
    public final void entryRuleQualified() throws RecognitionException {
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:600:1: ( ruleQualified EOF )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:601:1: ruleQualified EOF
            {
             before(grammarAccess.getQualifiedRule()); 
            pushFollow(FOLLOW_ruleQualified_in_entryRuleQualified1221);
            ruleQualified();

            state._fsp--;

             after(grammarAccess.getQualifiedRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQualified1228); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQualified"


    // $ANTLR start "ruleQualified"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:608:1: ruleQualified : ( ( rule__Qualified__Group__0 ) ) ;
    public final void ruleQualified() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:612:2: ( ( ( rule__Qualified__Group__0 ) ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:613:1: ( ( rule__Qualified__Group__0 ) )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:613:1: ( ( rule__Qualified__Group__0 ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:614:1: ( rule__Qualified__Group__0 )
            {
             before(grammarAccess.getQualifiedAccess().getGroup()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:615:1: ( rule__Qualified__Group__0 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:615:2: rule__Qualified__Group__0
            {
            pushFollow(FOLLOW_rule__Qualified__Group__0_in_ruleQualified1254);
            rule__Qualified__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQualifiedAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQualified"


    // $ANTLR start "rule__RETOUR1__Alternatives"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:627:1: rule__RETOUR1__Alternatives : ( ( 'Double' ) | ( 'Integer' ) | ( 'Boolean' ) | ( 'STRING' ) | ( ( rule__RETOUR1__NameAssignment_4 ) ) );
    public final void rule__RETOUR1__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:631:1: ( ( 'Double' ) | ( 'Integer' ) | ( 'Boolean' ) | ( 'STRING' ) | ( ( rule__RETOUR1__NameAssignment_4 ) ) )
            int alt5=5;
            switch ( input.LA(1) ) {
            case 11:
                {
                alt5=1;
                }
                break;
            case 12:
                {
                alt5=2;
                }
                break;
            case 13:
                {
                alt5=3;
                }
                break;
            case 14:
                {
                alt5=4;
                }
                break;
            case RULE_ID:
                {
                alt5=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:632:1: ( 'Double' )
                    {
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:632:1: ( 'Double' )
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:633:1: 'Double'
                    {
                     before(grammarAccess.getRETOUR1Access().getDoubleKeyword_0()); 
                    match(input,11,FOLLOW_11_in_rule__RETOUR1__Alternatives1291); 
                     after(grammarAccess.getRETOUR1Access().getDoubleKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:640:6: ( 'Integer' )
                    {
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:640:6: ( 'Integer' )
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:641:1: 'Integer'
                    {
                     before(grammarAccess.getRETOUR1Access().getIntegerKeyword_1()); 
                    match(input,12,FOLLOW_12_in_rule__RETOUR1__Alternatives1311); 
                     after(grammarAccess.getRETOUR1Access().getIntegerKeyword_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:648:6: ( 'Boolean' )
                    {
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:648:6: ( 'Boolean' )
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:649:1: 'Boolean'
                    {
                     before(grammarAccess.getRETOUR1Access().getBooleanKeyword_2()); 
                    match(input,13,FOLLOW_13_in_rule__RETOUR1__Alternatives1331); 
                     after(grammarAccess.getRETOUR1Access().getBooleanKeyword_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:656:6: ( 'STRING' )
                    {
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:656:6: ( 'STRING' )
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:657:1: 'STRING'
                    {
                     before(grammarAccess.getRETOUR1Access().getSTRINGKeyword_3()); 
                    match(input,14,FOLLOW_14_in_rule__RETOUR1__Alternatives1351); 
                     after(grammarAccess.getRETOUR1Access().getSTRINGKeyword_3()); 

                    }


                    }
                    break;
                case 5 :
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:664:6: ( ( rule__RETOUR1__NameAssignment_4 ) )
                    {
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:664:6: ( ( rule__RETOUR1__NameAssignment_4 ) )
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:665:1: ( rule__RETOUR1__NameAssignment_4 )
                    {
                     before(grammarAccess.getRETOUR1Access().getNameAssignment_4()); 
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:666:1: ( rule__RETOUR1__NameAssignment_4 )
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:666:2: rule__RETOUR1__NameAssignment_4
                    {
                    pushFollow(FOLLOW_rule__RETOUR1__NameAssignment_4_in_rule__RETOUR1__Alternatives1370);
                    rule__RETOUR1__NameAssignment_4();

                    state._fsp--;


                    }

                     after(grammarAccess.getRETOUR1Access().getNameAssignment_4()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RETOUR1__Alternatives"


    // $ANTLR start "rule__RETOUR4__Alternatives"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:675:1: rule__RETOUR4__Alternatives : ( ( '>' ) | ( '<' ) | ( '=' ) | ( '+' ) | ( '.' ) | ( ':' ) | ( '@pre' ) | ( '.end' ) | ( '(' ) | ( ')' ) | ( 'and' ) | ( '-' ) | ( '@' ) );
    public final void rule__RETOUR4__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:679:1: ( ( '>' ) | ( '<' ) | ( '=' ) | ( '+' ) | ( '.' ) | ( ':' ) | ( '@pre' ) | ( '.end' ) | ( '(' ) | ( ')' ) | ( 'and' ) | ( '-' ) | ( '@' ) )
            int alt6=13;
            switch ( input.LA(1) ) {
            case 15:
                {
                alt6=1;
                }
                break;
            case 16:
                {
                alt6=2;
                }
                break;
            case 17:
                {
                alt6=3;
                }
                break;
            case 18:
                {
                alt6=4;
                }
                break;
            case 19:
                {
                alt6=5;
                }
                break;
            case 20:
                {
                alt6=6;
                }
                break;
            case 21:
                {
                alt6=7;
                }
                break;
            case 22:
                {
                alt6=8;
                }
                break;
            case 23:
                {
                alt6=9;
                }
                break;
            case 24:
                {
                alt6=10;
                }
                break;
            case 25:
                {
                alt6=11;
                }
                break;
            case 26:
                {
                alt6=12;
                }
                break;
            case 27:
                {
                alt6=13;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:680:1: ( '>' )
                    {
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:680:1: ( '>' )
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:681:1: '>'
                    {
                     before(grammarAccess.getRETOUR4Access().getGreaterThanSignKeyword_0()); 
                    match(input,15,FOLLOW_15_in_rule__RETOUR4__Alternatives1404); 
                     after(grammarAccess.getRETOUR4Access().getGreaterThanSignKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:688:6: ( '<' )
                    {
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:688:6: ( '<' )
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:689:1: '<'
                    {
                     before(grammarAccess.getRETOUR4Access().getLessThanSignKeyword_1()); 
                    match(input,16,FOLLOW_16_in_rule__RETOUR4__Alternatives1424); 
                     after(grammarAccess.getRETOUR4Access().getLessThanSignKeyword_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:696:6: ( '=' )
                    {
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:696:6: ( '=' )
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:697:1: '='
                    {
                     before(grammarAccess.getRETOUR4Access().getEqualsSignKeyword_2()); 
                    match(input,17,FOLLOW_17_in_rule__RETOUR4__Alternatives1444); 
                     after(grammarAccess.getRETOUR4Access().getEqualsSignKeyword_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:704:6: ( '+' )
                    {
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:704:6: ( '+' )
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:705:1: '+'
                    {
                     before(grammarAccess.getRETOUR4Access().getPlusSignKeyword_3()); 
                    match(input,18,FOLLOW_18_in_rule__RETOUR4__Alternatives1464); 
                     after(grammarAccess.getRETOUR4Access().getPlusSignKeyword_3()); 

                    }


                    }
                    break;
                case 5 :
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:712:6: ( '.' )
                    {
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:712:6: ( '.' )
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:713:1: '.'
                    {
                     before(grammarAccess.getRETOUR4Access().getFullStopKeyword_4()); 
                    match(input,19,FOLLOW_19_in_rule__RETOUR4__Alternatives1484); 
                     after(grammarAccess.getRETOUR4Access().getFullStopKeyword_4()); 

                    }


                    }
                    break;
                case 6 :
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:720:6: ( ':' )
                    {
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:720:6: ( ':' )
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:721:1: ':'
                    {
                     before(grammarAccess.getRETOUR4Access().getColonKeyword_5()); 
                    match(input,20,FOLLOW_20_in_rule__RETOUR4__Alternatives1504); 
                     after(grammarAccess.getRETOUR4Access().getColonKeyword_5()); 

                    }


                    }
                    break;
                case 7 :
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:728:6: ( '@pre' )
                    {
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:728:6: ( '@pre' )
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:729:1: '@pre'
                    {
                     before(grammarAccess.getRETOUR4Access().getPreKeyword_6()); 
                    match(input,21,FOLLOW_21_in_rule__RETOUR4__Alternatives1524); 
                     after(grammarAccess.getRETOUR4Access().getPreKeyword_6()); 

                    }


                    }
                    break;
                case 8 :
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:736:6: ( '.end' )
                    {
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:736:6: ( '.end' )
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:737:1: '.end'
                    {
                     before(grammarAccess.getRETOUR4Access().getEndKeyword_7()); 
                    match(input,22,FOLLOW_22_in_rule__RETOUR4__Alternatives1544); 
                     after(grammarAccess.getRETOUR4Access().getEndKeyword_7()); 

                    }


                    }
                    break;
                case 9 :
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:744:6: ( '(' )
                    {
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:744:6: ( '(' )
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:745:1: '('
                    {
                     before(grammarAccess.getRETOUR4Access().getLeftParenthesisKeyword_8()); 
                    match(input,23,FOLLOW_23_in_rule__RETOUR4__Alternatives1564); 
                     after(grammarAccess.getRETOUR4Access().getLeftParenthesisKeyword_8()); 

                    }


                    }
                    break;
                case 10 :
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:752:6: ( ')' )
                    {
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:752:6: ( ')' )
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:753:1: ')'
                    {
                     before(grammarAccess.getRETOUR4Access().getRightParenthesisKeyword_9()); 
                    match(input,24,FOLLOW_24_in_rule__RETOUR4__Alternatives1584); 
                     after(grammarAccess.getRETOUR4Access().getRightParenthesisKeyword_9()); 

                    }


                    }
                    break;
                case 11 :
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:760:6: ( 'and' )
                    {
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:760:6: ( 'and' )
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:761:1: 'and'
                    {
                     before(grammarAccess.getRETOUR4Access().getAndKeyword_10()); 
                    match(input,25,FOLLOW_25_in_rule__RETOUR4__Alternatives1604); 
                     after(grammarAccess.getRETOUR4Access().getAndKeyword_10()); 

                    }


                    }
                    break;
                case 12 :
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:768:6: ( '-' )
                    {
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:768:6: ( '-' )
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:769:1: '-'
                    {
                     before(grammarAccess.getRETOUR4Access().getHyphenMinusKeyword_11()); 
                    match(input,26,FOLLOW_26_in_rule__RETOUR4__Alternatives1624); 
                     after(grammarAccess.getRETOUR4Access().getHyphenMinusKeyword_11()); 

                    }


                    }
                    break;
                case 13 :
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:776:6: ( '@' )
                    {
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:776:6: ( '@' )
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:777:1: '@'
                    {
                     before(grammarAccess.getRETOUR4Access().getCommercialAtKeyword_12()); 
                    match(input,27,FOLLOW_27_in_rule__RETOUR4__Alternatives1644); 
                     after(grammarAccess.getRETOUR4Access().getCommercialAtKeyword_12()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RETOUR4__Alternatives"


    // $ANTLR start "rule__RETOUR__Alternatives"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:789:1: rule__RETOUR__Alternatives : ( ( 'post' ) | ( 'pre' ) | ( 'inv' ) | ( 'deff' ) );
    public final void rule__RETOUR__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:793:1: ( ( 'post' ) | ( 'pre' ) | ( 'inv' ) | ( 'deff' ) )
            int alt7=4;
            switch ( input.LA(1) ) {
            case 28:
                {
                alt7=1;
                }
                break;
            case 29:
                {
                alt7=2;
                }
                break;
            case 30:
                {
                alt7=3;
                }
                break;
            case 31:
                {
                alt7=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }

            switch (alt7) {
                case 1 :
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:794:1: ( 'post' )
                    {
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:794:1: ( 'post' )
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:795:1: 'post'
                    {
                     before(grammarAccess.getRETOURAccess().getPostKeyword_0()); 
                    match(input,28,FOLLOW_28_in_rule__RETOUR__Alternatives1679); 
                     after(grammarAccess.getRETOURAccess().getPostKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:802:6: ( 'pre' )
                    {
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:802:6: ( 'pre' )
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:803:1: 'pre'
                    {
                     before(grammarAccess.getRETOURAccess().getPreKeyword_1()); 
                    match(input,29,FOLLOW_29_in_rule__RETOUR__Alternatives1699); 
                     after(grammarAccess.getRETOURAccess().getPreKeyword_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:810:6: ( 'inv' )
                    {
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:810:6: ( 'inv' )
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:811:1: 'inv'
                    {
                     before(grammarAccess.getRETOURAccess().getInvKeyword_2()); 
                    match(input,30,FOLLOW_30_in_rule__RETOUR__Alternatives1719); 
                     after(grammarAccess.getRETOURAccess().getInvKeyword_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:818:6: ( 'deff' )
                    {
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:818:6: ( 'deff' )
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:819:1: 'deff'
                    {
                     before(grammarAccess.getRETOURAccess().getDeffKeyword_3()); 
                    match(input,31,FOLLOW_31_in_rule__RETOUR__Alternatives1739); 
                     after(grammarAccess.getRETOURAccess().getDeffKeyword_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RETOUR__Alternatives"


    // $ANTLR start "rule__Main__Group__0"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:833:1: rule__Main__Group__0 : rule__Main__Group__0__Impl rule__Main__Group__1 ;
    public final void rule__Main__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:837:1: ( rule__Main__Group__0__Impl rule__Main__Group__1 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:838:2: rule__Main__Group__0__Impl rule__Main__Group__1
            {
            pushFollow(FOLLOW_rule__Main__Group__0__Impl_in_rule__Main__Group__01771);
            rule__Main__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Main__Group__1_in_rule__Main__Group__01774);
            rule__Main__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Main__Group__0"


    // $ANTLR start "rule__Main__Group__0__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:845:1: rule__Main__Group__0__Impl : ( ( rule__Main__Import_sentenceAssignment_0 )? ) ;
    public final void rule__Main__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:849:1: ( ( ( rule__Main__Import_sentenceAssignment_0 )? ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:850:1: ( ( rule__Main__Import_sentenceAssignment_0 )? )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:850:1: ( ( rule__Main__Import_sentenceAssignment_0 )? )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:851:1: ( rule__Main__Import_sentenceAssignment_0 )?
            {
             before(grammarAccess.getMainAccess().getImport_sentenceAssignment_0()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:852:1: ( rule__Main__Import_sentenceAssignment_0 )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==36) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:852:2: rule__Main__Import_sentenceAssignment_0
                    {
                    pushFollow(FOLLOW_rule__Main__Import_sentenceAssignment_0_in_rule__Main__Group__0__Impl1801);
                    rule__Main__Import_sentenceAssignment_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getMainAccess().getImport_sentenceAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Main__Group__0__Impl"


    // $ANTLR start "rule__Main__Group__1"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:862:1: rule__Main__Group__1 : rule__Main__Group__1__Impl ;
    public final void rule__Main__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:866:1: ( rule__Main__Group__1__Impl )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:867:2: rule__Main__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Main__Group__1__Impl_in_rule__Main__Group__11832);
            rule__Main__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Main__Group__1"


    // $ANTLR start "rule__Main__Group__1__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:873:1: rule__Main__Group__1__Impl : ( ( rule__Main__PackageAssignment_1 ) ) ;
    public final void rule__Main__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:877:1: ( ( ( rule__Main__PackageAssignment_1 ) ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:878:1: ( ( rule__Main__PackageAssignment_1 ) )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:878:1: ( ( rule__Main__PackageAssignment_1 ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:879:1: ( rule__Main__PackageAssignment_1 )
            {
             before(grammarAccess.getMainAccess().getPackageAssignment_1()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:880:1: ( rule__Main__PackageAssignment_1 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:880:2: rule__Main__PackageAssignment_1
            {
            pushFollow(FOLLOW_rule__Main__PackageAssignment_1_in_rule__Main__Group__1__Impl1859);
            rule__Main__PackageAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getMainAccess().getPackageAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Main__Group__1__Impl"


    // $ANTLR start "rule__Package_declaration__Group__0"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:894:1: rule__Package_declaration__Group__0 : rule__Package_declaration__Group__0__Impl rule__Package_declaration__Group__1 ;
    public final void rule__Package_declaration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:898:1: ( rule__Package_declaration__Group__0__Impl rule__Package_declaration__Group__1 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:899:2: rule__Package_declaration__Group__0__Impl rule__Package_declaration__Group__1
            {
            pushFollow(FOLLOW_rule__Package_declaration__Group__0__Impl_in_rule__Package_declaration__Group__01893);
            rule__Package_declaration__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Package_declaration__Group__1_in_rule__Package_declaration__Group__01896);
            rule__Package_declaration__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Package_declaration__Group__0"


    // $ANTLR start "rule__Package_declaration__Group__0__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:906:1: rule__Package_declaration__Group__0__Impl : ( 'package' ) ;
    public final void rule__Package_declaration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:910:1: ( ( 'package' ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:911:1: ( 'package' )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:911:1: ( 'package' )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:912:1: 'package'
            {
             before(grammarAccess.getPackage_declarationAccess().getPackageKeyword_0()); 
            match(input,32,FOLLOW_32_in_rule__Package_declaration__Group__0__Impl1924); 
             after(grammarAccess.getPackage_declarationAccess().getPackageKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Package_declaration__Group__0__Impl"


    // $ANTLR start "rule__Package_declaration__Group__1"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:925:1: rule__Package_declaration__Group__1 : rule__Package_declaration__Group__1__Impl rule__Package_declaration__Group__2 ;
    public final void rule__Package_declaration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:929:1: ( rule__Package_declaration__Group__1__Impl rule__Package_declaration__Group__2 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:930:2: rule__Package_declaration__Group__1__Impl rule__Package_declaration__Group__2
            {
            pushFollow(FOLLOW_rule__Package_declaration__Group__1__Impl_in_rule__Package_declaration__Group__11955);
            rule__Package_declaration__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Package_declaration__Group__2_in_rule__Package_declaration__Group__11958);
            rule__Package_declaration__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Package_declaration__Group__1"


    // $ANTLR start "rule__Package_declaration__Group__1__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:937:1: rule__Package_declaration__Group__1__Impl : ( ( rule__Package_declaration__SharedelementAssignment_1 ) ) ;
    public final void rule__Package_declaration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:941:1: ( ( ( rule__Package_declaration__SharedelementAssignment_1 ) ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:942:1: ( ( rule__Package_declaration__SharedelementAssignment_1 ) )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:942:1: ( ( rule__Package_declaration__SharedelementAssignment_1 ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:943:1: ( rule__Package_declaration__SharedelementAssignment_1 )
            {
             before(grammarAccess.getPackage_declarationAccess().getSharedelementAssignment_1()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:944:1: ( rule__Package_declaration__SharedelementAssignment_1 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:944:2: rule__Package_declaration__SharedelementAssignment_1
            {
            pushFollow(FOLLOW_rule__Package_declaration__SharedelementAssignment_1_in_rule__Package_declaration__Group__1__Impl1985);
            rule__Package_declaration__SharedelementAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getPackage_declarationAccess().getSharedelementAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Package_declaration__Group__1__Impl"


    // $ANTLR start "rule__Package_declaration__Group__2"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:954:1: rule__Package_declaration__Group__2 : rule__Package_declaration__Group__2__Impl rule__Package_declaration__Group__3 ;
    public final void rule__Package_declaration__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:958:1: ( rule__Package_declaration__Group__2__Impl rule__Package_declaration__Group__3 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:959:2: rule__Package_declaration__Group__2__Impl rule__Package_declaration__Group__3
            {
            pushFollow(FOLLOW_rule__Package_declaration__Group__2__Impl_in_rule__Package_declaration__Group__22015);
            rule__Package_declaration__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Package_declaration__Group__3_in_rule__Package_declaration__Group__22018);
            rule__Package_declaration__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Package_declaration__Group__2"


    // $ANTLR start "rule__Package_declaration__Group__2__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:966:1: rule__Package_declaration__Group__2__Impl : ( ( rule__Package_declaration__ElementAssignment_2 )* ) ;
    public final void rule__Package_declaration__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:970:1: ( ( ( rule__Package_declaration__ElementAssignment_2 )* ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:971:1: ( ( rule__Package_declaration__ElementAssignment_2 )* )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:971:1: ( ( rule__Package_declaration__ElementAssignment_2 )* )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:972:1: ( rule__Package_declaration__ElementAssignment_2 )*
            {
             before(grammarAccess.getPackage_declarationAccess().getElementAssignment_2()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:973:1: ( rule__Package_declaration__ElementAssignment_2 )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==34) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:973:2: rule__Package_declaration__ElementAssignment_2
            	    {
            	    pushFollow(FOLLOW_rule__Package_declaration__ElementAssignment_2_in_rule__Package_declaration__Group__2__Impl2045);
            	    rule__Package_declaration__ElementAssignment_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

             after(grammarAccess.getPackage_declarationAccess().getElementAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Package_declaration__Group__2__Impl"


    // $ANTLR start "rule__Package_declaration__Group__3"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:983:1: rule__Package_declaration__Group__3 : rule__Package_declaration__Group__3__Impl rule__Package_declaration__Group__4 ;
    public final void rule__Package_declaration__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:987:1: ( rule__Package_declaration__Group__3__Impl rule__Package_declaration__Group__4 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:988:2: rule__Package_declaration__Group__3__Impl rule__Package_declaration__Group__4
            {
            pushFollow(FOLLOW_rule__Package_declaration__Group__3__Impl_in_rule__Package_declaration__Group__32076);
            rule__Package_declaration__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Package_declaration__Group__4_in_rule__Package_declaration__Group__32079);
            rule__Package_declaration__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Package_declaration__Group__3"


    // $ANTLR start "rule__Package_declaration__Group__3__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:995:1: rule__Package_declaration__Group__3__Impl : ( 'end' ) ;
    public final void rule__Package_declaration__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:999:1: ( ( 'end' ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1000:1: ( 'end' )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1000:1: ( 'end' )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1001:1: 'end'
            {
             before(grammarAccess.getPackage_declarationAccess().getEndKeyword_3()); 
            match(input,33,FOLLOW_33_in_rule__Package_declaration__Group__3__Impl2107); 
             after(grammarAccess.getPackage_declarationAccess().getEndKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Package_declaration__Group__3__Impl"


    // $ANTLR start "rule__Package_declaration__Group__4"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1014:1: rule__Package_declaration__Group__4 : rule__Package_declaration__Group__4__Impl ;
    public final void rule__Package_declaration__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1018:1: ( rule__Package_declaration__Group__4__Impl )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1019:2: rule__Package_declaration__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__Package_declaration__Group__4__Impl_in_rule__Package_declaration__Group__42138);
            rule__Package_declaration__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Package_declaration__Group__4"


    // $ANTLR start "rule__Package_declaration__Group__4__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1025:1: rule__Package_declaration__Group__4__Impl : ( ( rule__Package_declaration__SharedelementAssignment_4 ) ) ;
    public final void rule__Package_declaration__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1029:1: ( ( ( rule__Package_declaration__SharedelementAssignment_4 ) ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1030:1: ( ( rule__Package_declaration__SharedelementAssignment_4 ) )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1030:1: ( ( rule__Package_declaration__SharedelementAssignment_4 ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1031:1: ( rule__Package_declaration__SharedelementAssignment_4 )
            {
             before(grammarAccess.getPackage_declarationAccess().getSharedelementAssignment_4()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1032:1: ( rule__Package_declaration__SharedelementAssignment_4 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1032:2: rule__Package_declaration__SharedelementAssignment_4
            {
            pushFollow(FOLLOW_rule__Package_declaration__SharedelementAssignment_4_in_rule__Package_declaration__Group__4__Impl2165);
            rule__Package_declaration__SharedelementAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getPackage_declarationAccess().getSharedelementAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Package_declaration__Group__4__Impl"


    // $ANTLR start "rule__Context_declaration__Group__0"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1052:1: rule__Context_declaration__Group__0 : rule__Context_declaration__Group__0__Impl rule__Context_declaration__Group__1 ;
    public final void rule__Context_declaration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1056:1: ( rule__Context_declaration__Group__0__Impl rule__Context_declaration__Group__1 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1057:2: rule__Context_declaration__Group__0__Impl rule__Context_declaration__Group__1
            {
            pushFollow(FOLLOW_rule__Context_declaration__Group__0__Impl_in_rule__Context_declaration__Group__02205);
            rule__Context_declaration__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Context_declaration__Group__1_in_rule__Context_declaration__Group__02208);
            rule__Context_declaration__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Context_declaration__Group__0"


    // $ANTLR start "rule__Context_declaration__Group__0__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1064:1: rule__Context_declaration__Group__0__Impl : ( 'context' ) ;
    public final void rule__Context_declaration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1068:1: ( ( 'context' ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1069:1: ( 'context' )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1069:1: ( 'context' )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1070:1: 'context'
            {
             before(grammarAccess.getContext_declarationAccess().getContextKeyword_0()); 
            match(input,34,FOLLOW_34_in_rule__Context_declaration__Group__0__Impl2236); 
             after(grammarAccess.getContext_declarationAccess().getContextKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Context_declaration__Group__0__Impl"


    // $ANTLR start "rule__Context_declaration__Group__1"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1083:1: rule__Context_declaration__Group__1 : rule__Context_declaration__Group__1__Impl rule__Context_declaration__Group__2 ;
    public final void rule__Context_declaration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1087:1: ( rule__Context_declaration__Group__1__Impl rule__Context_declaration__Group__2 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1088:2: rule__Context_declaration__Group__1__Impl rule__Context_declaration__Group__2
            {
            pushFollow(FOLLOW_rule__Context_declaration__Group__1__Impl_in_rule__Context_declaration__Group__12267);
            rule__Context_declaration__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Context_declaration__Group__2_in_rule__Context_declaration__Group__12270);
            rule__Context_declaration__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Context_declaration__Group__1"


    // $ANTLR start "rule__Context_declaration__Group__1__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1095:1: rule__Context_declaration__Group__1__Impl : ( ( rule__Context_declaration__NameAssignment_1 ) ) ;
    public final void rule__Context_declaration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1099:1: ( ( ( rule__Context_declaration__NameAssignment_1 ) ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1100:1: ( ( rule__Context_declaration__NameAssignment_1 ) )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1100:1: ( ( rule__Context_declaration__NameAssignment_1 ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1101:1: ( rule__Context_declaration__NameAssignment_1 )
            {
             before(grammarAccess.getContext_declarationAccess().getNameAssignment_1()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1102:1: ( rule__Context_declaration__NameAssignment_1 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1102:2: rule__Context_declaration__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__Context_declaration__NameAssignment_1_in_rule__Context_declaration__Group__1__Impl2297);
            rule__Context_declaration__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getContext_declarationAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Context_declaration__Group__1__Impl"


    // $ANTLR start "rule__Context_declaration__Group__2"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1112:1: rule__Context_declaration__Group__2 : rule__Context_declaration__Group__2__Impl rule__Context_declaration__Group__3 ;
    public final void rule__Context_declaration__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1116:1: ( rule__Context_declaration__Group__2__Impl rule__Context_declaration__Group__3 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1117:2: rule__Context_declaration__Group__2__Impl rule__Context_declaration__Group__3
            {
            pushFollow(FOLLOW_rule__Context_declaration__Group__2__Impl_in_rule__Context_declaration__Group__22327);
            rule__Context_declaration__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Context_declaration__Group__3_in_rule__Context_declaration__Group__22330);
            rule__Context_declaration__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Context_declaration__Group__2"


    // $ANTLR start "rule__Context_declaration__Group__2__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1124:1: rule__Context_declaration__Group__2__Impl : ( ( rule__Context_declaration__ModaliteAssignment_2 )? ) ;
    public final void rule__Context_declaration__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1128:1: ( ( ( rule__Context_declaration__ModaliteAssignment_2 )? ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1129:1: ( ( rule__Context_declaration__ModaliteAssignment_2 )? )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1129:1: ( ( rule__Context_declaration__ModaliteAssignment_2 )? )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1130:1: ( rule__Context_declaration__ModaliteAssignment_2 )?
            {
             before(grammarAccess.getContext_declarationAccess().getModaliteAssignment_2()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1131:1: ( rule__Context_declaration__ModaliteAssignment_2 )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==35) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1131:2: rule__Context_declaration__ModaliteAssignment_2
                    {
                    pushFollow(FOLLOW_rule__Context_declaration__ModaliteAssignment_2_in_rule__Context_declaration__Group__2__Impl2357);
                    rule__Context_declaration__ModaliteAssignment_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getContext_declarationAccess().getModaliteAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Context_declaration__Group__2__Impl"


    // $ANTLR start "rule__Context_declaration__Group__3"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1141:1: rule__Context_declaration__Group__3 : rule__Context_declaration__Group__3__Impl ;
    public final void rule__Context_declaration__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1145:1: ( rule__Context_declaration__Group__3__Impl )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1146:2: rule__Context_declaration__Group__3__Impl
            {
            pushFollow(FOLLOW_rule__Context_declaration__Group__3__Impl_in_rule__Context_declaration__Group__32388);
            rule__Context_declaration__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Context_declaration__Group__3"


    // $ANTLR start "rule__Context_declaration__Group__3__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1152:1: rule__Context_declaration__Group__3__Impl : ( ( rule__Context_declaration__Modalite1Assignment_3 )* ) ;
    public final void rule__Context_declaration__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1156:1: ( ( ( rule__Context_declaration__Modalite1Assignment_3 )* ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1157:1: ( ( rule__Context_declaration__Modalite1Assignment_3 )* )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1157:1: ( ( rule__Context_declaration__Modalite1Assignment_3 )* )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1158:1: ( rule__Context_declaration__Modalite1Assignment_3 )*
            {
             before(grammarAccess.getContext_declarationAccess().getModalite1Assignment_3()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1159:1: ( rule__Context_declaration__Modalite1Assignment_3 )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( ((LA11_0>=28 && LA11_0<=31)) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1159:2: rule__Context_declaration__Modalite1Assignment_3
            	    {
            	    pushFollow(FOLLOW_rule__Context_declaration__Modalite1Assignment_3_in_rule__Context_declaration__Group__3__Impl2415);
            	    rule__Context_declaration__Modalite1Assignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

             after(grammarAccess.getContext_declarationAccess().getModalite1Assignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Context_declaration__Group__3__Impl"


    // $ANTLR start "rule__MiddleSection__Group__0"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1177:1: rule__MiddleSection__Group__0 : rule__MiddleSection__Group__0__Impl rule__MiddleSection__Group__1 ;
    public final void rule__MiddleSection__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1181:1: ( rule__MiddleSection__Group__0__Impl rule__MiddleSection__Group__1 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1182:2: rule__MiddleSection__Group__0__Impl rule__MiddleSection__Group__1
            {
            pushFollow(FOLLOW_rule__MiddleSection__Group__0__Impl_in_rule__MiddleSection__Group__02454);
            rule__MiddleSection__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__MiddleSection__Group__1_in_rule__MiddleSection__Group__02457);
            rule__MiddleSection__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MiddleSection__Group__0"


    // $ANTLR start "rule__MiddleSection__Group__0__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1189:1: rule__MiddleSection__Group__0__Impl : ( '::' ) ;
    public final void rule__MiddleSection__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1193:1: ( ( '::' ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1194:1: ( '::' )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1194:1: ( '::' )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1195:1: '::'
            {
             before(grammarAccess.getMiddleSectionAccess().getColonColonKeyword_0()); 
            match(input,35,FOLLOW_35_in_rule__MiddleSection__Group__0__Impl2485); 
             after(grammarAccess.getMiddleSectionAccess().getColonColonKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MiddleSection__Group__0__Impl"


    // $ANTLR start "rule__MiddleSection__Group__1"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1208:1: rule__MiddleSection__Group__1 : rule__MiddleSection__Group__1__Impl rule__MiddleSection__Group__2 ;
    public final void rule__MiddleSection__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1212:1: ( rule__MiddleSection__Group__1__Impl rule__MiddleSection__Group__2 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1213:2: rule__MiddleSection__Group__1__Impl rule__MiddleSection__Group__2
            {
            pushFollow(FOLLOW_rule__MiddleSection__Group__1__Impl_in_rule__MiddleSection__Group__12516);
            rule__MiddleSection__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__MiddleSection__Group__2_in_rule__MiddleSection__Group__12519);
            rule__MiddleSection__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MiddleSection__Group__1"


    // $ANTLR start "rule__MiddleSection__Group__1__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1220:1: rule__MiddleSection__Group__1__Impl : ( ( rule__MiddleSection__FirstSection1Assignment_1 ) ) ;
    public final void rule__MiddleSection__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1224:1: ( ( ( rule__MiddleSection__FirstSection1Assignment_1 ) ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1225:1: ( ( rule__MiddleSection__FirstSection1Assignment_1 ) )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1225:1: ( ( rule__MiddleSection__FirstSection1Assignment_1 ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1226:1: ( rule__MiddleSection__FirstSection1Assignment_1 )
            {
             before(grammarAccess.getMiddleSectionAccess().getFirstSection1Assignment_1()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1227:1: ( rule__MiddleSection__FirstSection1Assignment_1 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1227:2: rule__MiddleSection__FirstSection1Assignment_1
            {
            pushFollow(FOLLOW_rule__MiddleSection__FirstSection1Assignment_1_in_rule__MiddleSection__Group__1__Impl2546);
            rule__MiddleSection__FirstSection1Assignment_1();

            state._fsp--;


            }

             after(grammarAccess.getMiddleSectionAccess().getFirstSection1Assignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MiddleSection__Group__1__Impl"


    // $ANTLR start "rule__MiddleSection__Group__2"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1237:1: rule__MiddleSection__Group__2 : rule__MiddleSection__Group__2__Impl rule__MiddleSection__Group__3 ;
    public final void rule__MiddleSection__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1241:1: ( rule__MiddleSection__Group__2__Impl rule__MiddleSection__Group__3 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1242:2: rule__MiddleSection__Group__2__Impl rule__MiddleSection__Group__3
            {
            pushFollow(FOLLOW_rule__MiddleSection__Group__2__Impl_in_rule__MiddleSection__Group__22576);
            rule__MiddleSection__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__MiddleSection__Group__3_in_rule__MiddleSection__Group__22579);
            rule__MiddleSection__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MiddleSection__Group__2"


    // $ANTLR start "rule__MiddleSection__Group__2__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1249:1: rule__MiddleSection__Group__2__Impl : ( '(' ) ;
    public final void rule__MiddleSection__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1253:1: ( ( '(' ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1254:1: ( '(' )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1254:1: ( '(' )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1255:1: '('
            {
             before(grammarAccess.getMiddleSectionAccess().getLeftParenthesisKeyword_2()); 
            match(input,23,FOLLOW_23_in_rule__MiddleSection__Group__2__Impl2607); 
             after(grammarAccess.getMiddleSectionAccess().getLeftParenthesisKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MiddleSection__Group__2__Impl"


    // $ANTLR start "rule__MiddleSection__Group__3"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1268:1: rule__MiddleSection__Group__3 : rule__MiddleSection__Group__3__Impl rule__MiddleSection__Group__4 ;
    public final void rule__MiddleSection__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1272:1: ( rule__MiddleSection__Group__3__Impl rule__MiddleSection__Group__4 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1273:2: rule__MiddleSection__Group__3__Impl rule__MiddleSection__Group__4
            {
            pushFollow(FOLLOW_rule__MiddleSection__Group__3__Impl_in_rule__MiddleSection__Group__32638);
            rule__MiddleSection__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__MiddleSection__Group__4_in_rule__MiddleSection__Group__32641);
            rule__MiddleSection__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MiddleSection__Group__3"


    // $ANTLR start "rule__MiddleSection__Group__3__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1280:1: rule__MiddleSection__Group__3__Impl : ( ( rule__MiddleSection__SecondSection2Assignment_3 )? ) ;
    public final void rule__MiddleSection__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1284:1: ( ( ( rule__MiddleSection__SecondSection2Assignment_3 )? ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1285:1: ( ( rule__MiddleSection__SecondSection2Assignment_3 )? )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1285:1: ( ( rule__MiddleSection__SecondSection2Assignment_3 )? )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1286:1: ( rule__MiddleSection__SecondSection2Assignment_3 )?
            {
             before(grammarAccess.getMiddleSectionAccess().getSecondSection2Assignment_3()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1287:1: ( rule__MiddleSection__SecondSection2Assignment_3 )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==RULE_ID) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1287:2: rule__MiddleSection__SecondSection2Assignment_3
                    {
                    pushFollow(FOLLOW_rule__MiddleSection__SecondSection2Assignment_3_in_rule__MiddleSection__Group__3__Impl2668);
                    rule__MiddleSection__SecondSection2Assignment_3();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getMiddleSectionAccess().getSecondSection2Assignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MiddleSection__Group__3__Impl"


    // $ANTLR start "rule__MiddleSection__Group__4"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1297:1: rule__MiddleSection__Group__4 : rule__MiddleSection__Group__4__Impl rule__MiddleSection__Group__5 ;
    public final void rule__MiddleSection__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1301:1: ( rule__MiddleSection__Group__4__Impl rule__MiddleSection__Group__5 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1302:2: rule__MiddleSection__Group__4__Impl rule__MiddleSection__Group__5
            {
            pushFollow(FOLLOW_rule__MiddleSection__Group__4__Impl_in_rule__MiddleSection__Group__42699);
            rule__MiddleSection__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__MiddleSection__Group__5_in_rule__MiddleSection__Group__42702);
            rule__MiddleSection__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MiddleSection__Group__4"


    // $ANTLR start "rule__MiddleSection__Group__4__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1309:1: rule__MiddleSection__Group__4__Impl : ( ')' ) ;
    public final void rule__MiddleSection__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1313:1: ( ( ')' ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1314:1: ( ')' )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1314:1: ( ')' )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1315:1: ')'
            {
             before(grammarAccess.getMiddleSectionAccess().getRightParenthesisKeyword_4()); 
            match(input,24,FOLLOW_24_in_rule__MiddleSection__Group__4__Impl2730); 
             after(grammarAccess.getMiddleSectionAccess().getRightParenthesisKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MiddleSection__Group__4__Impl"


    // $ANTLR start "rule__MiddleSection__Group__5"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1328:1: rule__MiddleSection__Group__5 : rule__MiddleSection__Group__5__Impl ;
    public final void rule__MiddleSection__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1332:1: ( rule__MiddleSection__Group__5__Impl )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1333:2: rule__MiddleSection__Group__5__Impl
            {
            pushFollow(FOLLOW_rule__MiddleSection__Group__5__Impl_in_rule__MiddleSection__Group__52761);
            rule__MiddleSection__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MiddleSection__Group__5"


    // $ANTLR start "rule__MiddleSection__Group__5__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1339:1: rule__MiddleSection__Group__5__Impl : ( ( rule__MiddleSection__ThirdSection3Assignment_5 )* ) ;
    public final void rule__MiddleSection__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1343:1: ( ( ( rule__MiddleSection__ThirdSection3Assignment_5 )* ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1344:1: ( ( rule__MiddleSection__ThirdSection3Assignment_5 )* )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1344:1: ( ( rule__MiddleSection__ThirdSection3Assignment_5 )* )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1345:1: ( rule__MiddleSection__ThirdSection3Assignment_5 )*
            {
             before(grammarAccess.getMiddleSectionAccess().getThirdSection3Assignment_5()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1346:1: ( rule__MiddleSection__ThirdSection3Assignment_5 )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==20) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1346:2: rule__MiddleSection__ThirdSection3Assignment_5
            	    {
            	    pushFollow(FOLLOW_rule__MiddleSection__ThirdSection3Assignment_5_in_rule__MiddleSection__Group__5__Impl2788);
            	    rule__MiddleSection__ThirdSection3Assignment_5();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

             after(grammarAccess.getMiddleSectionAccess().getThirdSection3Assignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MiddleSection__Group__5__Impl"


    // $ANTLR start "rule__ThirdSection__Group__0"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1368:1: rule__ThirdSection__Group__0 : rule__ThirdSection__Group__0__Impl rule__ThirdSection__Group__1 ;
    public final void rule__ThirdSection__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1372:1: ( rule__ThirdSection__Group__0__Impl rule__ThirdSection__Group__1 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1373:2: rule__ThirdSection__Group__0__Impl rule__ThirdSection__Group__1
            {
            pushFollow(FOLLOW_rule__ThirdSection__Group__0__Impl_in_rule__ThirdSection__Group__02831);
            rule__ThirdSection__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ThirdSection__Group__1_in_rule__ThirdSection__Group__02834);
            rule__ThirdSection__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThirdSection__Group__0"


    // $ANTLR start "rule__ThirdSection__Group__0__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1380:1: rule__ThirdSection__Group__0__Impl : ( ':' ) ;
    public final void rule__ThirdSection__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1384:1: ( ( ':' ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1385:1: ( ':' )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1385:1: ( ':' )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1386:1: ':'
            {
             before(grammarAccess.getThirdSectionAccess().getColonKeyword_0()); 
            match(input,20,FOLLOW_20_in_rule__ThirdSection__Group__0__Impl2862); 
             after(grammarAccess.getThirdSectionAccess().getColonKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThirdSection__Group__0__Impl"


    // $ANTLR start "rule__ThirdSection__Group__1"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1399:1: rule__ThirdSection__Group__1 : rule__ThirdSection__Group__1__Impl ;
    public final void rule__ThirdSection__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1403:1: ( rule__ThirdSection__Group__1__Impl )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1404:2: rule__ThirdSection__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__ThirdSection__Group__1__Impl_in_rule__ThirdSection__Group__12893);
            rule__ThirdSection__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThirdSection__Group__1"


    // $ANTLR start "rule__ThirdSection__Group__1__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1410:1: rule__ThirdSection__Group__1__Impl : ( ( rule__ThirdSection__Resultat1Assignment_1 ) ) ;
    public final void rule__ThirdSection__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1414:1: ( ( ( rule__ThirdSection__Resultat1Assignment_1 ) ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1415:1: ( ( rule__ThirdSection__Resultat1Assignment_1 ) )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1415:1: ( ( rule__ThirdSection__Resultat1Assignment_1 ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1416:1: ( rule__ThirdSection__Resultat1Assignment_1 )
            {
             before(grammarAccess.getThirdSectionAccess().getResultat1Assignment_1()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1417:1: ( rule__ThirdSection__Resultat1Assignment_1 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1417:2: rule__ThirdSection__Resultat1Assignment_1
            {
            pushFollow(FOLLOW_rule__ThirdSection__Resultat1Assignment_1_in_rule__ThirdSection__Group__1__Impl2920);
            rule__ThirdSection__Resultat1Assignment_1();

            state._fsp--;


            }

             after(grammarAccess.getThirdSectionAccess().getResultat1Assignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThirdSection__Group__1__Impl"


    // $ANTLR start "rule__SecondSection__Group__0"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1431:1: rule__SecondSection__Group__0 : rule__SecondSection__Group__0__Impl rule__SecondSection__Group__1 ;
    public final void rule__SecondSection__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1435:1: ( rule__SecondSection__Group__0__Impl rule__SecondSection__Group__1 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1436:2: rule__SecondSection__Group__0__Impl rule__SecondSection__Group__1
            {
            pushFollow(FOLLOW_rule__SecondSection__Group__0__Impl_in_rule__SecondSection__Group__02954);
            rule__SecondSection__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__SecondSection__Group__1_in_rule__SecondSection__Group__02957);
            rule__SecondSection__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SecondSection__Group__0"


    // $ANTLR start "rule__SecondSection__Group__0__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1443:1: rule__SecondSection__Group__0__Impl : ( ( rule__SecondSection__Resultat12Assignment_0 ) ) ;
    public final void rule__SecondSection__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1447:1: ( ( ( rule__SecondSection__Resultat12Assignment_0 ) ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1448:1: ( ( rule__SecondSection__Resultat12Assignment_0 ) )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1448:1: ( ( rule__SecondSection__Resultat12Assignment_0 ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1449:1: ( rule__SecondSection__Resultat12Assignment_0 )
            {
             before(grammarAccess.getSecondSectionAccess().getResultat12Assignment_0()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1450:1: ( rule__SecondSection__Resultat12Assignment_0 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1450:2: rule__SecondSection__Resultat12Assignment_0
            {
            pushFollow(FOLLOW_rule__SecondSection__Resultat12Assignment_0_in_rule__SecondSection__Group__0__Impl2984);
            rule__SecondSection__Resultat12Assignment_0();

            state._fsp--;


            }

             after(grammarAccess.getSecondSectionAccess().getResultat12Assignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SecondSection__Group__0__Impl"


    // $ANTLR start "rule__SecondSection__Group__1"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1460:1: rule__SecondSection__Group__1 : rule__SecondSection__Group__1__Impl rule__SecondSection__Group__2 ;
    public final void rule__SecondSection__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1464:1: ( rule__SecondSection__Group__1__Impl rule__SecondSection__Group__2 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1465:2: rule__SecondSection__Group__1__Impl rule__SecondSection__Group__2
            {
            pushFollow(FOLLOW_rule__SecondSection__Group__1__Impl_in_rule__SecondSection__Group__13014);
            rule__SecondSection__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__SecondSection__Group__2_in_rule__SecondSection__Group__13017);
            rule__SecondSection__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SecondSection__Group__1"


    // $ANTLR start "rule__SecondSection__Group__1__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1472:1: rule__SecondSection__Group__1__Impl : ( ':' ) ;
    public final void rule__SecondSection__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1476:1: ( ( ':' ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1477:1: ( ':' )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1477:1: ( ':' )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1478:1: ':'
            {
             before(grammarAccess.getSecondSectionAccess().getColonKeyword_1()); 
            match(input,20,FOLLOW_20_in_rule__SecondSection__Group__1__Impl3045); 
             after(grammarAccess.getSecondSectionAccess().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SecondSection__Group__1__Impl"


    // $ANTLR start "rule__SecondSection__Group__2"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1491:1: rule__SecondSection__Group__2 : rule__SecondSection__Group__2__Impl ;
    public final void rule__SecondSection__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1495:1: ( rule__SecondSection__Group__2__Impl )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1496:2: rule__SecondSection__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__SecondSection__Group__2__Impl_in_rule__SecondSection__Group__23076);
            rule__SecondSection__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SecondSection__Group__2"


    // $ANTLR start "rule__SecondSection__Group__2__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1502:1: rule__SecondSection__Group__2__Impl : ( ( rule__SecondSection__Resultat1Assignment_2 ) ) ;
    public final void rule__SecondSection__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1506:1: ( ( ( rule__SecondSection__Resultat1Assignment_2 ) ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1507:1: ( ( rule__SecondSection__Resultat1Assignment_2 ) )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1507:1: ( ( rule__SecondSection__Resultat1Assignment_2 ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1508:1: ( rule__SecondSection__Resultat1Assignment_2 )
            {
             before(grammarAccess.getSecondSectionAccess().getResultat1Assignment_2()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1509:1: ( rule__SecondSection__Resultat1Assignment_2 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1509:2: rule__SecondSection__Resultat1Assignment_2
            {
            pushFollow(FOLLOW_rule__SecondSection__Resultat1Assignment_2_in_rule__SecondSection__Group__2__Impl3103);
            rule__SecondSection__Resultat1Assignment_2();

            state._fsp--;


            }

             after(grammarAccess.getSecondSectionAccess().getResultat1Assignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SecondSection__Group__2__Impl"


    // $ANTLR start "rule__LastSection__Group__0"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1525:1: rule__LastSection__Group__0 : rule__LastSection__Group__0__Impl rule__LastSection__Group__1 ;
    public final void rule__LastSection__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1529:1: ( rule__LastSection__Group__0__Impl rule__LastSection__Group__1 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1530:2: rule__LastSection__Group__0__Impl rule__LastSection__Group__1
            {
            pushFollow(FOLLOW_rule__LastSection__Group__0__Impl_in_rule__LastSection__Group__03139);
            rule__LastSection__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__LastSection__Group__1_in_rule__LastSection__Group__03142);
            rule__LastSection__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LastSection__Group__0"


    // $ANTLR start "rule__LastSection__Group__0__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1537:1: rule__LastSection__Group__0__Impl : ( ( rule__LastSection__ResultatAssignment_0 ) ) ;
    public final void rule__LastSection__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1541:1: ( ( ( rule__LastSection__ResultatAssignment_0 ) ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1542:1: ( ( rule__LastSection__ResultatAssignment_0 ) )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1542:1: ( ( rule__LastSection__ResultatAssignment_0 ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1543:1: ( rule__LastSection__ResultatAssignment_0 )
            {
             before(grammarAccess.getLastSectionAccess().getResultatAssignment_0()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1544:1: ( rule__LastSection__ResultatAssignment_0 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1544:2: rule__LastSection__ResultatAssignment_0
            {
            pushFollow(FOLLOW_rule__LastSection__ResultatAssignment_0_in_rule__LastSection__Group__0__Impl3169);
            rule__LastSection__ResultatAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getLastSectionAccess().getResultatAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LastSection__Group__0__Impl"


    // $ANTLR start "rule__LastSection__Group__1"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1554:1: rule__LastSection__Group__1 : rule__LastSection__Group__1__Impl rule__LastSection__Group__2 ;
    public final void rule__LastSection__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1558:1: ( rule__LastSection__Group__1__Impl rule__LastSection__Group__2 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1559:2: rule__LastSection__Group__1__Impl rule__LastSection__Group__2
            {
            pushFollow(FOLLOW_rule__LastSection__Group__1__Impl_in_rule__LastSection__Group__13199);
            rule__LastSection__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__LastSection__Group__2_in_rule__LastSection__Group__13202);
            rule__LastSection__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LastSection__Group__1"


    // $ANTLR start "rule__LastSection__Group__1__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1566:1: rule__LastSection__Group__1__Impl : ( ':' ) ;
    public final void rule__LastSection__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1570:1: ( ( ':' ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1571:1: ( ':' )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1571:1: ( ':' )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1572:1: ':'
            {
             before(grammarAccess.getLastSectionAccess().getColonKeyword_1()); 
            match(input,20,FOLLOW_20_in_rule__LastSection__Group__1__Impl3230); 
             after(grammarAccess.getLastSectionAccess().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LastSection__Group__1__Impl"


    // $ANTLR start "rule__LastSection__Group__2"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1585:1: rule__LastSection__Group__2 : rule__LastSection__Group__2__Impl ;
    public final void rule__LastSection__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1589:1: ( rule__LastSection__Group__2__Impl )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1590:2: rule__LastSection__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__LastSection__Group__2__Impl_in_rule__LastSection__Group__23261);
            rule__LastSection__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LastSection__Group__2"


    // $ANTLR start "rule__LastSection__Group__2__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1596:1: rule__LastSection__Group__2__Impl : ( ( rule__LastSection__Group_2__0 )* ) ;
    public final void rule__LastSection__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1600:1: ( ( ( rule__LastSection__Group_2__0 )* ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1601:1: ( ( rule__LastSection__Group_2__0 )* )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1601:1: ( ( rule__LastSection__Group_2__0 )* )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1602:1: ( rule__LastSection__Group_2__0 )*
            {
             before(grammarAccess.getLastSectionAccess().getGroup_2()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1603:1: ( rule__LastSection__Group_2__0 )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==RULE_ID||(LA14_0>=15 && LA14_0<=27)) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1603:2: rule__LastSection__Group_2__0
            	    {
            	    pushFollow(FOLLOW_rule__LastSection__Group_2__0_in_rule__LastSection__Group__2__Impl3288);
            	    rule__LastSection__Group_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

             after(grammarAccess.getLastSectionAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LastSection__Group__2__Impl"


    // $ANTLR start "rule__LastSection__Group_2__0"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1619:1: rule__LastSection__Group_2__0 : rule__LastSection__Group_2__0__Impl rule__LastSection__Group_2__1 ;
    public final void rule__LastSection__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1623:1: ( rule__LastSection__Group_2__0__Impl rule__LastSection__Group_2__1 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1624:2: rule__LastSection__Group_2__0__Impl rule__LastSection__Group_2__1
            {
            pushFollow(FOLLOW_rule__LastSection__Group_2__0__Impl_in_rule__LastSection__Group_2__03325);
            rule__LastSection__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__LastSection__Group_2__1_in_rule__LastSection__Group_2__03328);
            rule__LastSection__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LastSection__Group_2__0"


    // $ANTLR start "rule__LastSection__Group_2__0__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1631:1: rule__LastSection__Group_2__0__Impl : ( ( rule__LastSection__Debut1Assignment_2_0 ) ) ;
    public final void rule__LastSection__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1635:1: ( ( ( rule__LastSection__Debut1Assignment_2_0 ) ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1636:1: ( ( rule__LastSection__Debut1Assignment_2_0 ) )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1636:1: ( ( rule__LastSection__Debut1Assignment_2_0 ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1637:1: ( rule__LastSection__Debut1Assignment_2_0 )
            {
             before(grammarAccess.getLastSectionAccess().getDebut1Assignment_2_0()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1638:1: ( rule__LastSection__Debut1Assignment_2_0 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1638:2: rule__LastSection__Debut1Assignment_2_0
            {
            pushFollow(FOLLOW_rule__LastSection__Debut1Assignment_2_0_in_rule__LastSection__Group_2__0__Impl3355);
            rule__LastSection__Debut1Assignment_2_0();

            state._fsp--;


            }

             after(grammarAccess.getLastSectionAccess().getDebut1Assignment_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LastSection__Group_2__0__Impl"


    // $ANTLR start "rule__LastSection__Group_2__1"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1648:1: rule__LastSection__Group_2__1 : rule__LastSection__Group_2__1__Impl rule__LastSection__Group_2__2 ;
    public final void rule__LastSection__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1652:1: ( rule__LastSection__Group_2__1__Impl rule__LastSection__Group_2__2 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1653:2: rule__LastSection__Group_2__1__Impl rule__LastSection__Group_2__2
            {
            pushFollow(FOLLOW_rule__LastSection__Group_2__1__Impl_in_rule__LastSection__Group_2__13385);
            rule__LastSection__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__LastSection__Group_2__2_in_rule__LastSection__Group_2__13388);
            rule__LastSection__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LastSection__Group_2__1"


    // $ANTLR start "rule__LastSection__Group_2__1__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1660:1: rule__LastSection__Group_2__1__Impl : ( ( rule__LastSection__Resultat4Assignment_2_1 ) ) ;
    public final void rule__LastSection__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1664:1: ( ( ( rule__LastSection__Resultat4Assignment_2_1 ) ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1665:1: ( ( rule__LastSection__Resultat4Assignment_2_1 ) )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1665:1: ( ( rule__LastSection__Resultat4Assignment_2_1 ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1666:1: ( rule__LastSection__Resultat4Assignment_2_1 )
            {
             before(grammarAccess.getLastSectionAccess().getResultat4Assignment_2_1()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1667:1: ( rule__LastSection__Resultat4Assignment_2_1 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1667:2: rule__LastSection__Resultat4Assignment_2_1
            {
            pushFollow(FOLLOW_rule__LastSection__Resultat4Assignment_2_1_in_rule__LastSection__Group_2__1__Impl3415);
            rule__LastSection__Resultat4Assignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getLastSectionAccess().getResultat4Assignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LastSection__Group_2__1__Impl"


    // $ANTLR start "rule__LastSection__Group_2__2"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1677:1: rule__LastSection__Group_2__2 : rule__LastSection__Group_2__2__Impl rule__LastSection__Group_2__3 ;
    public final void rule__LastSection__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1681:1: ( rule__LastSection__Group_2__2__Impl rule__LastSection__Group_2__3 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1682:2: rule__LastSection__Group_2__2__Impl rule__LastSection__Group_2__3
            {
            pushFollow(FOLLOW_rule__LastSection__Group_2__2__Impl_in_rule__LastSection__Group_2__23445);
            rule__LastSection__Group_2__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__LastSection__Group_2__3_in_rule__LastSection__Group_2__23448);
            rule__LastSection__Group_2__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LastSection__Group_2__2"


    // $ANTLR start "rule__LastSection__Group_2__2__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1689:1: rule__LastSection__Group_2__2__Impl : ( ( rule__LastSection__Milieu1Assignment_2_2 ) ) ;
    public final void rule__LastSection__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1693:1: ( ( ( rule__LastSection__Milieu1Assignment_2_2 ) ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1694:1: ( ( rule__LastSection__Milieu1Assignment_2_2 ) )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1694:1: ( ( rule__LastSection__Milieu1Assignment_2_2 ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1695:1: ( rule__LastSection__Milieu1Assignment_2_2 )
            {
             before(grammarAccess.getLastSectionAccess().getMilieu1Assignment_2_2()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1696:1: ( rule__LastSection__Milieu1Assignment_2_2 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1696:2: rule__LastSection__Milieu1Assignment_2_2
            {
            pushFollow(FOLLOW_rule__LastSection__Milieu1Assignment_2_2_in_rule__LastSection__Group_2__2__Impl3475);
            rule__LastSection__Milieu1Assignment_2_2();

            state._fsp--;


            }

             after(grammarAccess.getLastSectionAccess().getMilieu1Assignment_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LastSection__Group_2__2__Impl"


    // $ANTLR start "rule__LastSection__Group_2__3"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1706:1: rule__LastSection__Group_2__3 : rule__LastSection__Group_2__3__Impl ;
    public final void rule__LastSection__Group_2__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1710:1: ( rule__LastSection__Group_2__3__Impl )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1711:2: rule__LastSection__Group_2__3__Impl
            {
            pushFollow(FOLLOW_rule__LastSection__Group_2__3__Impl_in_rule__LastSection__Group_2__33505);
            rule__LastSection__Group_2__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LastSection__Group_2__3"


    // $ANTLR start "rule__LastSection__Group_2__3__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1717:1: rule__LastSection__Group_2__3__Impl : ( ( rule__LastSection__Fin1Assignment_2_3 ) ) ;
    public final void rule__LastSection__Group_2__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1721:1: ( ( ( rule__LastSection__Fin1Assignment_2_3 ) ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1722:1: ( ( rule__LastSection__Fin1Assignment_2_3 ) )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1722:1: ( ( rule__LastSection__Fin1Assignment_2_3 ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1723:1: ( rule__LastSection__Fin1Assignment_2_3 )
            {
             before(grammarAccess.getLastSectionAccess().getFin1Assignment_2_3()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1724:1: ( rule__LastSection__Fin1Assignment_2_3 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1724:2: rule__LastSection__Fin1Assignment_2_3
            {
            pushFollow(FOLLOW_rule__LastSection__Fin1Assignment_2_3_in_rule__LastSection__Group_2__3__Impl3532);
            rule__LastSection__Fin1Assignment_2_3();

            state._fsp--;


            }

             after(grammarAccess.getLastSectionAccess().getFin1Assignment_2_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LastSection__Group_2__3__Impl"


    // $ANTLR start "rule__Declaration_sentence__Group__0"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1742:1: rule__Declaration_sentence__Group__0 : rule__Declaration_sentence__Group__0__Impl rule__Declaration_sentence__Group__1 ;
    public final void rule__Declaration_sentence__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1746:1: ( rule__Declaration_sentence__Group__0__Impl rule__Declaration_sentence__Group__1 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1747:2: rule__Declaration_sentence__Group__0__Impl rule__Declaration_sentence__Group__1
            {
            pushFollow(FOLLOW_rule__Declaration_sentence__Group__0__Impl_in_rule__Declaration_sentence__Group__03570);
            rule__Declaration_sentence__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Declaration_sentence__Group__1_in_rule__Declaration_sentence__Group__03573);
            rule__Declaration_sentence__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_sentence__Group__0"


    // $ANTLR start "rule__Declaration_sentence__Group__0__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1754:1: rule__Declaration_sentence__Group__0__Impl : ( 'import' ) ;
    public final void rule__Declaration_sentence__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1758:1: ( ( 'import' ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1759:1: ( 'import' )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1759:1: ( 'import' )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1760:1: 'import'
            {
             before(grammarAccess.getDeclaration_sentenceAccess().getImportKeyword_0()); 
            match(input,36,FOLLOW_36_in_rule__Declaration_sentence__Group__0__Impl3601); 
             after(grammarAccess.getDeclaration_sentenceAccess().getImportKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_sentence__Group__0__Impl"


    // $ANTLR start "rule__Declaration_sentence__Group__1"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1773:1: rule__Declaration_sentence__Group__1 : rule__Declaration_sentence__Group__1__Impl rule__Declaration_sentence__Group__2 ;
    public final void rule__Declaration_sentence__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1777:1: ( rule__Declaration_sentence__Group__1__Impl rule__Declaration_sentence__Group__2 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1778:2: rule__Declaration_sentence__Group__1__Impl rule__Declaration_sentence__Group__2
            {
            pushFollow(FOLLOW_rule__Declaration_sentence__Group__1__Impl_in_rule__Declaration_sentence__Group__13632);
            rule__Declaration_sentence__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Declaration_sentence__Group__2_in_rule__Declaration_sentence__Group__13635);
            rule__Declaration_sentence__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_sentence__Group__1"


    // $ANTLR start "rule__Declaration_sentence__Group__1__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1785:1: rule__Declaration_sentence__Group__1__Impl : ( ( rule__Declaration_sentence__NameAssignment_1 ) ) ;
    public final void rule__Declaration_sentence__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1789:1: ( ( ( rule__Declaration_sentence__NameAssignment_1 ) ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1790:1: ( ( rule__Declaration_sentence__NameAssignment_1 ) )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1790:1: ( ( rule__Declaration_sentence__NameAssignment_1 ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1791:1: ( rule__Declaration_sentence__NameAssignment_1 )
            {
             before(grammarAccess.getDeclaration_sentenceAccess().getNameAssignment_1()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1792:1: ( rule__Declaration_sentence__NameAssignment_1 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1792:2: rule__Declaration_sentence__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__Declaration_sentence__NameAssignment_1_in_rule__Declaration_sentence__Group__1__Impl3662);
            rule__Declaration_sentence__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getDeclaration_sentenceAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_sentence__Group__1__Impl"


    // $ANTLR start "rule__Declaration_sentence__Group__2"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1802:1: rule__Declaration_sentence__Group__2 : rule__Declaration_sentence__Group__2__Impl rule__Declaration_sentence__Group__3 ;
    public final void rule__Declaration_sentence__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1806:1: ( rule__Declaration_sentence__Group__2__Impl rule__Declaration_sentence__Group__3 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1807:2: rule__Declaration_sentence__Group__2__Impl rule__Declaration_sentence__Group__3
            {
            pushFollow(FOLLOW_rule__Declaration_sentence__Group__2__Impl_in_rule__Declaration_sentence__Group__23692);
            rule__Declaration_sentence__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Declaration_sentence__Group__3_in_rule__Declaration_sentence__Group__23695);
            rule__Declaration_sentence__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_sentence__Group__2"


    // $ANTLR start "rule__Declaration_sentence__Group__2__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1814:1: rule__Declaration_sentence__Group__2__Impl : ( 'http://' ) ;
    public final void rule__Declaration_sentence__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1818:1: ( ( 'http://' ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1819:1: ( 'http://' )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1819:1: ( 'http://' )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1820:1: 'http://'
            {
             before(grammarAccess.getDeclaration_sentenceAccess().getHttpKeyword_2()); 
            match(input,37,FOLLOW_37_in_rule__Declaration_sentence__Group__2__Impl3723); 
             after(grammarAccess.getDeclaration_sentenceAccess().getHttpKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_sentence__Group__2__Impl"


    // $ANTLR start "rule__Declaration_sentence__Group__3"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1833:1: rule__Declaration_sentence__Group__3 : rule__Declaration_sentence__Group__3__Impl rule__Declaration_sentence__Group__4 ;
    public final void rule__Declaration_sentence__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1837:1: ( rule__Declaration_sentence__Group__3__Impl rule__Declaration_sentence__Group__4 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1838:2: rule__Declaration_sentence__Group__3__Impl rule__Declaration_sentence__Group__4
            {
            pushFollow(FOLLOW_rule__Declaration_sentence__Group__3__Impl_in_rule__Declaration_sentence__Group__33754);
            rule__Declaration_sentence__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Declaration_sentence__Group__4_in_rule__Declaration_sentence__Group__33757);
            rule__Declaration_sentence__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_sentence__Group__3"


    // $ANTLR start "rule__Declaration_sentence__Group__3__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1845:1: rule__Declaration_sentence__Group__3__Impl : ( ( rule__Declaration_sentence__DeuxAssignment_3 ) ) ;
    public final void rule__Declaration_sentence__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1849:1: ( ( ( rule__Declaration_sentence__DeuxAssignment_3 ) ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1850:1: ( ( rule__Declaration_sentence__DeuxAssignment_3 ) )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1850:1: ( ( rule__Declaration_sentence__DeuxAssignment_3 ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1851:1: ( rule__Declaration_sentence__DeuxAssignment_3 )
            {
             before(grammarAccess.getDeclaration_sentenceAccess().getDeuxAssignment_3()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1852:1: ( rule__Declaration_sentence__DeuxAssignment_3 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1852:2: rule__Declaration_sentence__DeuxAssignment_3
            {
            pushFollow(FOLLOW_rule__Declaration_sentence__DeuxAssignment_3_in_rule__Declaration_sentence__Group__3__Impl3784);
            rule__Declaration_sentence__DeuxAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getDeclaration_sentenceAccess().getDeuxAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_sentence__Group__3__Impl"


    // $ANTLR start "rule__Declaration_sentence__Group__4"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1862:1: rule__Declaration_sentence__Group__4 : rule__Declaration_sentence__Group__4__Impl ;
    public final void rule__Declaration_sentence__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1866:1: ( rule__Declaration_sentence__Group__4__Impl )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1867:2: rule__Declaration_sentence__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__Declaration_sentence__Group__4__Impl_in_rule__Declaration_sentence__Group__43814);
            rule__Declaration_sentence__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_sentence__Group__4"


    // $ANTLR start "rule__Declaration_sentence__Group__4__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1873:1: rule__Declaration_sentence__Group__4__Impl : ( ( rule__Declaration_sentence__TroisAssignment_4 ) ) ;
    public final void rule__Declaration_sentence__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1877:1: ( ( ( rule__Declaration_sentence__TroisAssignment_4 ) ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1878:1: ( ( rule__Declaration_sentence__TroisAssignment_4 ) )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1878:1: ( ( rule__Declaration_sentence__TroisAssignment_4 ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1879:1: ( rule__Declaration_sentence__TroisAssignment_4 )
            {
             before(grammarAccess.getDeclaration_sentenceAccess().getTroisAssignment_4()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1880:1: ( rule__Declaration_sentence__TroisAssignment_4 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1880:2: rule__Declaration_sentence__TroisAssignment_4
            {
            pushFollow(FOLLOW_rule__Declaration_sentence__TroisAssignment_4_in_rule__Declaration_sentence__Group__4__Impl3841);
            rule__Declaration_sentence__TroisAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getDeclaration_sentenceAccess().getTroisAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_sentence__Group__4__Impl"


    // $ANTLR start "rule__Qualified1__Group__0"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1900:1: rule__Qualified1__Group__0 : rule__Qualified1__Group__0__Impl rule__Qualified1__Group__1 ;
    public final void rule__Qualified1__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1904:1: ( rule__Qualified1__Group__0__Impl rule__Qualified1__Group__1 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1905:2: rule__Qualified1__Group__0__Impl rule__Qualified1__Group__1
            {
            pushFollow(FOLLOW_rule__Qualified1__Group__0__Impl_in_rule__Qualified1__Group__03881);
            rule__Qualified1__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Qualified1__Group__1_in_rule__Qualified1__Group__03884);
            rule__Qualified1__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Qualified1__Group__0"


    // $ANTLR start "rule__Qualified1__Group__0__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1912:1: rule__Qualified1__Group__0__Impl : ( '/' ) ;
    public final void rule__Qualified1__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1916:1: ( ( '/' ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1917:1: ( '/' )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1917:1: ( '/' )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1918:1: '/'
            {
             before(grammarAccess.getQualified1Access().getSolidusKeyword_0()); 
            match(input,38,FOLLOW_38_in_rule__Qualified1__Group__0__Impl3912); 
             after(grammarAccess.getQualified1Access().getSolidusKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Qualified1__Group__0__Impl"


    // $ANTLR start "rule__Qualified1__Group__1"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1931:1: rule__Qualified1__Group__1 : rule__Qualified1__Group__1__Impl ;
    public final void rule__Qualified1__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1935:1: ( rule__Qualified1__Group__1__Impl )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1936:2: rule__Qualified1__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Qualified1__Group__1__Impl_in_rule__Qualified1__Group__13943);
            rule__Qualified1__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Qualified1__Group__1"


    // $ANTLR start "rule__Qualified1__Group__1__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1942:1: rule__Qualified1__Group__1__Impl : ( ( rule__Qualified1__Group_1__0 )* ) ;
    public final void rule__Qualified1__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1946:1: ( ( ( rule__Qualified1__Group_1__0 )* ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1947:1: ( ( rule__Qualified1__Group_1__0 )* )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1947:1: ( ( rule__Qualified1__Group_1__0 )* )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1948:1: ( rule__Qualified1__Group_1__0 )*
            {
             before(grammarAccess.getQualified1Access().getGroup_1()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1949:1: ( rule__Qualified1__Group_1__0 )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==RULE_ID) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1949:2: rule__Qualified1__Group_1__0
            	    {
            	    pushFollow(FOLLOW_rule__Qualified1__Group_1__0_in_rule__Qualified1__Group__1__Impl3970);
            	    rule__Qualified1__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

             after(grammarAccess.getQualified1Access().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Qualified1__Group__1__Impl"


    // $ANTLR start "rule__Qualified1__Group_1__0"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1963:1: rule__Qualified1__Group_1__0 : rule__Qualified1__Group_1__0__Impl rule__Qualified1__Group_1__1 ;
    public final void rule__Qualified1__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1967:1: ( rule__Qualified1__Group_1__0__Impl rule__Qualified1__Group_1__1 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1968:2: rule__Qualified1__Group_1__0__Impl rule__Qualified1__Group_1__1
            {
            pushFollow(FOLLOW_rule__Qualified1__Group_1__0__Impl_in_rule__Qualified1__Group_1__04005);
            rule__Qualified1__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Qualified1__Group_1__1_in_rule__Qualified1__Group_1__04008);
            rule__Qualified1__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Qualified1__Group_1__0"


    // $ANTLR start "rule__Qualified1__Group_1__0__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1975:1: rule__Qualified1__Group_1__0__Impl : ( RULE_ID ) ;
    public final void rule__Qualified1__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1979:1: ( ( RULE_ID ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1980:1: ( RULE_ID )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1980:1: ( RULE_ID )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1981:1: RULE_ID
            {
             before(grammarAccess.getQualified1Access().getIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Qualified1__Group_1__0__Impl4035); 
             after(grammarAccess.getQualified1Access().getIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Qualified1__Group_1__0__Impl"


    // $ANTLR start "rule__Qualified1__Group_1__1"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1992:1: rule__Qualified1__Group_1__1 : rule__Qualified1__Group_1__1__Impl ;
    public final void rule__Qualified1__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1996:1: ( rule__Qualified1__Group_1__1__Impl )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:1997:2: rule__Qualified1__Group_1__1__Impl
            {
            pushFollow(FOLLOW_rule__Qualified1__Group_1__1__Impl_in_rule__Qualified1__Group_1__14064);
            rule__Qualified1__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Qualified1__Group_1__1"


    // $ANTLR start "rule__Qualified1__Group_1__1__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2003:1: rule__Qualified1__Group_1__1__Impl : ( '/' ) ;
    public final void rule__Qualified1__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2007:1: ( ( '/' ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2008:1: ( '/' )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2008:1: ( '/' )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2009:1: '/'
            {
             before(grammarAccess.getQualified1Access().getSolidusKeyword_1_1()); 
            match(input,38,FOLLOW_38_in_rule__Qualified1__Group_1__1__Impl4092); 
             after(grammarAccess.getQualified1Access().getSolidusKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Qualified1__Group_1__1__Impl"


    // $ANTLR start "rule__Qualified__Group__0"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2026:1: rule__Qualified__Group__0 : rule__Qualified__Group__0__Impl rule__Qualified__Group__1 ;
    public final void rule__Qualified__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2030:1: ( rule__Qualified__Group__0__Impl rule__Qualified__Group__1 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2031:2: rule__Qualified__Group__0__Impl rule__Qualified__Group__1
            {
            pushFollow(FOLLOW_rule__Qualified__Group__0__Impl_in_rule__Qualified__Group__04127);
            rule__Qualified__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Qualified__Group__1_in_rule__Qualified__Group__04130);
            rule__Qualified__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Qualified__Group__0"


    // $ANTLR start "rule__Qualified__Group__0__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2038:1: rule__Qualified__Group__0__Impl : ( RULE_ID ) ;
    public final void rule__Qualified__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2042:1: ( ( RULE_ID ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2043:1: ( RULE_ID )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2043:1: ( RULE_ID )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2044:1: RULE_ID
            {
             before(grammarAccess.getQualifiedAccess().getIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Qualified__Group__0__Impl4157); 
             after(grammarAccess.getQualifiedAccess().getIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Qualified__Group__0__Impl"


    // $ANTLR start "rule__Qualified__Group__1"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2055:1: rule__Qualified__Group__1 : rule__Qualified__Group__1__Impl rule__Qualified__Group__2 ;
    public final void rule__Qualified__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2059:1: ( rule__Qualified__Group__1__Impl rule__Qualified__Group__2 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2060:2: rule__Qualified__Group__1__Impl rule__Qualified__Group__2
            {
            pushFollow(FOLLOW_rule__Qualified__Group__1__Impl_in_rule__Qualified__Group__14186);
            rule__Qualified__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Qualified__Group__2_in_rule__Qualified__Group__14189);
            rule__Qualified__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Qualified__Group__1"


    // $ANTLR start "rule__Qualified__Group__1__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2067:1: rule__Qualified__Group__1__Impl : ( '.' ) ;
    public final void rule__Qualified__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2071:1: ( ( '.' ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2072:1: ( '.' )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2072:1: ( '.' )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2073:1: '.'
            {
             before(grammarAccess.getQualifiedAccess().getFullStopKeyword_1()); 
            match(input,19,FOLLOW_19_in_rule__Qualified__Group__1__Impl4217); 
             after(grammarAccess.getQualifiedAccess().getFullStopKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Qualified__Group__1__Impl"


    // $ANTLR start "rule__Qualified__Group__2"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2086:1: rule__Qualified__Group__2 : rule__Qualified__Group__2__Impl ;
    public final void rule__Qualified__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2090:1: ( rule__Qualified__Group__2__Impl )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2091:2: rule__Qualified__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__Qualified__Group__2__Impl_in_rule__Qualified__Group__24248);
            rule__Qualified__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Qualified__Group__2"


    // $ANTLR start "rule__Qualified__Group__2__Impl"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2097:1: rule__Qualified__Group__2__Impl : ( ( RULE_ID )* ) ;
    public final void rule__Qualified__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2101:1: ( ( ( RULE_ID )* ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2102:1: ( ( RULE_ID )* )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2102:1: ( ( RULE_ID )* )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2103:1: ( RULE_ID )*
            {
             before(grammarAccess.getQualifiedAccess().getIDTerminalRuleCall_2()); 
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2104:1: ( RULE_ID )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==RULE_ID) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2104:3: RULE_ID
            	    {
            	    match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Qualified__Group__2__Impl4276); 

            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

             after(grammarAccess.getQualifiedAccess().getIDTerminalRuleCall_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Qualified__Group__2__Impl"


    // $ANTLR start "rule__Main__Import_sentenceAssignment_0"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2121:1: rule__Main__Import_sentenceAssignment_0 : ( ruledeclaration_sentence ) ;
    public final void rule__Main__Import_sentenceAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2125:1: ( ( ruledeclaration_sentence ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2126:1: ( ruledeclaration_sentence )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2126:1: ( ruledeclaration_sentence )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2127:1: ruledeclaration_sentence
            {
             before(grammarAccess.getMainAccess().getImport_sentenceDeclaration_sentenceParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruledeclaration_sentence_in_rule__Main__Import_sentenceAssignment_04318);
            ruledeclaration_sentence();

            state._fsp--;

             after(grammarAccess.getMainAccess().getImport_sentenceDeclaration_sentenceParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Main__Import_sentenceAssignment_0"


    // $ANTLR start "rule__Main__PackageAssignment_1"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2136:1: rule__Main__PackageAssignment_1 : ( rulepackage_declaration ) ;
    public final void rule__Main__PackageAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2140:1: ( ( rulepackage_declaration ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2141:1: ( rulepackage_declaration )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2141:1: ( rulepackage_declaration )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2142:1: rulepackage_declaration
            {
             before(grammarAccess.getMainAccess().getPackagePackage_declarationParserRuleCall_1_0()); 
            pushFollow(FOLLOW_rulepackage_declaration_in_rule__Main__PackageAssignment_14349);
            rulepackage_declaration();

            state._fsp--;

             after(grammarAccess.getMainAccess().getPackagePackage_declarationParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Main__PackageAssignment_1"


    // $ANTLR start "rule__Package_declaration__SharedelementAssignment_1"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2151:1: rule__Package_declaration__SharedelementAssignment_1 : ( ruleendstartelement ) ;
    public final void rule__Package_declaration__SharedelementAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2155:1: ( ( ruleendstartelement ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2156:1: ( ruleendstartelement )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2156:1: ( ruleendstartelement )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2157:1: ruleendstartelement
            {
             before(grammarAccess.getPackage_declarationAccess().getSharedelementEndstartelementParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleendstartelement_in_rule__Package_declaration__SharedelementAssignment_14380);
            ruleendstartelement();

            state._fsp--;

             after(grammarAccess.getPackage_declarationAccess().getSharedelementEndstartelementParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Package_declaration__SharedelementAssignment_1"


    // $ANTLR start "rule__Package_declaration__ElementAssignment_2"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2166:1: rule__Package_declaration__ElementAssignment_2 : ( ruleabstractelement ) ;
    public final void rule__Package_declaration__ElementAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2170:1: ( ( ruleabstractelement ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2171:1: ( ruleabstractelement )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2171:1: ( ruleabstractelement )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2172:1: ruleabstractelement
            {
             before(grammarAccess.getPackage_declarationAccess().getElementAbstractelementParserRuleCall_2_0()); 
            pushFollow(FOLLOW_ruleabstractelement_in_rule__Package_declaration__ElementAssignment_24411);
            ruleabstractelement();

            state._fsp--;

             after(grammarAccess.getPackage_declarationAccess().getElementAbstractelementParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Package_declaration__ElementAssignment_2"


    // $ANTLR start "rule__Package_declaration__SharedelementAssignment_4"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2181:1: rule__Package_declaration__SharedelementAssignment_4 : ( ruleendstartelement ) ;
    public final void rule__Package_declaration__SharedelementAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2185:1: ( ( ruleendstartelement ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2186:1: ( ruleendstartelement )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2186:1: ( ruleendstartelement )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2187:1: ruleendstartelement
            {
             before(grammarAccess.getPackage_declarationAccess().getSharedelementEndstartelementParserRuleCall_4_0()); 
            pushFollow(FOLLOW_ruleendstartelement_in_rule__Package_declaration__SharedelementAssignment_44442);
            ruleendstartelement();

            state._fsp--;

             after(grammarAccess.getPackage_declarationAccess().getSharedelementEndstartelementParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Package_declaration__SharedelementAssignment_4"


    // $ANTLR start "rule__Endstartelement__NameAssignment"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2196:1: rule__Endstartelement__NameAssignment : ( RULE_ID ) ;
    public final void rule__Endstartelement__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2200:1: ( ( RULE_ID ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2201:1: ( RULE_ID )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2201:1: ( RULE_ID )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2202:1: RULE_ID
            {
             before(grammarAccess.getEndstartelementAccess().getNameIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Endstartelement__NameAssignment4473); 
             after(grammarAccess.getEndstartelementAccess().getNameIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Endstartelement__NameAssignment"


    // $ANTLR start "rule__Abstractelement__ElementsAssignment"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2211:1: rule__Abstractelement__ElementsAssignment : ( rulecontext_declaration ) ;
    public final void rule__Abstractelement__ElementsAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2215:1: ( ( rulecontext_declaration ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2216:1: ( rulecontext_declaration )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2216:1: ( rulecontext_declaration )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2217:1: rulecontext_declaration
            {
             before(grammarAccess.getAbstractelementAccess().getElementsContext_declarationParserRuleCall_0()); 
            pushFollow(FOLLOW_rulecontext_declaration_in_rule__Abstractelement__ElementsAssignment4504);
            rulecontext_declaration();

            state._fsp--;

             after(grammarAccess.getAbstractelementAccess().getElementsContext_declarationParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Abstractelement__ElementsAssignment"


    // $ANTLR start "rule__Context_declaration__NameAssignment_1"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2226:1: rule__Context_declaration__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Context_declaration__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2230:1: ( ( RULE_ID ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2231:1: ( RULE_ID )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2231:1: ( RULE_ID )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2232:1: RULE_ID
            {
             before(grammarAccess.getContext_declarationAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Context_declaration__NameAssignment_14535); 
             after(grammarAccess.getContext_declarationAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Context_declaration__NameAssignment_1"


    // $ANTLR start "rule__Context_declaration__ModaliteAssignment_2"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2241:1: rule__Context_declaration__ModaliteAssignment_2 : ( ruleMiddleSection ) ;
    public final void rule__Context_declaration__ModaliteAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2245:1: ( ( ruleMiddleSection ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2246:1: ( ruleMiddleSection )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2246:1: ( ruleMiddleSection )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2247:1: ruleMiddleSection
            {
             before(grammarAccess.getContext_declarationAccess().getModaliteMiddleSectionParserRuleCall_2_0()); 
            pushFollow(FOLLOW_ruleMiddleSection_in_rule__Context_declaration__ModaliteAssignment_24566);
            ruleMiddleSection();

            state._fsp--;

             after(grammarAccess.getContext_declarationAccess().getModaliteMiddleSectionParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Context_declaration__ModaliteAssignment_2"


    // $ANTLR start "rule__Context_declaration__Modalite1Assignment_3"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2256:1: rule__Context_declaration__Modalite1Assignment_3 : ( ruleLastSection ) ;
    public final void rule__Context_declaration__Modalite1Assignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2260:1: ( ( ruleLastSection ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2261:1: ( ruleLastSection )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2261:1: ( ruleLastSection )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2262:1: ruleLastSection
            {
             before(grammarAccess.getContext_declarationAccess().getModalite1LastSectionParserRuleCall_3_0()); 
            pushFollow(FOLLOW_ruleLastSection_in_rule__Context_declaration__Modalite1Assignment_34597);
            ruleLastSection();

            state._fsp--;

             after(grammarAccess.getContext_declarationAccess().getModalite1LastSectionParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Context_declaration__Modalite1Assignment_3"


    // $ANTLR start "rule__MiddleSection__FirstSection1Assignment_1"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2271:1: rule__MiddleSection__FirstSection1Assignment_1 : ( ruleFirstSection ) ;
    public final void rule__MiddleSection__FirstSection1Assignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2275:1: ( ( ruleFirstSection ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2276:1: ( ruleFirstSection )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2276:1: ( ruleFirstSection )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2277:1: ruleFirstSection
            {
             before(grammarAccess.getMiddleSectionAccess().getFirstSection1FirstSectionParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleFirstSection_in_rule__MiddleSection__FirstSection1Assignment_14628);
            ruleFirstSection();

            state._fsp--;

             after(grammarAccess.getMiddleSectionAccess().getFirstSection1FirstSectionParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MiddleSection__FirstSection1Assignment_1"


    // $ANTLR start "rule__MiddleSection__SecondSection2Assignment_3"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2286:1: rule__MiddleSection__SecondSection2Assignment_3 : ( ruleSecondSection ) ;
    public final void rule__MiddleSection__SecondSection2Assignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2290:1: ( ( ruleSecondSection ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2291:1: ( ruleSecondSection )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2291:1: ( ruleSecondSection )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2292:1: ruleSecondSection
            {
             before(grammarAccess.getMiddleSectionAccess().getSecondSection2SecondSectionParserRuleCall_3_0()); 
            pushFollow(FOLLOW_ruleSecondSection_in_rule__MiddleSection__SecondSection2Assignment_34659);
            ruleSecondSection();

            state._fsp--;

             after(grammarAccess.getMiddleSectionAccess().getSecondSection2SecondSectionParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MiddleSection__SecondSection2Assignment_3"


    // $ANTLR start "rule__MiddleSection__ThirdSection3Assignment_5"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2301:1: rule__MiddleSection__ThirdSection3Assignment_5 : ( ruleThirdSection ) ;
    public final void rule__MiddleSection__ThirdSection3Assignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2305:1: ( ( ruleThirdSection ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2306:1: ( ruleThirdSection )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2306:1: ( ruleThirdSection )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2307:1: ruleThirdSection
            {
             before(grammarAccess.getMiddleSectionAccess().getThirdSection3ThirdSectionParserRuleCall_5_0()); 
            pushFollow(FOLLOW_ruleThirdSection_in_rule__MiddleSection__ThirdSection3Assignment_54690);
            ruleThirdSection();

            state._fsp--;

             after(grammarAccess.getMiddleSectionAccess().getThirdSection3ThirdSectionParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MiddleSection__ThirdSection3Assignment_5"


    // $ANTLR start "rule__ThirdSection__Resultat1Assignment_1"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2316:1: rule__ThirdSection__Resultat1Assignment_1 : ( ruleRETOUR1 ) ;
    public final void rule__ThirdSection__Resultat1Assignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2320:1: ( ( ruleRETOUR1 ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2321:1: ( ruleRETOUR1 )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2321:1: ( ruleRETOUR1 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2322:1: ruleRETOUR1
            {
             before(grammarAccess.getThirdSectionAccess().getResultat1RETOUR1ParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleRETOUR1_in_rule__ThirdSection__Resultat1Assignment_14721);
            ruleRETOUR1();

            state._fsp--;

             after(grammarAccess.getThirdSectionAccess().getResultat1RETOUR1ParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ThirdSection__Resultat1Assignment_1"


    // $ANTLR start "rule__RETOUR1__NameAssignment_4"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2331:1: rule__RETOUR1__NameAssignment_4 : ( RULE_ID ) ;
    public final void rule__RETOUR1__NameAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2335:1: ( ( RULE_ID ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2336:1: ( RULE_ID )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2336:1: ( RULE_ID )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2337:1: RULE_ID
            {
             before(grammarAccess.getRETOUR1Access().getNameIDTerminalRuleCall_4_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__RETOUR1__NameAssignment_44752); 
             after(grammarAccess.getRETOUR1Access().getNameIDTerminalRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RETOUR1__NameAssignment_4"


    // $ANTLR start "rule__FirstSection__NameAssignment"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2346:1: rule__FirstSection__NameAssignment : ( RULE_ID ) ;
    public final void rule__FirstSection__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2350:1: ( ( RULE_ID ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2351:1: ( RULE_ID )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2351:1: ( RULE_ID )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2352:1: RULE_ID
            {
             before(grammarAccess.getFirstSectionAccess().getNameIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__FirstSection__NameAssignment4783); 
             after(grammarAccess.getFirstSectionAccess().getNameIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FirstSection__NameAssignment"


    // $ANTLR start "rule__SecondSection__Resultat12Assignment_0"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2361:1: rule__SecondSection__Resultat12Assignment_0 : ( ruleRETOUR12 ) ;
    public final void rule__SecondSection__Resultat12Assignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2365:1: ( ( ruleRETOUR12 ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2366:1: ( ruleRETOUR12 )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2366:1: ( ruleRETOUR12 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2367:1: ruleRETOUR12
            {
             before(grammarAccess.getSecondSectionAccess().getResultat12RETOUR12ParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleRETOUR12_in_rule__SecondSection__Resultat12Assignment_04814);
            ruleRETOUR12();

            state._fsp--;

             after(grammarAccess.getSecondSectionAccess().getResultat12RETOUR12ParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SecondSection__Resultat12Assignment_0"


    // $ANTLR start "rule__SecondSection__Resultat1Assignment_2"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2376:1: rule__SecondSection__Resultat1Assignment_2 : ( ruleRETOUR1 ) ;
    public final void rule__SecondSection__Resultat1Assignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2380:1: ( ( ruleRETOUR1 ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2381:1: ( ruleRETOUR1 )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2381:1: ( ruleRETOUR1 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2382:1: ruleRETOUR1
            {
             before(grammarAccess.getSecondSectionAccess().getResultat1RETOUR1ParserRuleCall_2_0()); 
            pushFollow(FOLLOW_ruleRETOUR1_in_rule__SecondSection__Resultat1Assignment_24845);
            ruleRETOUR1();

            state._fsp--;

             after(grammarAccess.getSecondSectionAccess().getResultat1RETOUR1ParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SecondSection__Resultat1Assignment_2"


    // $ANTLR start "rule__RETOUR12__NameAssignment"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2391:1: rule__RETOUR12__NameAssignment : ( RULE_ID ) ;
    public final void rule__RETOUR12__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2395:1: ( ( RULE_ID ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2396:1: ( RULE_ID )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2396:1: ( RULE_ID )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2397:1: RULE_ID
            {
             before(grammarAccess.getRETOUR12Access().getNameIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__RETOUR12__NameAssignment4876); 
             after(grammarAccess.getRETOUR12Access().getNameIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RETOUR12__NameAssignment"


    // $ANTLR start "rule__LastSection__ResultatAssignment_0"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2406:1: rule__LastSection__ResultatAssignment_0 : ( ruleRETOUR ) ;
    public final void rule__LastSection__ResultatAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2410:1: ( ( ruleRETOUR ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2411:1: ( ruleRETOUR )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2411:1: ( ruleRETOUR )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2412:1: ruleRETOUR
            {
             before(grammarAccess.getLastSectionAccess().getResultatRETOURParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleRETOUR_in_rule__LastSection__ResultatAssignment_04907);
            ruleRETOUR();

            state._fsp--;

             after(grammarAccess.getLastSectionAccess().getResultatRETOURParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LastSection__ResultatAssignment_0"


    // $ANTLR start "rule__LastSection__Debut1Assignment_2_0"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2421:1: rule__LastSection__Debut1Assignment_2_0 : ( ruleDebut ) ;
    public final void rule__LastSection__Debut1Assignment_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2425:1: ( ( ruleDebut ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2426:1: ( ruleDebut )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2426:1: ( ruleDebut )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2427:1: ruleDebut
            {
             before(grammarAccess.getLastSectionAccess().getDebut1DebutParserRuleCall_2_0_0()); 
            pushFollow(FOLLOW_ruleDebut_in_rule__LastSection__Debut1Assignment_2_04938);
            ruleDebut();

            state._fsp--;

             after(grammarAccess.getLastSectionAccess().getDebut1DebutParserRuleCall_2_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LastSection__Debut1Assignment_2_0"


    // $ANTLR start "rule__LastSection__Resultat4Assignment_2_1"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2436:1: rule__LastSection__Resultat4Assignment_2_1 : ( ruleRETOUR4 ) ;
    public final void rule__LastSection__Resultat4Assignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2440:1: ( ( ruleRETOUR4 ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2441:1: ( ruleRETOUR4 )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2441:1: ( ruleRETOUR4 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2442:1: ruleRETOUR4
            {
             before(grammarAccess.getLastSectionAccess().getResultat4RETOUR4ParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_ruleRETOUR4_in_rule__LastSection__Resultat4Assignment_2_14969);
            ruleRETOUR4();

            state._fsp--;

             after(grammarAccess.getLastSectionAccess().getResultat4RETOUR4ParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LastSection__Resultat4Assignment_2_1"


    // $ANTLR start "rule__LastSection__Milieu1Assignment_2_2"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2451:1: rule__LastSection__Milieu1Assignment_2_2 : ( ruleMilieu ) ;
    public final void rule__LastSection__Milieu1Assignment_2_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2455:1: ( ( ruleMilieu ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2456:1: ( ruleMilieu )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2456:1: ( ruleMilieu )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2457:1: ruleMilieu
            {
             before(grammarAccess.getLastSectionAccess().getMilieu1MilieuParserRuleCall_2_2_0()); 
            pushFollow(FOLLOW_ruleMilieu_in_rule__LastSection__Milieu1Assignment_2_25000);
            ruleMilieu();

            state._fsp--;

             after(grammarAccess.getLastSectionAccess().getMilieu1MilieuParserRuleCall_2_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LastSection__Milieu1Assignment_2_2"


    // $ANTLR start "rule__LastSection__Fin1Assignment_2_3"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2466:1: rule__LastSection__Fin1Assignment_2_3 : ( ruleFin ) ;
    public final void rule__LastSection__Fin1Assignment_2_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2470:1: ( ( ruleFin ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2471:1: ( ruleFin )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2471:1: ( ruleFin )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2472:1: ruleFin
            {
             before(grammarAccess.getLastSectionAccess().getFin1FinParserRuleCall_2_3_0()); 
            pushFollow(FOLLOW_ruleFin_in_rule__LastSection__Fin1Assignment_2_35031);
            ruleFin();

            state._fsp--;

             after(grammarAccess.getLastSectionAccess().getFin1FinParserRuleCall_2_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LastSection__Fin1Assignment_2_3"


    // $ANTLR start "rule__Fin__NameAssignment"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2481:1: rule__Fin__NameAssignment : ( RULE_ID ) ;
    public final void rule__Fin__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2485:1: ( ( RULE_ID ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2486:1: ( RULE_ID )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2486:1: ( RULE_ID )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2487:1: RULE_ID
            {
             before(grammarAccess.getFinAccess().getNameIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Fin__NameAssignment5062); 
             after(grammarAccess.getFinAccess().getNameIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fin__NameAssignment"


    // $ANTLR start "rule__Milieu__NameAssignment"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2496:1: rule__Milieu__NameAssignment : ( RULE_INT ) ;
    public final void rule__Milieu__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2500:1: ( ( RULE_INT ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2501:1: ( RULE_INT )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2501:1: ( RULE_INT )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2502:1: RULE_INT
            {
             before(grammarAccess.getMilieuAccess().getNameINTTerminalRuleCall_0()); 
            match(input,RULE_INT,FOLLOW_RULE_INT_in_rule__Milieu__NameAssignment5093); 
             after(grammarAccess.getMilieuAccess().getNameINTTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Milieu__NameAssignment"


    // $ANTLR start "rule__Debut__NameAssignment"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2511:1: rule__Debut__NameAssignment : ( RULE_ID ) ;
    public final void rule__Debut__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2515:1: ( ( RULE_ID ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2516:1: ( RULE_ID )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2516:1: ( RULE_ID )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2517:1: RULE_ID
            {
             before(grammarAccess.getDebutAccess().getNameIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Debut__NameAssignment5124); 
             after(grammarAccess.getDebutAccess().getNameIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Debut__NameAssignment"


    // $ANTLR start "rule__Declaration_sentence__NameAssignment_1"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2526:1: rule__Declaration_sentence__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Declaration_sentence__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2530:1: ( ( RULE_ID ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2531:1: ( RULE_ID )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2531:1: ( RULE_ID )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2532:1: RULE_ID
            {
             before(grammarAccess.getDeclaration_sentenceAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Declaration_sentence__NameAssignment_15155); 
             after(grammarAccess.getDeclaration_sentenceAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_sentence__NameAssignment_1"


    // $ANTLR start "rule__Declaration_sentence__DeuxAssignment_3"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2541:1: rule__Declaration_sentence__DeuxAssignment_3 : ( ruleQualified ) ;
    public final void rule__Declaration_sentence__DeuxAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2545:1: ( ( ruleQualified ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2546:1: ( ruleQualified )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2546:1: ( ruleQualified )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2547:1: ruleQualified
            {
             before(grammarAccess.getDeclaration_sentenceAccess().getDeuxQualifiedParserRuleCall_3_0()); 
            pushFollow(FOLLOW_ruleQualified_in_rule__Declaration_sentence__DeuxAssignment_35186);
            ruleQualified();

            state._fsp--;

             after(grammarAccess.getDeclaration_sentenceAccess().getDeuxQualifiedParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_sentence__DeuxAssignment_3"


    // $ANTLR start "rule__Declaration_sentence__TroisAssignment_4"
    // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2556:1: rule__Declaration_sentence__TroisAssignment_4 : ( ruleQualified1 ) ;
    public final void rule__Declaration_sentence__TroisAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2560:1: ( ( ruleQualified1 ) )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2561:1: ( ruleQualified1 )
            {
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2561:1: ( ruleQualified1 )
            // ../org.xtext.example.myocl2.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyOcl2.g:2562:1: ruleQualified1
            {
             before(grammarAccess.getDeclaration_sentenceAccess().getTroisQualified1ParserRuleCall_4_0()); 
            pushFollow(FOLLOW_ruleQualified1_in_rule__Declaration_sentence__TroisAssignment_45217);
            ruleQualified1();

            state._fsp--;

             after(grammarAccess.getDeclaration_sentenceAccess().getTroisQualified1ParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_sentence__TroisAssignment_4"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleMain_in_entryRuleMain61 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMain68 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Main__Group__0_in_ruleMain94 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulepackage_declaration_in_entryRulepackage_declaration121 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulepackage_declaration128 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Package_declaration__Group__0_in_rulepackage_declaration154 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleendstartelement_in_entryRuleendstartelement181 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleendstartelement188 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Endstartelement__NameAssignment_in_ruleendstartelement214 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleabstractelement_in_entryRuleabstractelement241 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleabstractelement248 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Abstractelement__ElementsAssignment_in_ruleabstractelement276 = new BitSet(new long[]{0x0000000400000002L});
    public static final BitSet FOLLOW_rule__Abstractelement__ElementsAssignment_in_ruleabstractelement288 = new BitSet(new long[]{0x0000000400000002L});
    public static final BitSet FOLLOW_rulecontext_declaration_in_entryRulecontext_declaration318 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulecontext_declaration325 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Context_declaration__Group__0_in_rulecontext_declaration351 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMiddleSection_in_entryRuleMiddleSection378 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMiddleSection385 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MiddleSection__Group__0_in_ruleMiddleSection411 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleThirdSection_in_entryRuleThirdSection438 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleThirdSection445 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ThirdSection__Group__0_in_ruleThirdSection471 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRETOUR1_in_entryRuleRETOUR1498 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRETOUR1505 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RETOUR1__Alternatives_in_ruleRETOUR1531 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFirstSection_in_entryRuleFirstSection558 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFirstSection565 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__FirstSection__NameAssignment_in_ruleFirstSection591 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSecondSection_in_entryRuleSecondSection618 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSecondSection625 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SecondSection__Group__0_in_ruleSecondSection651 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRETOUR12_in_entryRuleRETOUR12678 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRETOUR12685 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RETOUR12__NameAssignment_in_ruleRETOUR12711 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLastSection_in_entryRuleLastSection738 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLastSection745 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LastSection__Group__0_in_ruleLastSection771 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFin_in_entryRuleFin798 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFin805 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Fin__NameAssignment_in_ruleFin831 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_ruleMilieu_in_entryRuleMilieu859 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMilieu866 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Milieu__NameAssignment_in_ruleMilieu892 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRETOUR4_in_entryRuleRETOUR4920 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRETOUR4927 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RETOUR4__Alternatives_in_ruleRETOUR4953 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRETOUR_in_entryRuleRETOUR980 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRETOUR987 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RETOUR__Alternatives_in_ruleRETOUR1013 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDebut_in_entryRuleDebut1040 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDebut1047 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Debut__NameAssignment_in_ruleDebut1073 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_ruledeclaration_sentence_in_entryRuledeclaration_sentence1101 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuledeclaration_sentence1108 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Declaration_sentence__Group__0_in_ruledeclaration_sentence1134 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualified1_in_entryRuleQualified11161 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQualified11168 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Qualified1__Group__0_in_ruleQualified11194 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualified_in_entryRuleQualified1221 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQualified1228 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Qualified__Group__0_in_ruleQualified1254 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_rule__RETOUR1__Alternatives1291 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__RETOUR1__Alternatives1311 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__RETOUR1__Alternatives1331 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_rule__RETOUR1__Alternatives1351 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RETOUR1__NameAssignment_4_in_rule__RETOUR1__Alternatives1370 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__RETOUR4__Alternatives1404 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__RETOUR4__Alternatives1424 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__RETOUR4__Alternatives1444 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__RETOUR4__Alternatives1464 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__RETOUR4__Alternatives1484 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__RETOUR4__Alternatives1504 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__RETOUR4__Alternatives1524 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rule__RETOUR4__Alternatives1544 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_rule__RETOUR4__Alternatives1564 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_rule__RETOUR4__Alternatives1584 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_rule__RETOUR4__Alternatives1604 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_rule__RETOUR4__Alternatives1624 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_rule__RETOUR4__Alternatives1644 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_28_in_rule__RETOUR__Alternatives1679 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_29_in_rule__RETOUR__Alternatives1699 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_rule__RETOUR__Alternatives1719 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_rule__RETOUR__Alternatives1739 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Main__Group__0__Impl_in_rule__Main__Group__01771 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_rule__Main__Group__1_in_rule__Main__Group__01774 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Main__Import_sentenceAssignment_0_in_rule__Main__Group__0__Impl1801 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Main__Group__1__Impl_in_rule__Main__Group__11832 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Main__PackageAssignment_1_in_rule__Main__Group__1__Impl1859 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Package_declaration__Group__0__Impl_in_rule__Package_declaration__Group__01893 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Package_declaration__Group__1_in_rule__Package_declaration__Group__01896 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_rule__Package_declaration__Group__0__Impl1924 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Package_declaration__Group__1__Impl_in_rule__Package_declaration__Group__11955 = new BitSet(new long[]{0x0000000600000000L});
    public static final BitSet FOLLOW_rule__Package_declaration__Group__2_in_rule__Package_declaration__Group__11958 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Package_declaration__SharedelementAssignment_1_in_rule__Package_declaration__Group__1__Impl1985 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Package_declaration__Group__2__Impl_in_rule__Package_declaration__Group__22015 = new BitSet(new long[]{0x0000000600000000L});
    public static final BitSet FOLLOW_rule__Package_declaration__Group__3_in_rule__Package_declaration__Group__22018 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Package_declaration__ElementAssignment_2_in_rule__Package_declaration__Group__2__Impl2045 = new BitSet(new long[]{0x0000000400000002L});
    public static final BitSet FOLLOW_rule__Package_declaration__Group__3__Impl_in_rule__Package_declaration__Group__32076 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Package_declaration__Group__4_in_rule__Package_declaration__Group__32079 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_33_in_rule__Package_declaration__Group__3__Impl2107 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Package_declaration__Group__4__Impl_in_rule__Package_declaration__Group__42138 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Package_declaration__SharedelementAssignment_4_in_rule__Package_declaration__Group__4__Impl2165 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Context_declaration__Group__0__Impl_in_rule__Context_declaration__Group__02205 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Context_declaration__Group__1_in_rule__Context_declaration__Group__02208 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_34_in_rule__Context_declaration__Group__0__Impl2236 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Context_declaration__Group__1__Impl_in_rule__Context_declaration__Group__12267 = new BitSet(new long[]{0x00000008F0000000L});
    public static final BitSet FOLLOW_rule__Context_declaration__Group__2_in_rule__Context_declaration__Group__12270 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Context_declaration__NameAssignment_1_in_rule__Context_declaration__Group__1__Impl2297 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Context_declaration__Group__2__Impl_in_rule__Context_declaration__Group__22327 = new BitSet(new long[]{0x00000008F0000000L});
    public static final BitSet FOLLOW_rule__Context_declaration__Group__3_in_rule__Context_declaration__Group__22330 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Context_declaration__ModaliteAssignment_2_in_rule__Context_declaration__Group__2__Impl2357 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Context_declaration__Group__3__Impl_in_rule__Context_declaration__Group__32388 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Context_declaration__Modalite1Assignment_3_in_rule__Context_declaration__Group__3__Impl2415 = new BitSet(new long[]{0x00000000F0000002L});
    public static final BitSet FOLLOW_rule__MiddleSection__Group__0__Impl_in_rule__MiddleSection__Group__02454 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__MiddleSection__Group__1_in_rule__MiddleSection__Group__02457 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_35_in_rule__MiddleSection__Group__0__Impl2485 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MiddleSection__Group__1__Impl_in_rule__MiddleSection__Group__12516 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_rule__MiddleSection__Group__2_in_rule__MiddleSection__Group__12519 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MiddleSection__FirstSection1Assignment_1_in_rule__MiddleSection__Group__1__Impl2546 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MiddleSection__Group__2__Impl_in_rule__MiddleSection__Group__22576 = new BitSet(new long[]{0x0000000001000010L});
    public static final BitSet FOLLOW_rule__MiddleSection__Group__3_in_rule__MiddleSection__Group__22579 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_rule__MiddleSection__Group__2__Impl2607 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MiddleSection__Group__3__Impl_in_rule__MiddleSection__Group__32638 = new BitSet(new long[]{0x0000000001000010L});
    public static final BitSet FOLLOW_rule__MiddleSection__Group__4_in_rule__MiddleSection__Group__32641 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MiddleSection__SecondSection2Assignment_3_in_rule__MiddleSection__Group__3__Impl2668 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MiddleSection__Group__4__Impl_in_rule__MiddleSection__Group__42699 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_rule__MiddleSection__Group__5_in_rule__MiddleSection__Group__42702 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_rule__MiddleSection__Group__4__Impl2730 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MiddleSection__Group__5__Impl_in_rule__MiddleSection__Group__52761 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__MiddleSection__ThirdSection3Assignment_5_in_rule__MiddleSection__Group__5__Impl2788 = new BitSet(new long[]{0x0000000000100002L});
    public static final BitSet FOLLOW_rule__ThirdSection__Group__0__Impl_in_rule__ThirdSection__Group__02831 = new BitSet(new long[]{0x0000000000007810L});
    public static final BitSet FOLLOW_rule__ThirdSection__Group__1_in_rule__ThirdSection__Group__02834 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__ThirdSection__Group__0__Impl2862 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ThirdSection__Group__1__Impl_in_rule__ThirdSection__Group__12893 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ThirdSection__Resultat1Assignment_1_in_rule__ThirdSection__Group__1__Impl2920 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SecondSection__Group__0__Impl_in_rule__SecondSection__Group__02954 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_rule__SecondSection__Group__1_in_rule__SecondSection__Group__02957 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SecondSection__Resultat12Assignment_0_in_rule__SecondSection__Group__0__Impl2984 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SecondSection__Group__1__Impl_in_rule__SecondSection__Group__13014 = new BitSet(new long[]{0x0000000000007810L});
    public static final BitSet FOLLOW_rule__SecondSection__Group__2_in_rule__SecondSection__Group__13017 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__SecondSection__Group__1__Impl3045 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SecondSection__Group__2__Impl_in_rule__SecondSection__Group__23076 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SecondSection__Resultat1Assignment_2_in_rule__SecondSection__Group__2__Impl3103 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LastSection__Group__0__Impl_in_rule__LastSection__Group__03139 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_rule__LastSection__Group__1_in_rule__LastSection__Group__03142 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LastSection__ResultatAssignment_0_in_rule__LastSection__Group__0__Impl3169 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LastSection__Group__1__Impl_in_rule__LastSection__Group__13199 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__LastSection__Group__2_in_rule__LastSection__Group__13202 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__LastSection__Group__1__Impl3230 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LastSection__Group__2__Impl_in_rule__LastSection__Group__23261 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LastSection__Group_2__0_in_rule__LastSection__Group__2__Impl3288 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_rule__LastSection__Group_2__0__Impl_in_rule__LastSection__Group_2__03325 = new BitSet(new long[]{0x000000000FFF8000L});
    public static final BitSet FOLLOW_rule__LastSection__Group_2__1_in_rule__LastSection__Group_2__03328 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LastSection__Debut1Assignment_2_0_in_rule__LastSection__Group_2__0__Impl3355 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LastSection__Group_2__1__Impl_in_rule__LastSection__Group_2__13385 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_rule__LastSection__Group_2__2_in_rule__LastSection__Group_2__13388 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LastSection__Resultat4Assignment_2_1_in_rule__LastSection__Group_2__1__Impl3415 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LastSection__Group_2__2__Impl_in_rule__LastSection__Group_2__23445 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__LastSection__Group_2__3_in_rule__LastSection__Group_2__23448 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LastSection__Milieu1Assignment_2_2_in_rule__LastSection__Group_2__2__Impl3475 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LastSection__Group_2__3__Impl_in_rule__LastSection__Group_2__33505 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LastSection__Fin1Assignment_2_3_in_rule__LastSection__Group_2__3__Impl3532 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Declaration_sentence__Group__0__Impl_in_rule__Declaration_sentence__Group__03570 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Declaration_sentence__Group__1_in_rule__Declaration_sentence__Group__03573 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_36_in_rule__Declaration_sentence__Group__0__Impl3601 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Declaration_sentence__Group__1__Impl_in_rule__Declaration_sentence__Group__13632 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_rule__Declaration_sentence__Group__2_in_rule__Declaration_sentence__Group__13635 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Declaration_sentence__NameAssignment_1_in_rule__Declaration_sentence__Group__1__Impl3662 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Declaration_sentence__Group__2__Impl_in_rule__Declaration_sentence__Group__23692 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Declaration_sentence__Group__3_in_rule__Declaration_sentence__Group__23695 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_37_in_rule__Declaration_sentence__Group__2__Impl3723 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Declaration_sentence__Group__3__Impl_in_rule__Declaration_sentence__Group__33754 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_rule__Declaration_sentence__Group__4_in_rule__Declaration_sentence__Group__33757 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Declaration_sentence__DeuxAssignment_3_in_rule__Declaration_sentence__Group__3__Impl3784 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Declaration_sentence__Group__4__Impl_in_rule__Declaration_sentence__Group__43814 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Declaration_sentence__TroisAssignment_4_in_rule__Declaration_sentence__Group__4__Impl3841 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Qualified1__Group__0__Impl_in_rule__Qualified1__Group__03881 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Qualified1__Group__1_in_rule__Qualified1__Group__03884 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_38_in_rule__Qualified1__Group__0__Impl3912 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Qualified1__Group__1__Impl_in_rule__Qualified1__Group__13943 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Qualified1__Group_1__0_in_rule__Qualified1__Group__1__Impl3970 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_rule__Qualified1__Group_1__0__Impl_in_rule__Qualified1__Group_1__04005 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_rule__Qualified1__Group_1__1_in_rule__Qualified1__Group_1__04008 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Qualified1__Group_1__0__Impl4035 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Qualified1__Group_1__1__Impl_in_rule__Qualified1__Group_1__14064 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_38_in_rule__Qualified1__Group_1__1__Impl4092 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Qualified__Group__0__Impl_in_rule__Qualified__Group__04127 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_rule__Qualified__Group__1_in_rule__Qualified__Group__04130 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Qualified__Group__0__Impl4157 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Qualified__Group__1__Impl_in_rule__Qualified__Group__14186 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Qualified__Group__2_in_rule__Qualified__Group__14189 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__Qualified__Group__1__Impl4217 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Qualified__Group__2__Impl_in_rule__Qualified__Group__24248 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Qualified__Group__2__Impl4276 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_ruledeclaration_sentence_in_rule__Main__Import_sentenceAssignment_04318 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulepackage_declaration_in_rule__Main__PackageAssignment_14349 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleendstartelement_in_rule__Package_declaration__SharedelementAssignment_14380 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleabstractelement_in_rule__Package_declaration__ElementAssignment_24411 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleendstartelement_in_rule__Package_declaration__SharedelementAssignment_44442 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Endstartelement__NameAssignment4473 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulecontext_declaration_in_rule__Abstractelement__ElementsAssignment4504 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Context_declaration__NameAssignment_14535 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMiddleSection_in_rule__Context_declaration__ModaliteAssignment_24566 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLastSection_in_rule__Context_declaration__Modalite1Assignment_34597 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFirstSection_in_rule__MiddleSection__FirstSection1Assignment_14628 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSecondSection_in_rule__MiddleSection__SecondSection2Assignment_34659 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleThirdSection_in_rule__MiddleSection__ThirdSection3Assignment_54690 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRETOUR1_in_rule__ThirdSection__Resultat1Assignment_14721 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__RETOUR1__NameAssignment_44752 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__FirstSection__NameAssignment4783 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRETOUR12_in_rule__SecondSection__Resultat12Assignment_04814 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRETOUR1_in_rule__SecondSection__Resultat1Assignment_24845 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__RETOUR12__NameAssignment4876 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRETOUR_in_rule__LastSection__ResultatAssignment_04907 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDebut_in_rule__LastSection__Debut1Assignment_2_04938 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRETOUR4_in_rule__LastSection__Resultat4Assignment_2_14969 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMilieu_in_rule__LastSection__Milieu1Assignment_2_25000 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFin_in_rule__LastSection__Fin1Assignment_2_35031 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Fin__NameAssignment5062 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_rule__Milieu__NameAssignment5093 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Debut__NameAssignment5124 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Declaration_sentence__NameAssignment_15155 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualified_in_rule__Declaration_sentence__DeuxAssignment_35186 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQualified1_in_rule__Declaration_sentence__TroisAssignment_45217 = new BitSet(new long[]{0x0000000000000002L});

}